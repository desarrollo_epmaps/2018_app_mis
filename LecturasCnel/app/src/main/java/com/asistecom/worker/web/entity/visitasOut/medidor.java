package com.asistecom.worker.web.entity.visitasOut;

import org.simpleframework.xml.Element;

public class medidor {

    @Element(required=false, data = false)
    private String marca;



    public String getSerieFabrica() {
        return serieFabrica;
    }

    public void setSerieFabrica(String serieFabrica) {
        this.serieFabrica = serieFabrica;
    }

    public String getSerieAdicional() {
        return serieAdicional;
    }

    public void setSerieAdicional(String serieAdicional) {
        this.serieAdicional = serieAdicional;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMedPrevio() {
        return medPrevio;
    }

    public void setMedPrevio(String medPrevio) {
        this.medPrevio = medPrevio;
    }

    public String getMedPosterior() {
        return medPosterior;
    }

    public void setMedPosterior(String medPosterior) {
        this.medPosterior = medPosterior;
    }


    @Element(required=false, data = false)
    private String serieFabrica;

    @Element(required=false, data = false)
    private String serieAdicional;

    @Element(required=false, data = false)
    private String variable;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Element(required=false, data = false)
    private String rango;

    @Element(required=false, data = false)
    private String modelo;

    @Element(required=false, data = false)
    private String medPrevio;

    @Element(required=false, data = false)
    private String medPosterior;


}
