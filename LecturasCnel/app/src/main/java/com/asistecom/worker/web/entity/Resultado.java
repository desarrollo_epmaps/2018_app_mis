package com.asistecom.worker.web.entity;

import org.simpleframework.xml.Element;

public class Resultado {

    @Element
    private String estado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    @Element
    public String mensaje;
    @Element
    public int id;
    @Element (data=false,required=false)
    public Object objeto;
    @Element
    public String codError;
}
