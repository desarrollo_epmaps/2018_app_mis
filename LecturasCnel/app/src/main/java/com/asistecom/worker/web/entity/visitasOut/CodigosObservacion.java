package com.asistecom.worker.web.entity.visitasOut;

import org.simpleframework.xml.ElementList;

import java.util.ArrayList;
import java.util.List;

public class CodigosObservacion {
    @ElementList(name="CodigosObservacion", entry="codigoObservacion")
    List<String> codigoObservacion = new ArrayList<String>();

    public List<String> getCodigoObservacion() {
        return codigoObservacion;
    }

    public void setCodigoObservacion(List<String> codigoObservacion) {
        this.codigoObservacion = codigoObservacion;
    }
}

