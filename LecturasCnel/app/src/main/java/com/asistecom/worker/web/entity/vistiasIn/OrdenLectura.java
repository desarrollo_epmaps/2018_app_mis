package com.asistecom.worker.web.entity.vistiasIn;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root
public class OrdenLectura {

    @Element
    public String porcion;
    @Element
    public String cecgsecu;
    @Element
    public String periodo;
    @Element(required=false, data = false)
    public String ciclo;

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    @Element(required=false, data = false)
    public String ruta;
    @Element(required=false, data = false)
    public String sector;
    @Element(required=false, data = false)
    public String mes;
    @Element(required=false, data = false)
    public String secuencia;
    @Element(required=false, data = false)
    public String estado;
    @Element(required=false, data = false)
    public String numeroMedidorAdd;
    @Element(required=false, data = false)
    public String piso;
    @Element(required=false, data = false)
    public String manzana;
    @Element(required=false, data = false)
    public String departamento;
    @Element(required=false, data = false)
    public String direccion;
    @Element(required=false, data = false)
    public String direccionTrans;
    @Element(required=false, data = false)
    public String direccionRef;
    @Element(required=false, data = false)
    public String mensajeLector;
    @Element(required=false, data = false)
    public String nombreSector;
    @Element(required=false, data = false)
    public String placaPredial;
    @Element(required=false, data = false)
    public String guia;
    @Element(required=false, data = false)
    public String origenPrevio;


    public String getNumeroDeInstalacionPrincipal() {
        return numeroDeInstalacionPrincipal;
    }

    public void setNumeroDeInstalacionPrincipal(String numeroDeInstalacionPrincipal) {
        this.numeroDeInstalacionPrincipal = numeroDeInstalacionPrincipal;
    }

    @Element(required=false, data = false)
    private String numeroDeInstalacionPrincipal;

    public String getPorcion() {
        return porcion;
    }

    public void setPorcion(String porcion) {
        this.porcion = porcion;
    }

    public String getCecgsecu() {
        return cecgsecu;
    }

    public void setCecgsecu(String cecgsecu) {
        this.cecgsecu = cecgsecu;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccionTrans() {
        return direccionTrans;
    }

    public void setDireccionTrans(String direccionTrans) {
        this.direccionTrans = direccionTrans;
    }

    public String getDireccionRef() {
        return direccionRef;
    }

    public void setDireccionRef(String direccionRef) {
        this.direccionRef = direccionRef;
    }

    public String getMensajeLector() {
        return mensajeLector;
    }

    public void setMensajeLector(String mensajeLector) {
        this.mensajeLector = mensajeLector;
    }

    public String getNombreSector() {
        return nombreSector;
    }

    public void setNombreSector(String nombreSector) {
        this.nombreSector = nombreSector;
    }

    public String getPlacaPredial() {
        return placaPredial;
    }

    public void setPlacaPredial(String placaPredial) {
        this.placaPredial = placaPredial;
    }

    public String getOrdenLectura() {
        return ordenLectura;
    }

    public void setOrdenLectura(String ordenLectura) {
        this.ordenLectura = ordenLectura;
    }

    public String getActividadLectura() {
        return actividadLectura;
    }

    public void setActividadLectura(String actividadLectura) {
        this.actividadLectura = actividadLectura;
    }

    public String getNumeroDeInstalacion() {
        return numeroDeInstalacion;
    }

    public void setNumeroDeInstalacion(String numeroDeInstalacion) {
        this.numeroDeInstalacion = numeroDeInstalacion;
    }

    public String getCuentaContrato() {
        return cuentaContrato;
    }

    public void setCuentaContrato(String cuentaContrato) {
        this.cuentaContrato = cuentaContrato;
    }

    public String getIdEstadoInstalacion() {
        return idEstadoInstalacion;
    }

    public void setIdEstadoInstalacion(String idEstadoInstalacion) {
        this.idEstadoInstalacion = idEstadoInstalacion;
    }

    public String getIdEstadoMedidor() {
        return idEstadoMedidor;
    }

    public void setIdEstadoMedidor(String idEstadoMedidor) {
        this.idEstadoMedidor = idEstadoMedidor;
    }

    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFechaLecturaAnterior() {
        return fechaLecturaAnterior;
    }

    public void setFechaLecturaAnterior(String fechaLecturaAnterior) {
        this.fechaLecturaAnterior = fechaLecturaAnterior;
    }

    public String getLecturaAnterior() {
        return lecturaAnterior;
    }

    public void setLecturaAnterior(String lecturaAnterior) {
        this.lecturaAnterior = lecturaAnterior;
    }

    public String getLecturaMin() {
        return lecturaMin;
    }

    public void setLecturaMin(String lecturaMin) {
        this.lecturaMin = lecturaMin;
    }

    public String getLecturaMax() {
        return lecturaMax;
    }

    public void setLecturaMax(String lecturaMax) {
        this.lecturaMax = lecturaMax;
    }

    public String getIdCodigoUbicacion() {
        return idCodigoUbicacion;
    }

    public void setIdCodigoUbicacion(String idCodigoUbicacion) {
        this.idCodigoUbicacion = idCodigoUbicacion;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getDigitosMedidor() {
        return digitosMedidor;
    }

    public void setDigitosMedidor(String digitosMedidor) {
        this.digitosMedidor = digitosMedidor;
    }

    public String getConsumoPromedio() {
        return consumoPromedio;
    }

    public void setConsumoPromedio(String consumoPromedio) {
        this.consumoPromedio = consumoPromedio;
    }

    public String getMetodoConsumoPrevio() {
        return metodoConsumoPrevio;
    }

    public void setMetodoConsumoPrevio(String metodoConsumoPrevio) {
        this.metodoConsumoPrevio = metodoConsumoPrevio;
    }

    public com.asistecom.worker.web.entity.vistiasIn.informacionAdicional getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(com.asistecom.worker.web.entity.vistiasIn.informacionAdicional informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    public String getConceptosEnBaseAlConsumo() {
        return conceptosEnBaseAlConsumo;
    }

    public void setConceptosEnBaseAlConsumo(String conceptosEnBaseAlConsumo) {
        this.conceptosEnBaseAlConsumo = conceptosEnBaseAlConsumo;
    }

    @Element
    public String ordenLectura;
    @Element
    public String actividadLectura;
    @Element
    public String numeroDeInstalacion;
    @Element
    public String cuentaContrato;
    @Element(required=false, data = false)
    public String idEstadoInstalacion;
    @Element(required=false, data = false)
    public String idEstadoMedidor;
    @Element
    public String numeroMedidor;
    @Element
    public String fabricante;
    @Element
    public String modelo;
    @Element(required=false, data = false)
    public String fechaLecturaAnterior;
    @Element(required=false, data = false)
    public String lecturaAnterior;
    @Element(required=false, data = false)
    public String lecturaMin;
    @Element(required=false, data = false)
    public String lecturaMax;
    @Element(required=false, data = false)
    public String idCodigoUbicacion;
    @Element(required=false, data = false)
    public String tipoInstalacion;
    @Element(required=false, data = false)
    public String digitosMedidor;
    @Element(required=false, data = false)
    public String consumoPromedio;
    @Element(required=false, data = false)
    public String metodoConsumoPrevio;
    @Element
    public informacionAdicional informacionAdicional;
    @Element(required=false, data = false)
    public String conceptosEnBaseAlConsumo;

    public String getGuia() {
        return guia;
    }

    public void setGuia(String guia) {
        this.guia = guia;
    }

    public String getOrigenPrevio() {
        return origenPrevio;
    }

    public void setOrigenPrevio(String origenPrevio) {
        this.origenPrevio = origenPrevio;
    }

    public ArrayList<com.asistecom.worker.web.entity.vistiasIn.parametro> getParametro() {
        return parametro;
    }

    public void setParametro(ArrayList<com.asistecom.worker.web.entity.vistiasIn.parametro> parametro) {
        this.parametro = parametro;
    }

    @ElementList(name="parametros", entry="parametro",required = false)
    ArrayList<parametro> parametro = new ArrayList<>();

    @ElementList(name="historicos", entry="historico")
    ArrayList<historico> historico = new ArrayList<>();

    public ArrayList<com.asistecom.worker.web.entity.vistiasIn.historico> getHistorico() {
        return historico;
    }

    public void setHistorico(ArrayList<com.asistecom.worker.web.entity.vistiasIn.historico> historico) {
        this.historico = historico;
    }
    public String leido;
    public String processed;
    public String close;
    public String continius;

    public String getNumeroMedidorAdd() {
        return numeroMedidorAdd;
    }

    public void setNumeroMedidorAdd(String numeroMedidorAdd) {
        this.numeroMedidorAdd = numeroMedidorAdd;
    }

    public String getContinius() {
        return continius;
    }

    public void setContinius(String continius) {
        this.continius = continius;
    }

    public String getLeido() {
        return leido;
    }

    public void setLeido(String leido) {
        this.leido = leido;
    }

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getPathin() {
        return pathin;
    }

    public void setPathin(String pathin) {
        this.pathin = pathin;
    }

    public String getPathout() {
        return pathout;
    }

    public void setPathout(String pathout) {
        this.pathout = pathout;
    }

    public String pathin;
    public String pathout;
}

