package com.asistecom.worker.web.entity.resources;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class Resources {
    @ElementList(name="resource", inline=true)
    private List<Resource> resources;

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
}
