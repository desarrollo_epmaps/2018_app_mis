package com.asistecom.worker.web.entity.resources;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Resource {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Attribute
    private String name;

    @Attribute
    private String lastUpdate;
}

