package com.asistecom.worker.web.entity.visitasOut;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class Fotografias {

    @ElementList(name="foto", inline=true)
    private List<String> foto;

    public List<String> getFoto() {
        return foto;
    }

    public void setFoto(List<String> foto) {
        this.foto = foto;
    }
}
