package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class codigoEntregaIA {


    @Attribute(required=false, name = "id")
    public String id;
    @Attribute(required=false, name = "idEntrega")
    public String idEntrega;
    @Attribute(required=false, name = "descEntrega")
    public String descEntrega;
    @Attribute(required=false, name = "codigoTipo")
    public String codigoTipo;
    @Attribute(required=false, name = "codNotrec")
    public String codNotrec;

    public String getCodNotrec() {
        return codNotrec;
    }

    public void setCodNotrec(String codNotrec) {
        this.codNotrec = codNotrec;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEntrega() {
        return idEntrega;
    }

    public void setIdEntrega(String idEntrega) {
        this.idEntrega = idEntrega;
    }

    public String getDescEntrega() {
        return descEntrega;
    }

    public void setDescEntrega(String descEntrega) {
        this.descEntrega = descEntrega;
    }

    public String getCodigoTipo() {
        return codigoTipo;
    }

    public void setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
    }
}
