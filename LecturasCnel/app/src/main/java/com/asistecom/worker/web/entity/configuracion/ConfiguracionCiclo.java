package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root
public class ConfiguracionCiclo {

    @ElementList(name="codigosEntregaCNEL", entry="codigoEntregaCNEL")
    List<codigoEntregaCNEL> codigoEntrega = new ArrayList<codigoEntregaCNEL>();

    @ElementList(name="codigosCNEL", entry="codigoCNEL")
    List<codigoCNEL> codigoCNEL = new ArrayList<codigoCNEL>();

    @ElementList(name="codigosEntregaIA", entry="codigoEntregaIA")
    List<codigoEntregaIA> codigoEntregaIA = new ArrayList<codigoEntregaIA>();

    @ElementList(name="codigosIA", entry="codigoIA")
    List<codigoIA> codigoIA = new ArrayList<codigoIA>();

    @ElementList(name="marcasCNEL", entry="marcaCNEL")
    List<marcaCNEL> marcaCNEL = new ArrayList<marcaCNEL>();

    public List<com.asistecom.worker.web.entity.configuracion.codigoEntregaCNEL> getCodigoEntrega() {
        return codigoEntrega;
    }

    public List<com.asistecom.worker.web.entity.configuracion.marcaCNEL> getMarcaCNEL() {
        return marcaCNEL;
    }

    public void setMarcaCNEL(List<com.asistecom.worker.web.entity.configuracion.marcaCNEL> marcaCNEL) {
        this.marcaCNEL = marcaCNEL;
    }

    public void setCodigoEntrega(List<com.asistecom.worker.web.entity.configuracion.codigoEntregaCNEL> codigoEntrega) {
        this.codigoEntrega = codigoEntrega;
    }

    public List<com.asistecom.worker.web.entity.configuracion.codigoCNEL> getCodigoCNEL() {
        return codigoCNEL;
    }

    public void setCodigoCNEL(List<com.asistecom.worker.web.entity.configuracion.codigoCNEL> codigoCNEL) {
        this.codigoCNEL = codigoCNEL;
    }

    public List<com.asistecom.worker.web.entity.configuracion.codigoEntregaIA> getCodigoEntregaIA() {
        return codigoEntregaIA;
    }

    public void setCodigoEntregaIA(List<com.asistecom.worker.web.entity.configuracion.codigoEntregaIA> codigoEntregaIA) {
        this.codigoEntregaIA = codigoEntregaIA;
    }

    public List<com.asistecom.worker.web.entity.configuracion.codigoIA> getCodigoIA() {
        return codigoIA;
    }

    public void setCodigoIA(List<com.asistecom.worker.web.entity.configuracion.codigoIA> codigoIA) {
        this.codigoIA = codigoIA;
    }

    public void setCodigos(List<codigoCNEL> codigos) {
        this.codigoCNEL = codigos;
    }


}
