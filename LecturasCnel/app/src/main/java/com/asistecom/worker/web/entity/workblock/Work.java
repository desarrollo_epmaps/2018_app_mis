package com.asistecom.worker.web.entity.workblock;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class Work {
    @Attribute
    private String name;
    @Element
    protected elemento pendingworks;
    @Element
    protected elemento pendingworksPatch;
    @Element
    protected elemento processedvisits;
    @Element
    protected elemento continuousCommunication;
    @Element
    protected elemento filePath;
    @Element
    protected elemento closedvisits;

    @Element
    protected elemento files;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public elemento getPendingworks() {
        return pendingworks;
    }

    public void setPendingworks(elemento pendingworks) {
        this.pendingworks = pendingworks;
    }

    public elemento getPendingworksPatch() {
        return pendingworksPatch;
    }

    public void setPendingworksPatch(elemento pendingworksPatch) {
        this.pendingworksPatch = pendingworksPatch;
    }

    public elemento getProcessedvisits() {
        return processedvisits;
    }

    public void setProcessedvisits(elemento processedvisits) {
        this.processedvisits = processedvisits;
    }

    public elemento getContinuousCommunication() {
        return continuousCommunication;
    }

    public void setContinuousCommunication(elemento continuousCommunication) {
        this.continuousCommunication = continuousCommunication;
    }

    public elemento getClosedvisits() {
        return closedvisits;
    }

    public void setClosedvisits(elemento closedvisits) {
        this.closedvisits = closedvisits;
    }

    public elemento getFiles() {
        return files;
    }

    public void setFiles(elemento files) {
        this.files = files;
    }

    public elemento getFilePath() {
        return filePath;
    }

    public void setFilePath(elemento filePath) {
        filePath = filePath;
    }




}

