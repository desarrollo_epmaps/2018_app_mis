package com.asistecom.worker.web.entity.visitasOut;


import org.simpleframework.xml.Element;

public class Notificacion {
    @Element(required=false, data = false)
    private String id;
    @Element(required=false, data = false)
    private String numero;
    @Element(required=false, data = false)
    private String codEntrega;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodEntrega() {
        return codEntrega;
    }

    public Notificacion() {
    }

    public void setCodEntrega(String codEntrega) {
        this.codEntrega = codEntrega;
    }
}
