package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class codigoCNEL {
    @Attribute(required=false, name = "idCodigo")
    public String idCodigo;
    @Attribute(required=false, name = "nombreCodigo")
    public String nombreCodigo;
    @Attribute(required=false, name = "tipoCodigo")
    public String tipoCodigo;

    public String getIdCodigo() {
        return idCodigo;
    }

    public void setIdCodigo(String idCodigo) {
        this.idCodigo = idCodigo;
    }

    public String getNombreCodigo() {
        return nombreCodigo;
    }

    public void setNombreCodigo(String nombreCodigo) {
        this.nombreCodigo = nombreCodigo;
    }

    public String getTipoCodigo() {
        return tipoCodigo;
    }

    public void setTipoCodigo(String tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    public String getObservacionAuto() {
        return ObservacionAuto;
    }

    public void setObservacionAuto(String observacionAuto) {
        ObservacionAuto = observacionAuto;
    }


    @Override
    public String toString() {
        return idCodigo+"-"+nombreCodigo;
    }

    @Attribute(required=false, name = "ObservacionAuto")
    public String ObservacionAuto;
}
