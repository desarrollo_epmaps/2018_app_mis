package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.Attribute;

public class codigoIA {


    @Override
    public String toString() {
        return codigo +"-"+DescripcionAI;
    }

    @Attribute(required=false, name = "id")
    public String id;
    @Attribute(required=false, name = "codigo")
    public String codigo;
    @Attribute(required=false, name = "DescripcionAI")
    public String DescripcionAI;
    @Attribute(required=false, name = "Tipo")
    public String Tipo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getId() {
        return id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcionAI() {
        return DescripcionAI;
    }

    public void setDescripcionAI(String descripcionAI) {
        DescripcionAI = descripcionAI;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }
}
