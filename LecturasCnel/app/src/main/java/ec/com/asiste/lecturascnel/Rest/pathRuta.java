package ec.com.asiste.lecturascnel.Rest;

import android.os.AsyncTask;

import com.asistecom.worker.web.entity.workblock.Work;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import ec.com.asiste.lecturascnel.logica.Parametros;

public class pathRuta extends AsyncTask<String , Void , String> {

    public pathRuta(Work wk) {
        doInBackground(wk);
    }




    protected String doInBackground(Work wk) {
        URL url = null;
        try {
            url = new URL(Parametros.ipInterna+ ":"+Parametros.puertoInterna+Parametros.servicio+wk.getPendingworksPatch().path);

            //url = new URL("http://192.168.1.152:8080/wis/servicio/registerDevice"+strings[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();

        } catch (IOException e) {
            e.printStackTrace();

        }
        try {
            connection.setRequestMethod("PATCH");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        String authString = "terminalLectura" + ":" + "password";
        byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
        String authStringEnc = new String(authEncBytes);
        connection.addRequestProperty("Authorization", "Basic "+ authStringEnc);
        connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
        connection.setDoOutput(true);
        connection.setConnectTimeout(30000);

        int resp = 0;
        try {
            resp = connection.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (resp == 200  ) {
            //throw new BetterLuckNextTimeException(...);


        }
        else if (resp == 201   ) {
            //throw new BetterLuckNextTimeException(...);


        }
        else if (resp != 200  ) {
            //throw new BetterLuckNextTimeException(...);

            return "";
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())

            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        String line = null;

        StringBuilder sb = new StringBuilder();
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (line != null) {
            sb.append(line);
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.disconnect();
        return sb.toString();
    }

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }
}
