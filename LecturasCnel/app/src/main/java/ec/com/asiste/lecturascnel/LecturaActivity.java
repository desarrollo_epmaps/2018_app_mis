package ec.com.asiste.lecturascnel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.configuracion.codigoCNEL;
import com.asistecom.worker.web.entity.visitasOut.CodigoObservacion;
import com.asistecom.worker.web.entity.visitasOut.Coordenada;
import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.visitasOut.medidor;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ec.com.asiste.lecturascnel.Zebra.BluetoothDiscovery;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Nombrar_fotos;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.RetoLectura;
import ec.com.asiste.lecturascnel.logica.SimpleGestureFilter;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.util;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.util.formatearfechaString;
import static ec.com.asiste.lecturascnel.logica.util.formatearhora;
import static ec.com.asiste.lecturascnel.logica.util.getCurrentTimeStamp;


public class LecturaActivity extends AppCompatActivity implements LocationListener, SimpleGestureFilter.SimpleGestureListener {
    Button bfoto;
    final  Context context = this;
    private visitas vt = new visitas();
    Spinner s_obs, s_imp;
    private SimpleGestureFilter detector;
    TextView tv_cuenta,tv_ciclo, tv_sector, tv_ruta, tv_seciencia, tv_cliente, tv_direccion,tv_sr,tv_srf,tv_marca,tv_fotos;
    EditText  et_contrato, et_lectura, et_obs;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    int intentos = 1;
    String obs_auto = "";
    String obs_calculo = "";
    TextView txtLat;
    String revez = "";
    String lat = "";
    String lonn = "";
    String sat = "";
    String dao = "";
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    ArrayList<String> fotos = new ArrayList<String>();
    TextView tv_gps;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);  locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        detector = new SimpleGestureFilter(LecturaActivity.this, this);
        s_imp = (Spinner)findViewById(R.id.spinner_l_imp) ;
        s_obs = (Spinner)findViewById(R.id.spinner_l_obs) ;

        tv_gps = (TextView) findViewById(R.id.tv_l_gps);
        bfoto = (Button)  findViewById(R.id.bt_l_foto);
        tv_gps = (TextView) findViewById(R.id.tv_l_gps);
        tv_cliente = (TextView)findViewById(R.id.tv_l_nombre);
        tv_sector  = (TextView)findViewById(R.id.tv_l_sector);
        tv_direccion  = (TextView)findViewById(R.id.tv_l_dir);

       //et_contrato  = (EditText) findViewById(R.id.et_contrato);
        tv_cuenta  = (TextView) findViewById(R.id.tv_l_cta);
        tv_marca  = (TextView) findViewById(R.id.tv_l_marca);
        tv_sr  = (TextView) findViewById(R.id.tv_l_sr);
        tv_srf = (TextView) findViewById(R.id.tv_l_srf);
        et_lectura  = (EditText) findViewById(R.id.et_l_lectura);
        et_obs = (EditText) findViewById(R.id.et_l_obs);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        cargarR("");
        mapVisita(vt);
        try {
            cargarObs();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_l_cnel  , menu);
        return true;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent me)
    {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    public void cargarR(String previa )
    {
        try {

            sqLite sq =  new sqLite();
            vt = new visitas();
            vt = sq.cuentaR_sql(Parametros.res, previa);
        }
        catch (Exception e )
        {}


            }
    public void cargarObs() throws Exception
    {

        List<codigoCNEL> cimp = new ArrayList<codigoCNEL>();
        List<codigoCNEL> cobs = new ArrayList<codigoCNEL>();
        codigoCNEL cod_vacio = new codigoCNEL();
        cod_vacio.tipoCodigo = "";
        cod_vacio.nombreCodigo = "";
        cod_vacio.idCodigo = "";
        cimp.add(cod_vacio);
        cobs.add(cod_vacio);
      String cods = fileManager.ReadFile(getApplicationContext(),Parametros.dir_archivos.toString(),"/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class,cods);
        for (codigoCNEL cc: v.getCodigoCNEL()
             ) {

                if(cc.tipoCodigo.contains("1"))
                {
                    cimp.add(cc);
                }
                else
                {
                    cobs.add(cc);

                }

        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cimp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_imp.setAdapter(adapter);
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cobs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_obs.setAdapter(adapter2);




    }

    public void accionder(View view)
    {
        cargarR(vt.ordenLectura.ordenLectura);
        mapVisita(vt);

    }
    public void accionizq(View view)
    {
        cargarL(vt.ordenLectura.ordenLectura);
        mapVisita(vt);

    }
    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        mostar();
    }
    public void mostar()
    {
        if(Parametros.vt_busqueda == null)
        {

        }
        else
        {

            vt = Parametros.vt_busqueda;
            Parametros.vt_busqueda = null;
            mapVisita(vt);

        }

    }
    public void cargarL(String previa )
    {
        try {

            sqLite sq =  new sqLite();
            vt = new visitas();
            vt = sq.cuentaL_sql(Parametros.res, previa);
        }
        catch (Exception e )
        {}




    }
    public void mapVisita(visitas vt)
    {
        intentos = 1;
        obs_auto = "";
        obs_calculo = "";
        Parametros.vt_actual = vt;
        bfoto.setText("FOTO("+String.valueOf(0)+")");
        intentos = 0;
        revez = "";

        et_lectura.setText("");
        et_obs.setText("");
        s_imp.setSelection(0);
        s_obs.setSelection(0);
        fotos.clear();
        try {
            //tv_ciclo.setText(vt.ordenLectura.porcion);
            tv_direccion.setText(vt.ordenLectura.direccion);

            tv_sector.setText(vt.ordenLectura.ciclo+"-"+ vt.ordenLectura.sector.toString()+"-"+ vt.ordenLectura.ruta.toString()+"-"+ vt.ordenLectura.secuencia.toString());

            tv_cliente.setText(vt.ordenLectura.informacionAdicional.getNombreCliente());
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);
            try {
                tv_sr.setText(vt.ordenLectura.numeroMedidorAdd);
            }catch (Exception ex)
            {}
            try {
                tv_srf.setText(vt.ordenLectura.numeroMedidor);
            }catch (Exception ex)
            {}
            try {
                tv_marca.setText(vt.ordenLectura.fabricante);
            }catch (Exception ex)
            {}

            //et_contrato.setText(vt.ordenLectura.ordenLectura);
        }
        catch (Exception exx)
        {

            int a = 0;
        }


    }
    @Override
    public void onSwipe(int direction)
    {

        //Detect the swipe gestures and display toast
        String showToastMessage = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                showToastMessage = "You have Swiped Right.";
                cargarL(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                showToastMessage = "You have Swiped Left.";
                cargarR(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                showToastMessage = "You have Swiped Down.";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                showToastMessage = "You have Swiped Up.";
                break;

        }
        Toast.makeText(this, showToastMessage, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onDoubleTap() {

    }
    @Override
    public void onLocationChanged(Location location)
    {
        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);

        lat = location.getLatitude()+"";
        lonn =   location.getLongitude()+"";
        tv_gps.setText("GPS: "+ lat + "  ,  " + lonn) ;
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {

    }
    @Override
    public void onProviderDisabled(String provider) {

    }
    public void tomar_foto(View view) throws IOException
    {
       dispatchTakePictureIntent();
       int cantidad = fotos.size();
        //tv_fotos.setText(String.valueOf(cantidad));
        bfoto.setText("FOTO("+String.valueOf(cantidad)+")");
    }
    private void dispatchTakePictureIntent()
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    public void pedir_revez()
    {
        et_lectura.setVisibility(View.INVISIBLE);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.lectura_revez, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                revez = userInput.getText().toString();
                                RetoLectura rl = new RetoLectura();
                                if(rl.valida_revez(et_lectura.getText().toString(), revez))
                                {
                                    et_lectura.setVisibility(View.VISIBLE);
                                    try {
                                        guarda_file();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                                else
                                {
                                    intentos += 1;
                                    et_lectura.setVisibility(View.VISIBLE);
                                    et_lectura.setText("");
                                    Toast.makeText(Parametros.getContexto(), "DATOS INCOSISTENTES", Toast.LENGTH_LONG).show();
                                }


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();




    }
    public void validar_lectura()
    {
        intentos += 1;
        boolean resultado = false;
        try
        {
            if(revez.equals("") && intentos == 0 && s_imp.getSelectedItemPosition() > 1)
            {

                guarda_file();
            }
            else if(!revez.equals("") && intentos < 3)
            {

                if(RetoLectura.valida_revez(et_lectura.getText().toString(), revez)) {
                    guarda_file();
                }
                else
                {

                }

            }
            else if(revez.equals("") && intentos == 1)
            {

                pedir_revez();
                guarda_file();

            }
            else if(!revez.equals("") && intentos == 0 && s_imp.getSelectedItemPosition() > 1)
            {


            }
            else
            {
                if(RetoLectura.valida_revez(et_lectura.getText().toString(), revez)) {

                }
                else
                {

                }
            }

        }
        catch (Exception ex)
        {
            //return false;

        }


    }
    public boolean validar_foto_video()
    {
        boolean resultado = false;
        int f = 0;
        int v = 0;

        for (String vv : fotos
                ) {
            if(vv.contains(".jpg"))
            {
                f += 1;
            }
            else if (vv.contains(".mp4"))
            {

                v += 1;

            }

        }

        if( f >= Parametros.fotos_lecturas)
        {
            resultado = true;
        }

        return resultado;

    }
    public void guarda_file() throws Exception
    {

            com.asistecom.worker.web.entity.visitasOut.OrdenLectura odl = new com.asistecom.worker.web.entity.visitasOut.OrdenLectura();
            odl.setOrdenLectura(vt.ordenLectura.getOrdenLectura());
            odl.setCecgsecu(vt.ordenLectura.getCecgsecu());
            odl.setClaseLectura("0");
            odl.setMedidor(new medidor());
            odl.setNumeroDeInstalacionPrincipal("");
            odl.setNumFactura("0");
            Coordenada coord = new Coordenada();
            coord.setCoord_X(lat);
            coord.setCoord_Y(lonn);

            ArrayList<Coordenada> liscord = new ArrayList<Coordenada>();
            liscord.add(coord);

            odl.setCoordenada(liscord);
            odl.setCuentaContrato(vt.ordenLectura.cuentaContrato);


            odl.setFoto(fotos);
            Date currentTime = Calendar.getInstance().getTime();
            odl.setFechaLecturaActual(formatearfechaString(currentTime));
            odl.setHoraLecturaActual(formatearhora(currentTime));
            odl.setLecturaActual(et_lectura.getText().toString());
            odl.setLecturaReal(et_lectura.getText().toString());
            odl.setIdLector(Parametros.usuario.toString());
            odl.setNumeroDeInstalacion(vt.ordenLectura.numeroDeInstalacion);
            odl.setPeriodo(vt.ordenLectura.getPeriodo());
            odl.setRuta(vt.ordenLectura.getRuta());
            odl.setSecuencia(vt.ordenLectura.getSecuencia());
            odl.setTipoEmisionFactura("L");
            odl.setCiclo(vt.ordenLectura.porcion.toString());
            odl.setEnServicioOut(true);
            odl.setTipoActividad(vt.ordenLectura.actividadLectura);
            odl.setTipoEnvio("1");
            odl.setClosedvisits(vt.ordenLectura.close);
            odl.setProcessedvisits(vt.ordenLectura.processed);
            odl.setContinuousCommunication(vt.ordenLectura.continius);

            CodigoObservacion c = new CodigoObservacion();
            c.setCodigo("1");
            ArrayList<CodigoObservacion> cc = new ArrayList<>();
            //cc.add("1");

            if (s_obs.getSelectedItemPosition() > 0) {
                codigoCNEL auxCod = (codigoCNEL) s_obs.getSelectedItem();
                CodigoObservacion cod = new  CodigoObservacion();
                cod.setCodigo(auxCod.idCodigo);
                cod.setTipo("observacion1");
                cc.add(cod);
                obs_auto = obs_auto + " "+auxCod.ObservacionAuto+" ";
            }
            if (s_imp.getSelectedItemPosition() > 0) {
                codigoCNEL auxCod = (codigoCNEL) s_imp.getSelectedItem();
                CodigoObservacion cod = new  CodigoObservacion();
                cod.setCodigo(auxCod.idCodigo);
                cod.setTipo("impedimento");
                cc.add(cod);
                obs_auto = obs_auto + " "+auxCod.ObservacionAuto+" ";
            }
            odl.setIntentosLectura(String.valueOf(intentos));
            odl.setIntentosImpresion("0");
            odl.setCodigoObservacion(cc);
            odl.setSobrante(false);
            String version = Parametros.version;
            odl.setVersion(version);
            odl.setObservacionAlfanumerica(obs_auto  + obs_calculo +et_obs.getText().toString());
            Serializer serializer = new Persister();
            File result = new File(vt.ordenLectura.pathout );
            try {
                com.asistecom.worker.web.entity.visitasOut.visitas vv = new com.asistecom.worker.web.entity.visitasOut.visitas();
                vv.setOrdenLectura(odl);
                serializer.write(vv, result);
                sqLite ss = new sqLite();
                ss.marcarLeido(vt.ordenLectura.ordenLectura.toString());
                String showToastMessage = "GUARDADO.";
                //// imprimir
//                Toast.makeText(this, "BUSCANDO IMPRESORAS", Toast.LENGTH_LONG).show();
//                Intent i = new Intent(this, ImprimirActivity.class);
//                startActivity(i);
                cargarR(vt.ordenLectura.ordenLectura);
                Toast.makeText(this, showToastMessage, Toast.LENGTH_LONG).show();
                mapVisita(vt);
            } catch (Exception e) {
                e.printStackTrace();
            }

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {


                }
            });

    }
    public boolean validarConsistencia()
    {
        boolean respuesta = true;
        if(et_lectura.getText().length() == 0 && s_imp.getSelectedItemPosition() == 0)
        {
            return false;
        }
        if(et_lectura.getText().length() > 0   && s_imp.getSelectedItemPosition()> 0)
        {
            return false;
        }

        return  respuesta;
    }
    public boolean validarRangos()
    {

        boolean respuesta = true;
        try {

            if(et_lectura.getText().length() == 0)
            {

                return true;
            }
            Double promedio = Double.parseDouble(vt.ordenLectura.consumoPromedio) * 30;
            Double bajo = promedio  * 0.5;
            Double alto = promedio * 1.25;
            Double muyalto = promedio * 1.75;
            Double consumo = Double.parseDouble(et_lectura.getText().toString()) - Double.parseDouble(vt.ordenLectura.lecturaAnterior);


            if(consumo < 0)
            {
                obs_calculo = " CONSUMO NEGATIVO DETECTADO ";
                return false;
            }
            if (consumo >= bajo && consumo <= alto ) {
                return true;
            }
            else
            {
                if(consumo < bajo)
                {
                    obs_calculo = " CONSUMO BAJO DETECTADO ";
                }
                if(consumo > muyalto   )
                {
                    obs_calculo = " CONSUMO MUY ALTO DETECTADO ";
                }
                if(consumo > alto   )
                {
                    obs_calculo = " CONSUMO ALTO DETECTADO ";
                }
                return false;
            }
        }
        catch (Exception ex)
        {
            return false;
        }



    }
    public void guardarLectura(View v) throws Exception
    {
        if(validar_foto_video())
        {
            if(validarConsistencia())
            {
                if(validarRangos())
                {
                    guarda_file();
                }
                else
                {

                    pedir_revez();
                }
            }
            else
            {
                Toast.makeText(this, "DATOS INCOSISTENTES", Toast.LENGTH_SHORT).show();
            }



        }
        else
        {

            Toast.makeText(this, "FOTO OBLIGATORIA", Toast.LENGTH_LONG).show();
        }
    }
    public void takePicAndDisplayIt(View view) throws IOException
    {
        Nombrar_fotos nf = new Nombrar_fotos();
        String nombre = nf.foto_cnel(vt.ordenLectura.cuentaContrato, getCurrentTimeStamp(),vt.ordenLectura.periodo);
        File file =  file =  util.createImageFile(Parametros.dir_fotos+"/"+nombre);
        fotos.add(nombre);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, Uri.fromFile(file));
        if (intent.resolveActivity(getPackageManager()) != null) {

            try {
                file =  util.createImageFile(Parametros.dir_fotos+"/"+nombre);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id==R.id.bt_lcnel_buscar_serie) {
            Toast.makeText(this,"BUSCAR",Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(this, BuscarLActivity.class);
            startActivity(intent);
        }
        if (id==R.id.bt_l_izq) {
            cargarL(vt.ordenLectura.ordenLectura.toString());
            mapVisita(vt);
        }
        if (id==R.id.bt_l_der) {
            cargarR(vt.ordenLectura.ordenLectura.toString());
            mapVisita(vt);
        }
        return super.onOptionsItemSelected(item);
    }
    static final int REQUEST_TAKE_PHOTO = 1;
    private File createImageFile() throws IOException, ParseException
    {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String nom = nf.foto_cnel(vt.ordenLectura.cuentaContrato , vt.ordenLectura.ordenLectura , "CNEL",  formatearfechaString(now),formatearhora(now),vt.ordenLectura.periodo,vt.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName


        File image = new File(storageDir+"/"+nom);
        fotos.add(util.renombraFotoVideo(nom));

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
