package ec.com.asiste.lecturascnel;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.asistecom.worker.web.entity.Resultado;
import com.asistecom.worker.web.entity.visitasOut.visitas;

import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSendXML;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.resumenRecin;

public class SendActivity extends AppCompatActivity {

    TextView des,sob;
    ArrayList<resumenRecin> resumen = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        des =(TextView)findViewById(R.id.tv_send_detalle);
        sob =(TextView)findViewById(R.id.tv_send_sobrante);
        cargar();
        startProgress();
    }
    public void startProgress() {


        new Thread(new TaskEnv()).start();
    }
    class TaskEnv implements Runnable {
        @Override
        public void run() {
            while (true) {

                try {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            cargar();
                        }
                    });

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    public void cargar()
    {
        sqLite ss = new sqLite();
        resumen = ss.resumen_sqlRIN();
        des.setText("ORDENES ENCONTRADAS:"+resumen.size());
        sob.setText("SOBRANTES ENCONTRADAS:"+fileManager.listaSobrante().size());
    }
    public void accionS(View view)
    {

        sendd();
    }
    public void sendd()
    {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //new postSystemdata("").execute("", "", "");
                try {
                    ArrayList<resumenRecin> resumenInicial = resumen;
                    for ( resumenRecin rrin: resumenInicial
                         ) {
                        String xmll = fileManager.ReadFile(
                                getApplicationContext(),
                                FilenameUtils.getPath( rrin.getPath()).replace("RECOUT/",
                                        "RECOUT") .replace("TRANSMITIDO/", "TRANSMITIDO")  ,
                                "/"+rrin.getOrden()+".xml");
                        String folder ="";
                        String[] parametross = {rrin.getUri(),
                                xmll ,
                                rrin.getOrden(),
                                FilenameUtils.getPath( rrin.getPath()).replace("/RECOUT/", "") .replace("/TRANSMITIDO/", ""),
                                "/"+rrin.getOrden()+".xml" };
                        System.out.println("orden"+rrin.getOrden());
                        System.out.println("file"+xmll);
                        new postSendXML(parametross);


                    }
                    for ( String rr: fileManager.listaSobrante()
                    ) {

                        String xmll = fileManager.ReadFile(getApplicationContext(),

                                FilenameUtils.getPath(rr).replace("RECOUT/","RECOUT"),
                                "/"+FilenameUtils.getName(rr));
                        Serializer serializer = new Persister();

                        //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");
                        visitas r = null;
                        try {
                            //Resources r = serializer.read(Resources.class, source);
                            r = serializer.read(visitas.class,xmll);
                            String[] partes = rr.split("_");

                            String[] parametross = {r.OrdenLectura.getProcessedvisits(), xmll,r.OrdenLectura.getOrdenLectura().toString(),
                                    FilenameUtils.getPath(rr).replace("/TRANSMITIDO/", "").replace("/RECOUT/", ""),
                                    "/"+ FilenameUtils.getName(rr)};
                            new postSendXML(parametross);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }




                    }
                }
                catch (Exception e)
                {
                    int a = 0;
                }

            }
        });

    }
}
