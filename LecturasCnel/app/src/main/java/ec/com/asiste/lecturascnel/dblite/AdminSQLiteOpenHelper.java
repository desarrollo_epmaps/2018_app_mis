package ec.com.asiste.lecturascnel.dblite;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by diego on 12/10/2016.
 */

public class AdminSQLiteOpenHelper  extends SQLiteOpenHelper {
    public AdminSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table usuarios(codigo int primary key,usuario text,password text, tipo text)");
        System.out.println("execute on create usr..........................");
        db.execSQL("create table global(cuenta text primary key,sr text,srf text,marca_medidor text,nombre text, direccion text)");
        System.out.println("execute on create global..........................");
        db.execSQL("create table works(name text primary key ,location text ,processing text ,finished text,postResult text ,filePath text)");
        System.out.println("execute on create works..........................");
        db.execSQL("create table recin(porcion text ,cecgsecu text ,periodo text ,\n" +
                "ordenLectura text primary key, cuentaContrato  text,ciclo text ,ruta text,sector text,\n" +
                "mes text,secuencia text,piso text,manzana text,departamento text,\n" +
                "direccion text,direccionTrans text,direccionRef text,mensajeLector text,\n" +
                "nombreSector text,placaPredial text,actividadLectura text,\n" +
                "numeroDeInstalacion text,idEstadoInstalacion text,\n" +
                "idEstadoMedidor text,numeroMedidor text,fabricante text,modelo text,\n" +
                "fechaLecturaAnterior text,lecturaAnterior text,lecturaMin text,\n" +
                "lecturaMax text,idCodigoUbicacion text,tipoInstalacion text,\n" +
                "consumoPromedio  text, metodoConsumoPrevio text,nombreCliente text,\n" +
                "numeroTelefono text,imprimir  text,idNotificacion text, trasmitido text," +
                "leido text, processed text , close text,continius text , pathin text, pathout text) ");
        System.out.println("execute on create recin..........................");
        db.execSQL("create table hist_ig(pk text primary key ,cuenta text ,orden text ,finished text,postResult text ,filePath text)");
        System.out.println("execute on create hist_ig..........................");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}