package ec.com.asiste.lecturascnel;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.asistecom.worker.web.entity.vistiasIn.parametro;

import java.util.ArrayList;

import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.util;

public class AdapterItemIndustriales extends BaseAdapter {
        protected Activity activity;
        protected ArrayList<parametro> items;

    public ArrayList<parametro> getItems() {
        return items;
    }

    public void setItems(ArrayList<parametro> items) {
        this.items = items;
    }

    public AdapterItemIndustriales (Activity activity, ArrayList<parametro> items) {
            this.activity = activity;
            this.items = items;
        }
        @Override
        public int getCount() {
            return items.size();
        }
        public void clear() {
            items.clear();
        }
        public void addAll(ArrayList<parametro> category) {
            for (int i = 0; i < category.size(); i++) {
                items.add(category.get(i));
            }
        }
        @Override
        public Object getItem(int arg0) {
            return items.get(arg0);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                //((EditText) convertView.findViewById(R.id.et_lista_valor)).addTextChangedListener(new TB_Abono_Watcher(convertView));
                v = inf.inflate(R.layout.lista_lect_industriales, null);

            }
            final int pos = position;
            parametro dir = items.get(position);
            String unidades = util.nombre_del_parametro(dir.getTipo());
            TextView title = (TextView) v.findViewById(R.id.tv_lista_paramtro);
            title.setText(dir.getTipo());
            final EditText imputt = (EditText)v.findViewById(R.id.et_lista_valor);
            imputt.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {}

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    items.get(pos).setLectura(imputt.getText().toString());
                }
            });
            TextView description = (TextView) v.findViewById(R.id.tv_lista_unidades);
            Spinner ps = (Spinner)v.findViewById(R.id.sp_i_lista_cod);
            ArrayAdapter adapter = new ArrayAdapter(Parametros.contexto, android.R.layout.simple_spinner_item, Parametros.codigos);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ps.setAdapter(adapter);
            description.setText(unidades);
              return v;
        }
}