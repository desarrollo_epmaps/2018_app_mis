package ec.com.asiste.lecturascnel.Rest;


import android.os.AsyncTask;
import android.view.View;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import ec.com.asiste.lecturascnel.EnviarActivity;
import ec.com.asiste.lecturascnel.EnvioFotosActivity;
import ec.com.asiste.lecturascnel.logica.Parametros;

import static ec.com.asiste.lecturascnel.logica.fileManager.ListaFotosC;
import static ec.com.asiste.lecturascnel.logica.fileManager.ListaFotosV;
import static ec.com.asiste.lecturascnel.logica.fileManager.deleteRecursive;

public class postUploadFile extends AsyncTask<String , Void , String> {


    int total = 0;
    int enviado = 0;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //EnvioFotosActivity.tv_log.setText("ENVIANDO.....");
    }



    @Override
    protected String doInBackground(String... strings) {

        ArrayList<String> listaf = ListaFotosC();
        total = listaf.size();
        for (String ss : listaf
                ) {
            enviado = enviado +1;
           //publishProgress(1);
            String[] partes = ss.split("_");
            URL url = null;
            try {
                url = new URL(Parametros.ipInterna + ":" + Parametros.puertoInterna + Parametros.servicioF+ "/"+partes[0]+"/"+partes[1]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            //String url_to_upload_on = Parametros.ipInterna + ":" + Parametros.puertoInterna + Parametros.servicioF;
            String file_name_with_ext =ss;
            byte[] byteArray = new byte[0];
            try {
                byteArray = FileUtils.readFileToByteArray(new File(Parametros.dir_fotosC.toString() +"/"+ss));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String attachmentName = "file";
            String attachmentFileName = file_name_with_ext;
            String crlf = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            try {

                //URL url = new URL(url_to_upload_on);
                  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setConnectTimeout(10000);
                connection.setReadTimeout(10000);
                connection.setRequestMethod("POST");

                connection.setRequestProperty(
                        "Content-Type", "multipart/form-data;boundary=" + boundary);
                String authString = "terminalLectura" + ":" + "password";
                byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
                String authStringEnc = new String(authEncBytes);
                connection.addRequestProperty("Authorization", "Basic "+ authStringEnc);
                DataOutputStream request = new DataOutputStream(
                        connection.getOutputStream());


                request.writeBytes(twoHyphens + boundary + crlf);
                request.writeBytes("Content-Disposition: form-data; name=\"" +
                        attachmentName + "\";filename=\"" +
                        attachmentFileName + "\"" + crlf);
                request.writeBytes(crlf);
                request.write(byteArray);
                request.writeBytes(crlf);
                request.writeBytes(twoHyphens + boundary +
                        twoHyphens + crlf);


                request.flush();
                request.close();
                int respuestaCod = connection.getResponseCode();
                if (respuestaCod == 200) {
                    //throw new BetterLuckNextTimeException(...);
                    //borrar file ya enviado ok
                    File fdelete = new File(Parametros.dir_fotosC.toString() +"/"+ss);
                    deleteRecursive(fdelete);

                }

                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())

                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String line = null;

                StringBuilder sb = new StringBuilder();
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                while (line != null) {
                    sb.append(line);
                    try {
                        line = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                connection.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return "";

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        EnvioFotosActivity.tv_log.setText(enviado+"/"+total);
    }

    @Override
    protected void onPostExecute(String s) {
        EnvioFotosActivity.pb.setVisibility(View.INVISIBLE);
        EnvioFotosActivity.tv_log.setText("TERMINADO.....");
    }
}

