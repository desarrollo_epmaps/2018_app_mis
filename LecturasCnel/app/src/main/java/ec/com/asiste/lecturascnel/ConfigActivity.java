package ec.com.asiste.lecturascnel;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;

public class ConfigActivity extends AppCompatActivity {

    TextView tv_files;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        tv_files = (TextView)findViewById(R.id.tv_config_fileslist);
        cargar();
    }

    public void cargar()
    {

        File directorio = new File(Parametros.dir_archivos.toString());
        String[] arrArchivos = directorio.list();
        ArrayList<String> files = new ArrayList<>();
        String resultado = "";
        for(int i=0; i<arrArchivos.length; ++i){
            if(arrArchivos[i].contains(".mp4")) {
               resultado = "\n\r" + resultado+  arrArchivos[i];
            }
        }
        tv_files.setText(resultado);
    }

    public void accionDBW(View view)
    {

    }
    public void accionDBL(View view)
    {
        procesarLocal();
    }
    public void procesarLocal()
    {
        try
        {
            sqLite sqLite = new sqLite();
            sqLite.insertarUsuarios(fileManager.ReadFile(getApplicationContext(), Parametros.dir_archivos.toString(), "/Lectores.txt"));
            sqLite.insertarGlobal(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/global.txt"));
        }
        catch(Exception ex)
        {}
    }
    public void regg()
    {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //new postSystemdata("").execute("", "", "");
                try {

                    Parametros.setContexto(getBaseContext());
                    new postSystemdata().execute("", "", "");
                }
                catch (Exception e)
                {
                    int a = 0;
                }

            }
        });
    }

}
