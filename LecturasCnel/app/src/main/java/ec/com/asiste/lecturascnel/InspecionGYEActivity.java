package ec.com.asiste.lecturascnel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.configuracion.codigoCNEL;
import com.asistecom.worker.web.entity.configuracion.codigoIA;
import com.asistecom.worker.web.entity.historicoLecturas;
import com.asistecom.worker.web.entity.vistiasIn.OrdenLectura;
import com.asistecom.worker.web.entity.vistiasIn.historico;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Nombrar_fotos;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.ResumenRuta;
import ec.com.asiste.lecturascnel.logica.SimpleGestureFilter;
import ec.com.asiste.lecturascnel.logica.fileManager;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.util.formatearfechaString;
import static ec.com.asiste.lecturascnel.logica.util.formatearhora;
import ec.com.asiste.lecturascnel.SingleCheckAdapteIG;

public class InspecionGYEActivity extends AppCompatActivity implements LocationListener, SimpleGestureFilter.SimpleGestureListener {

    RecyclerView mRecyclerView;
    private List<historicoLecturas> mSingleCheckList = new ArrayList<historicoLecturas>();
    public SingleCheckAdapteIG mAdapter;
    private SimpleGestureFilter detector;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    TextView txtLat;
    String lat = "";
    String lonn = "";
    String sat = "";
    String dao = "";
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    List<String> fotos = new ArrayList<String>();
    TextView tv_gps,tv_fotos,tv_ubic, tv_serie,tv_propietario, tv_direccion, tv_contrato, tv_cuenta;
    EditText et_lectura,et_obs;
    Spinner sp_imp, sp_cod, sp_cod2;
    String mCurrentPhotoPath;
   visitas vt = new visitas();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspecion_gye);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        tv_gps = (TextView) findViewById(R.id.tv_ig_gps);
        tv_fotos = (TextView) findViewById(R.id.tv_ig_fotos);
        tv_direccion = (TextView) findViewById(R.id.tv_ig_dir);
        tv_serie = (TextView) findViewById(R.id.tv_ig_serie);
        tv_cuenta = (TextView) findViewById(R.id.tv_ig_cuenta);
        tv_contrato = (TextView) findViewById(R.id.tv_ig_contrato);
        tv_propietario = (TextView) findViewById(R.id.tv_ig_propietario);
        tv_ubic = (TextView) findViewById(R.id.tv_ig_ubi);
        sp_imp = (Spinner)findViewById(R.id.sp_g_imp);
        sp_cod = (Spinner)findViewById(R.id.sp_ig_obs);
        sp_cod2 = (Spinner)findViewById(R.id.sp_ig_obs2);
        sp_imp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        sp_cod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                codigoIA cod = (codigoIA)sp_cod.getSelectedItem();
                codigoIA cod2 = (codigoIA)sp_cod2.getSelectedItem();

                if(cod2.DescripcionAI.equals(""))
                {

                }
                else
                {
                    if(cod.getId() == cod2.getId())
                    {
                        Toast.makeText(InspecionGYEActivity.this,"Codigo repetido", Toast.LENGTH_SHORT).show();

                        sp_cod.setSelection(0);

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        sp_cod2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                codigoIA cod = (codigoIA)sp_cod.getSelectedItem();
                codigoIA cod2 = (codigoIA)sp_cod2.getSelectedItem();

                if(cod.DescripcionAI.equals(""))
                {

                }
                else
                {
                    if(cod.getId() == cod2.getId())
                    {
                        Toast.makeText(InspecionGYEActivity.this,"Codigo repetido", Toast.LENGTH_SHORT).show();

                        sp_cod2.setSelection(0);

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        et_lectura = (EditText) findViewById(R.id.et_ig_lectura);
        et_obs = (EditText) findViewById(R.id.et_ig_obs);
        mSingleCheckList = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_ig);
        detector = new SimpleGestureFilter(InspecionGYEActivity.this, this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        try {
            carga_combos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cargarR("");
        mapVisita(vt);
        Serializer serializer = new Persister();

        visitas v = null;
        try {
            String xmll = fileManager.ReadFile(this ,vt.ordenLectura.pathin);
            v = serializer.read(visitas.class,xmll);
        } catch (Exception e) {
            e.printStackTrace();
        }
        v.ordenLectura.pathin = vt.ordenLectura.pathin;
        vt = v;


//        mapHistorico(v.ordenLectura.getHistorico());
//        mAdapter = new SingleCheckAdapteIG(this, mSingleCheckList);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        mRecyclerView.setAdapter(mAdapter);
        //mAdapter.setOnItemClickListener((AdapterView.OnItemClickListener) this);


    }
    public void mapHistorico(List<historico> p_h)
    {
        mSingleCheckList.clear();

        for (historico hh: p_h
             ) {
            historicoLecturas hl = new historicoLecturas();
            hl.setCodigo(hh.getCodigo());
            hl.setConsumo(hh.getConsumo());
            hl.setLectura(hh.getLectura());
            hl.setPeriodo(hh.getPeriodo());
            mSingleCheckList.add(hl);
        }

    }
    public void cargarR(String previa )
    {
        try {

            sqLite sq =  new sqLite();
            vt = new visitas();
            vt = sq.cuentaR_sql(Parametros.res, previa);
        }
        catch (Exception e )
        {}


    }
    public void cargarL(String previa )
    {
        try {

            sqLite sq =  new sqLite();
            vt = new visitas();
            vt = sq.cuentaL_sql(Parametros.res, previa);
        }
        catch (Exception e )
        {}


    }
    public void mapVisita(visitas vt)
    {
        tv_fotos.setText("0");
        et_lectura.setText("");
        et_obs.setText("");
        sp_imp.setSelection(0);
        sp_cod2.setSelection(0);
        sp_cod.setSelection(0);
        fotos.clear();
        try {
            //tv_ciclo.setText(vt.ordenLectura.porcion);
            tv_direccion.setText(vt.ordenLectura.direccion);
            tv_serie.setText("SERIE: "+vt.ordenLectura.numeroMedidor);
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);
            tv_contrato.setText(vt.ordenLectura.ordenLectura);
            tv_ubic.setText("C:"+vt.ordenLectura.porcion+ "  S:"+vt.ordenLectura.sector
            +"  R:"+vt.ordenLectura.ruta+" SEC:"+vt.ordenLectura.secuencia);
            ///tv_seciencia.setText(vt.ordenLectura.secuencia);
            //tv_sector.setText(vt.ordenLectura.sector.toString());
           // tv_ruta.setText(vt.ordenLectura.ruta);
            tv_propietario.setText(vt.ordenLectura.informacionAdicional.getNombreCliente());
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);

        }
        catch (Exception exx)
        {

            int a = 0;
        }


    }
    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);

        lat = location.getLatitude()+"";
        lonn =   location.getLongitude()+"";
        tv_gps.setText("GPS: "+ lat + "  ,  " + lonn + "IMPRESORA: "+prefe.getString("zebra",""));

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onSwipe(int direction) {
        String showToastMessage = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                showToastMessage = "You have Swiped Right.";
                cargarL(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                showToastMessage = "You have Swiped Left.";
                cargarR(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                showToastMessage = "You have Swiped Down.";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                showToastMessage = "You have Swiped Up.";
                break;

        }
        Toast.makeText(this, showToastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {

    }
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Toast.makeText(InspecionGYEActivity.this, "algo", Toast.LENGTH_LONG).show();
    }
    public void tomar_foto(View view) throws IOException {
        dispatchTakePictureIntent();
        int cantidad = fotos.size();
        tv_fotos.setText(String.valueOf(cantidad));
    }
    static final int REQUEST_TAKE_PHOTO = 1;
    private File createImageFile() throws IOException, ParseException {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String nom = nf.foto_cnel(vt.ordenLectura.cuentaContrato , vt.ordenLectura.ordenLectura , "IA",  formatearfechaString(now),formatearhora(now),vt.ordenLectura.periodo,vt.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName


        File image = new File(storageDir+"/"+nom);
        fotos.add(nom);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    public void carga_combos() throws Exception {
        List<codigoIA> cimp = new ArrayList<codigoIA>();
        List<codigoIA> cobs = new ArrayList<codigoIA>();
        codigoIA blanco = new codigoIA();
        blanco.id = "";
        blanco.Tipo = "";
        blanco.codigo = "";
        blanco.DescripcionAI = "";
        String cods = fileManager.ReadFile(getApplicationContext(),Parametros.dir_archivos.toString(),"/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class,cods);
        cimp.add(blanco);
        cobs.add(blanco);
        for (codigoIA cia: v.getCodigoIA()
             ) {
            if(cia.Tipo.equals("1"))
            {
                cimp.add(cia);
            }
            else
            {
                cobs.add(cia);
            }
        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cimp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_imp.setAdapter(adapter);

        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cobs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_cod.setAdapter(adapter2);
        sp_cod2.setAdapter(adapter2);
    }
}
