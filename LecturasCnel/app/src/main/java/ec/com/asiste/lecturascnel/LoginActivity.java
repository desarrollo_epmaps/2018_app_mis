package ec.com.asiste.lecturascnel;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.visitasOut.Parametro;

import java.io.File;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.dblite.AdminSQLiteOpenHelper;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.util;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity {
    EditText et_usuario, et_pdw;
    Button bt_login, bt_datosBase;
    TextView tv_mac, tv_version;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_usuario = (EditText) findViewById(R.id.et_l_usuario);
        et_pdw = (EditText) findViewById(R.id.et_l_pwd);
        tv_mac = (TextView) findViewById(R.id.tv_l_mac);
        tv_version = (TextView) findViewById(R.id.tv_l_version);
        Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES)+"/compact";
        Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Parametros.dir_videos = getExternalFilesDir(Environment.DIRECTORY_MOVIES);
        Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        Parametros.dir_archivos = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        Parametros.contexto = getApplicationContext();
        fileManager.listaSobrante();
        try{

            File dir = new File(Parametros.dir_fotosC);
            dir.mkdir();

        }
        catch (Exception e)
        {}


        try {


            String[] perms = {"android.permission.READ_PHONE_STATE", "android.permission.CAMERA",
                    "android.permission.ACCESS_FINE_LOCATION","android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.WRITE_EXTERNAL_STORAGE","android.permission.ACCESS_FINE_LOCATION","android.permission.BLUETOOTH_ADMIN"};


            int permsRequestCode = 200;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }

        }
        catch (Exception permEx)
        {


        }

        tv_version.setText("V:"+util.getVersion(getApplicationContext()));
        Parametros.version = util.getVersion(getApplicationContext());
        tv_mac.setText("ID:"+util.getMAC(getApplicationContext()));

        Parametros.mac = util.getMAC(getApplicationContext());
    }

    public void accionlog(View view)
    {
        try
        {

            if(et_usuario.getText().toString().equals("9999") && et_pdw.getText().toString().equals(Parametros.password9999))
            {
                SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                Parametros.zebra = preferencias.getString("zebra", "0:0:0:0");
                Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/compact";
                Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
                et_pdw.setText("");
                Intent i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
            }
            else
            {
                sqLite sql = new sqLite();
                //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
                if(sql.validar(et_usuario.getText().toString(), et_pdw.getText().toString())) {

                    SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferencias.edit();
                    editor.putString("usuario", et_usuario.getText().toString());
                    editor.commit();

                    Parametros.zebra = preferencias.getString("zebra", "0:0:0:0");
                    Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/compact";
                    Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
                    Intent i = new Intent(this, PrincipalActivity.class);
                    et_pdw.setText("");
                    startActivity(i);
                }
            }
        }
        catch (Exception ex)
        {
            Toast.makeText(this, "LOGIN ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    public void alerta()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("REGISTRAR DISPOSITIVO");
        builder.setMessage("Desea registrar?");

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                Toast toast = Toast.makeText(getApplicationContext(), "Registrando...", Toast.LENGTH_SHORT); toast.show();
                regg();
                dialog.dismiss();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast toast = Toast.makeText(getApplicationContext(), "Registro cancelado", Toast.LENGTH_SHORT); toast.show();
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    public void registroD(View view)
    {
       alerta();

    }
    public void regg()
    {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //new postSystemdata("").execute("", "", "");
                    try {
                        //tv_mac.setText("ID:"+util.getMAC(getApplicationContext()));
                        Parametros.setMAC(util.getMAC(getApplicationContext()));
                        String[] parametross = {"/" + et_usuario.getText().toString() + "/" + Parametros.getMAC().toString() + "/false", et_usuario.getText().toString(), Parametros.getMAC().toString()};
                        new postRegisterDevice(parametross);
                        Parametros.setContexto(getBaseContext());
                        new postSystemdata().execute("", "", "");
                    }
                    catch (Exception e)
                    {
                      int a = 0;
                    }

            }
        });
    }
}
