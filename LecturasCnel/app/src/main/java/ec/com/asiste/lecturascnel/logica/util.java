package ec.com.asiste.lecturascnel.logica;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.telephony.TelephonyManager;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

public class util {

    public static String nombre_del_parametro(String valor) {
        String aux = valor.substring(0,1);
        if (aux.equals("A")) {
            return "Kwh";
        } else if (aux.equals("R")) {
            return "kvArh";
        } else if (aux.equals("D")) {
            return "Kw";
        } else {
            return "";
        }
    }


    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String rellenarCerosIzquierda(String str, int tamanio) {
        StringBuilder sb = new StringBuilder();

        for (int toPrepend = tamanio - str.length(); toPrepend > 0; toPrepend--) {
            sb.append('0');
        }

        sb.append(str);
        String result = sb.toString();
        return result;
    }

    //**
    //* Obtiene la hora en formato HHmmss
    // * @param string
    // * @return
    // */
    public static String formatearhora(Date arg) throws ParseException {
        Date date = arg;
        // Tambien se puede obtener solo la hora
        SimpleDateFormat hora = new SimpleDateFormat("HHmmss");
        String convertido = hora.format(date);
        //System.out.println(convertido);

        return convertido;

    }

    /**
     //  * Obtiene la fecha y hora en formato yyyyMMdd
     // * @param string
     // * @return
     // */
    public static String formatearfechaString(Date arg) throws ParseException {
        Date date = arg;

        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        String convertido = fecha.format(date);

        return convertido;

    }

    public static String getVersion(Context applicationContext) {

        try {
            PackageInfo pInfo = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
        return "";
    }

    public static String
    getMAC(Context applicationContext) {

        try {
            TelephonyManager telephonyManager = (TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);



            @SuppressLint("MissingPermission") String deviceId = telephonyManager.getDeviceId();
            WifiManager manager = (WifiManager) applicationContext.getSystemService(Context.WIFI_SERVICE);
            @SuppressLint("MissingPermission") WifiInfo info = manager.getConnectionInfo();
            String address = info.getMacAddress();
            //address = "490154203237518";
            Parametros.setMAC(deviceId);
            return deviceId;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    public static File createImageFile(String name ) throws IOException {
        // Create an image file name

        File image = new File(name);
        return image;
    }
    public static String renombraFotoVideo(String name ) throws IOException {
        // Create an image file name
        String[] split = name.split("_");
        String resouesta = split[0]+"_"+split[2]+"_"+split[3]+"_"+split[4]+"_"+split[5]+"_"+split[6];
        return resouesta;
    }
    public static String convertRango(String rango)
    {
        String resultado = "";
        switch (rango)
        {
            case "R":
                resultado ="3";
                break;
            case "A":
                resultado ="2";
                break;
            case "D":
                resultado ="1";
                break;

        }

        return resultado;

    }
    public static String convertVariable(String variable)
    {
        String resultado = "";
        switch (variable)
        {
            case "D":
                resultado ="2";
                break;
            case "G":
                resultado ="5";
                break;
            case "P":
                resultado ="7";
                break;
            case "T":
                resultado ="8";
                break;
            case "N":
                resultado ="6";
                break;

        }

        return resultado;

    }
    public static String nombrarSobRec(String cuenta, String lector, String ciclo,String rango , String variable)
    {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        String yearInString = String.valueOf(year).substring(2);
        String monthInString = rellenarCerosIzquierda(String.valueOf(year),2);
        return  yearInString+monthInString+rellenarCerosIzquierda(ciclo,2)+"99"+rellenarCerosIzquierda(cuenta,8)+
                convertVariable(variable)+convertRango(rango);
    }
    public static String nombrarSobNuevo(String lector, String ciclo,String rango , String variable)
    {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int dia = now.get(Calendar.DAY_OF_MONTH);
        int hora = now.get(Calendar.HOUR_OF_DAY);
        int min = now.get(Calendar.MINUTE);
        String yearInString = String.valueOf(year).substring(2);
        String monthInString = rellenarCerosIzquierda(String.valueOf(month+1),2);
        String dayInString = rellenarCerosIzquierda(String.valueOf(dia),2);
        String hourInString = rellenarCerosIzquierda(String.valueOf(hora),2);
        String minInString = rellenarCerosIzquierda(String.valueOf(min),2);
        return  yearInString+monthInString+rellenarCerosIzquierda(ciclo,2)
                +"9"+rellenarCerosIzquierda(lector,3)
                +dayInString+hourInString+minInString+
                convertVariable(variable)+convertRango(rango);
    }
    public static String nombrarSobNuevoCuenta(String lector, String ciclo,String rango , String variable)
    {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int dia = now.get(Calendar.DAY_OF_MONTH);
        int hora = now.get(Calendar.HOUR_OF_DAY);
        int min = now.get(Calendar.MINUTE);
        String yearInString = String.valueOf(year).substring(2);
        String monthInString = rellenarCerosIzquierda(String.valueOf(month),2);
        String dayInString = rellenarCerosIzquierda(String.valueOf(dia),2);
        String hourInString = rellenarCerosIzquierda(String.valueOf(hora),2);
        String minInString = rellenarCerosIzquierda(String.valueOf(min),2);
        return dayInString+hourInString+minInString;

    }
}
