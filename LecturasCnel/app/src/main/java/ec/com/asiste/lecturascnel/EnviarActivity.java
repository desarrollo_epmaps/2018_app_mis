package ec.com.asiste.lecturascnel;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import ec.com.asiste.lecturascnel.Rest.postSendXML;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.resumenRecin;

public class EnviarActivity extends AppCompatActivity {
    ArrayList<resumenRecin> resumen = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar);
       startProgress();
    }
    public void startProgress() {



    }
    public void cargar()
    {
        sqLite ss = new sqLite();
        resumen = ss.resumen_sqlRIN();


    }

    public void accionS(View view)
    {

        sendd();
    }
    public void sendd()
    {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //new postSystemdata("").execute("", "", "");
                try {

                    for ( resumenRecin rrin: resumen
                            ) {
                        String xmll = fileManager.ReadFile(getApplicationContext(),Parametros.dir_files.toString(),rrin.getOrden()+".xml");
                        String[] parametross = {rrin.getUri(), xmll };
                        System.out.println("orden"+rrin.getOrden());
                        System.out.println("file"+xmll);
                        new postSendXML(parametross);

                    }
                    for ( String rr: fileManager.listaSobranter()
                    ) {

                        String xmll = fileManager.ReadFile(getApplicationContext(),Parametros.dir_files.toString(),rr);
                        String[] partes = rr.split("_");
                        String[] parametross = {partes[1].replace("!","/"), xmll };

                        new postSendXML(parametross);

                    }

                }
                catch (Exception e)
                {
                    int a = 0;
                }

            }
        });

    }
}
