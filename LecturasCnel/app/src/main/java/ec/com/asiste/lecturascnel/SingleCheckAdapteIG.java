package ec.com.asiste.lecturascnel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.historicoLecturas;
import com.asistecom.worker.web.entity.vistiasIn.historico;

import java.util.List;

import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.ResumenRuta;


public class SingleCheckAdapteIG extends RecyclerView.Adapter<SingleCheckAdapteIG.SingleCheckViewHolder> {

    private int mSelectedItem = -1;
    private List<historicoLecturas> mSingleCheckList;
    private Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    public SingleCheckAdapteIG(Context context, List<historicoLecturas> listItems) {
        mContext = context;
        mSingleCheckList = listItems;
    }

    public SingleCheckAdapteIG(InspecionGYEActivity context, List<historicoLecturas> mSingleCheckList) {
        mContext = Parametros.getContexto();
        mSingleCheckList = mSingleCheckList;
    }



    @Override
    public SingleCheckViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.item_single_check_ig, viewGroup, false);
        return new SingleCheckAdapteIG.SingleCheckViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(SingleCheckViewHolder viewHolder, final int position) {
        historicoLecturas item = mSingleCheckList.get(position);
        try {
            viewHolder.setDateToView(item, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mSingleCheckList.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public void onItemHolderClick(SingleCheckViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Parametros.res = (historico) mSingleCheckList.get(position);
        //lec.setText("mSingleCheckList.get(position).getLectura());
        //tv_r.setText("R: "+mSingleCheckList.get(position).getRuta());
        //Toast.makeText(InspecionGYEActivity.this, position + " - " + mSingleCheckList.get(position).getSector(), Toast.LENGTH_SHORT).show();
    }

    class SingleCheckViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private SingleCheckAdapteIG mAdapter;
        private TextView periodo, lectura, codd;

        public SingleCheckViewHolder(View itemView, final SingleCheckAdapteIG mAdapter) {
            super(itemView);
            this.mAdapter = mAdapter;

            periodo = (TextView) itemView.findViewById(R.id.tv_ig_resumen_periodo);
            lectura = (TextView) itemView.findViewById(R.id.tv_ig_resumen_lec);
            codd = (TextView) itemView.findViewById(R.id.tv_ig_resumen_codigos);
            itemView.setOnClickListener(this);

        }



        public void setDateToView( historicoLecturas item, int position) throws Exception {
            //mRadio.setChecked(position == mSelectedItem);
            lectura.setText(item.getLectura());
            codd.setText(item.getCodigo());
            periodo.setText(item.getPeriodo());
            //mText.setText(item.getTipo()+"-"+item.getSector()+"-"+item.getRuta()+"-TOTAL: "+item.total);
        }

        @Override
        public void onClick(View v) {
            mSelectedItem = getAdapterPosition();
            notifyItemRangeChanged(0, mSingleCheckList.size());
            mAdapter.onItemHolderClick(SingleCheckAdapteIG.SingleCheckViewHolder.this);
        }
    }

}
