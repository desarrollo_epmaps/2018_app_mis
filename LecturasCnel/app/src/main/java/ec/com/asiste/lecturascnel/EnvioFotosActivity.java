package ec.com.asiste.lecturascnel;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.Rest.postUploadFile;
import ec.com.asiste.lecturascnel.logica.Parametros;
import id.zelory.compressor.Compressor;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.fileManager.ListaFotos;
import static ec.com.asiste.lecturascnel.logica.fileManager.ListaFotosC;
import static ec.com.asiste.lecturascnel.logica.fileManager.contarFotos;
import static ec.com.asiste.lecturascnel.logica.fileManager.contarFotosC;
import static ec.com.asiste.lecturascnel.logica.fileManager.contarVideos;
import static ec.com.asiste.lecturascnel.logica.fileManager.contarVideosC;
import static ec.com.asiste.lecturascnel.logica.fileManager.deleteRecursive;

public class EnvioFotosActivity extends AppCompatActivity {

    public static TextView tv_log;
    public TextView tv_cantidad;
    public TextView tv_cantidadC;
    public TextView tv_cantidadV;
    public TextView tv_cantidadVC;
    public static ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envio_fotos);
        tv_cantidad = (TextView)findViewById(R.id.tv_ef_cantidad);
        tv_log = (TextView)findViewById(R.id.tv_ef_log);
        tv_cantidadC = (TextView)findViewById(R.id.tv_ef_cantidadC);
        tv_cantidadV = (TextView)findViewById(R.id.tv_ef_videos);
        tv_cantidadVC = (TextView)findViewById(R.id.tv_ef_videosC);
        pb =(ProgressBar)findViewById(R.id.progressBar_ef);
        pb.setVisibility(View.INVISIBLE);
                mostrar_fotos();
                startProgress();
    }


    public void setTextp(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pb.setVisibility(View.INVISIBLE);
                EnvioFotosActivity.tv_log.setText("TERMINADO..............");
            }
        });
    }
    public void mostrar_fotos()
    {
       tv_cantidad.setText(contarFotos().toString());
       tv_cantidadC.setText(contarFotosC().toString());
       tv_cantidadV.setText(contarVideos().toString());
       tv_cantidadVC.setText(contarVideosC().toString());

    }
    public void accion(View view) throws URISyntaxException {
        tv_log.setText("OPTIMIZANDO........");
        pb.setVisibility(View.VISIBLE);
        enviar_fotos();

    }
    public void enviar_fotos() throws URISyntaxException {

        //PASO 1 COMPRIMIR FOTOS PENDIENTES
        ArrayList<String> sin_comp = ListaFotos();
        for (final String ss : sin_comp
             ) {
            mostrar_fotos();
            if(ss.contains(".jpg")) {

                if (compress(ss)) {
                    File ff = new File(Parametros.dir_fotos + "/" + ss);
                    deleteRecursive(ff);
                }
                mostrar_fotos();
            }
            else  if(ss.contains(".mp4")) {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //new postSystemdata("").execute("", "", "");
                        try {
                            String photoURI =Parametros.getDir_fotos()+"/"+ss;

                            String destino = Parametros.getDir_fotosC();


                            String filePath = SiliCompressor.with(getApplicationContext()).compressVideo(photoURI, destino);


                            File forigen= new File(Parametros.dir_fotos + "/" + ss);
                            File fdestino = new File(filePath);
                            File fdestinoOk = new File(Parametros.dir_fotosC + "/" + ss);
                            fdestino.renameTo(fdestinoOk);

                            deleteRecursive(forigen);
                        }
                        catch (Exception e)
                        {
                            int a = 0;
                        }

                    }
                });



            }
            tv_log.setText("ENVIANDO........");
        }

        AsyncTask.execute(new Runnable() {
        @Override
        public void run() {
            //new postSystemdata("").execute("", "", "");

            ArrayList<String> listaf = ListaFotosC();
            new postUploadFile().execute( "","","");

        }
    });
        //PASO DOS ENVIAR FOTOS PENDIENTES

    }
    public void startProgress() {


        new Thread(new Task()).start();
    }

    class Task implements Runnable {
        @Override
        public void run() {
            while (true) {

                try {
                    Thread.sleep(100);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mostrar_fotos();
                        }
                    });

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }

    }
    public boolean compress(String file) {
        try {
            File actualImage = new File(Parametros.dir_fotos + "/" + file);
            File compressedImage;
            if (actualImage == null) {

            } else if(actualImage.getName().contains(".jpg")) {
                // Compress image in main thread using custom Compressor
                try {
                    compressedImage = new Compressor(this)
                            .setMaxWidth(1000)
                            .setMaxHeight(800)
                            .setQuality(90)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(Parametros.dir_fotosC)
                            .compressToFile(actualImage);

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

            }

            return true;
        }
        catch (Exception exxx)
        {
            return true;
        }
    }
}
