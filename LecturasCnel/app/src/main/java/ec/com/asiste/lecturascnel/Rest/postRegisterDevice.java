package ec.com.asiste.lecturascnel.Rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.asistecom.worker.web.entity.Resultado;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import ec.com.asiste.lecturascnel.logica.Parametros;


public class postRegisterDevice extends AsyncTask<String , Void , String> {

    public postRegisterDevice(String[] s) {
        doInBackground(s);
    }


    @Override
    protected String doInBackground(String... strings) {
        URL url = null;
        try {
            url = new URL(Parametros.ipInterna+ ":"+Parametros.puertoInterna+Parametros.servicioConfig+
                    "/registerDevice/CNEL"+strings[0]);
            //url = new URL("http://192.168.1.152:8080/wis/servicio/registerDevice"+strings[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
        connection.setDoOutput(true);
        connection.setConnectTimeout(30000);

        try {
            if (connection.getResponseCode() == 200  ) {
                //throw new BetterLuckNextTimeException(...);


            }
            else if (connection.getResponseCode() == 201   ) {
                //throw new BetterLuckNextTimeException(...);


            }
            else if (connection.getResponseCode() != 200  ) {
                //throw new BetterLuckNextTimeException(...);

                return "";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())

            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        String line = null;

        StringBuilder sb = new StringBuilder();
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (line != null) {
            sb.append(line);
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.disconnect();



        Serializer serializer = new Persister();

        //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");
        Resultado r = null;
        try {
            //Resources r = serializer.read(Resources.class, source);
            r = serializer.read(Resultado.class,sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        SharedPreferences preferencias = Parametros.getContexto().getSharedPreferences("datos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("token", r.mensaje);
        editor.putString("user", strings[1]);
        editor.putString("imei",  strings[2]);
        System.out.println("registrado okkkkkk " +r);


        editor.commit();
        System.out.print(sb.toString());
        return sb.toString();
    }
}
