package ec.com.asiste.lecturascnel;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.visitasOut.Parametro;

import java.util.ArrayList;
import java.util.List;

import ec.com.asiste.lecturascnel.Zebra.BluetoothDiscovery;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.ResumenRuta;

public class PrincipalActivity extends AppCompatActivity  implements AdapterView.OnItemClickListener {

    RecyclerView mRecyclerView;
    TextView tv_printer, tv_usr, tv_s,tv_r,tt;

    private List<ResumenRuta> mSingleCheckList  =  new ArrayList<>();
    private SingleCheckAdapter mAdapter;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        int a = 0;

        tv_printer.setText("   IMPRESORA: "+Parametros.getZebra());
        tv_usr.setText("   USR: "+Parametros.getNomberUsuario());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        tv_printer = (TextView) findViewById(R.id.tv_p_impresora);
        tv_usr = (TextView) findViewById(R.id.tv_p_lector);
        tv_s = (TextView) findViewById(R.id.tv_p_sector);
        tv_r = (TextView) findViewById(R.id.tv_p_ruta);
        mSingleCheckList = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_p_resumen);
        sqLite sq = new sqLite();
        mSingleCheckList = (ArrayList<ResumenRuta>)sq.resumen_sql();
        Parametros.resumen = mSingleCheckList;


        mAdapter = new SingleCheckAdapter(this, mSingleCheckList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        resumen();
    }

    public void resumen( )
    {


        try {


            sqLite sq = new sqLite();
            mSingleCheckList = (ArrayList<ResumenRuta>)sq.resumen_sql();
            Parametros.resumen = mSingleCheckList;


            mAdapter = new SingleCheckAdapter(this, mSingleCheckList);


            mRecyclerView.setAdapter(mAdapter);

            mAdapter.setOnItemClickListener(this);

                    }
        catch (Exception ex)
        {

            int a = 0;
        }

    }
    public void resumenRR(View view)
    {

        resumen();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Parametros.res = (ResumenRuta) mSingleCheckList.get(position);
        tv_s.setText("     SECTOR: "+mSingleCheckList.get(position).getSector());
        tv_r.setText("     RUTA: "+mSingleCheckList.get(position).getRuta());
        Toast.makeText(PrincipalActivity.this, position + " - "
                + mSingleCheckList.get(position).getSector(), Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal  , menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.bt_mp_descargar) {
            Toast.makeText(this,"Descargar",Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(this, DescargarActivity.class);
            startActivity(intent);
        }
        if (id==R.id.bt_mp_buscar) {
            Toast.makeText(this,"BUSCAR",Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(this, BluetoothDiscovery.class);
            startActivity(intent);
        }
        if (id==R.id.bt_mp_mapas) {
            Toast.makeText(this,"BUSCAR",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("geo:55.74274,37.56577?q=55.74274,37.56577 (punto)"));
// the following line should be used if you want use only Google maps
            intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
            startActivity(intent);
        }
        if (id==R.id.bt_mp_enviarfotos) {
            Toast.makeText(this,"ENVIO",Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(this, EnvioFotosActivity.class);
            startActivity(intent);
        }
        if (id==R.id.bt_mp_mapss) {

            Intent intent;
            intent = new Intent(this, SendActivity.class);
            startActivity(intent);
        }
        if (id==R.id.bt_mp_info) {

            Intent intent;
            intent = new Intent(this  , InfoActivity.class);
            startActivity(intent);
        }
        if (id==R.id.bt_mp_config) {

            Intent intent;
            intent = new Intent(this  , ConfigActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    public void leer (View view )
    {
        if (Parametros.res == null)
        {
            Toast.makeText(this,"Debe seleccionar una ruta",Toast.LENGTH_LONG).show();
        }
        else {
            if (Parametros.res.tipo.equals("LECTURA")) {
                Intent intent;
                intent = new Intent(this, LecturaActivity.class);
                startActivity(intent);
            } else if (Parametros.res.tipo.equals("ENTREGA")) {
                Intent intent;
                intent = new Intent(this, EntregaActivity.class);
                startActivity(intent);
            } else if (Parametros.res.tipo.equals("INDUSTRIALES")) {
                Intent intent;
                intent = new Intent(this, IndustrialesActivity.class);
                startActivity(intent);
            } else {
                Intent intent;
                intent = new Intent(this, InspecionGYEActivity.class);
                startActivity(intent);

            }

        }
    }

}
