package ec.com.asiste.lecturascnel;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ec.com.asiste.lecturascnel.dblite.AdminSQLiteOpenHelper;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;

public class InfoActivity extends AppCompatActivity {

    TextView v,g,u,ip,token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        v = (TextView)findViewById(R.id.tv_inf_version);
        g = (TextView)findViewById(R.id.tv_inf_global);
        u = (TextView)findViewById(R.id.tv_inf_usuarios);
        ip = (TextView)findViewById(R.id.tv_inf_ip);
        token = (TextView)findViewById(R.id.tv_inf_token);
        mostrar();
    }
    public void mostrarAcion(View view)
    {
        mostrar();
    }
    public void mostrar()
    {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(), "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        try {
            sqLite sql = new sqLite();
            //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
            Cursor fila = bd.rawQuery(
                    "select count(*) from global ", null);
            while (fila.moveToNext()) {
                g.setText(fila.getString(0));
            }
            fila.close();
            fila = bd.rawQuery(
                    "select count(*) from usuarios ", null);
            while (fila.moveToNext()) {
                u.setText(fila.getString(0));
            }
            fila.close();
            v.setText(Parametros.getVersion());
            ip.setText(Parametros.ipPublica);


            SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
            token.setText(preferencias.getString("token", "0").toString());

        } catch (Exception ex) {

        }

    }
}
