package com.asistecom.worker.web.entity.visitasOut;



import org.simpleframework.xml.Element;

public class visitas {

    @Element
    public OrdenLectura OrdenLectura;

    public com.asistecom.worker.web.entity.visitasOut.OrdenLectura getOrdenLectura() {
        return OrdenLectura;
    }

    public void setOrdenLectura(com.asistecom.worker.web.entity.visitasOut.OrdenLectura ordenLectura) {
        OrdenLectura = ordenLectura;
    }
}
