package com.asistecom.worker.web.entity.workblock;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class Works {
    @ElementList(name="work", inline=true)
    private List<Work> works;

    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
    }
}

