package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.Attribute;

public class marcaCNEL {

    @Attribute(required=false, name = "id")
    public String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override

    public String toString() {
        return id;
    }
}
