package com.asistecom.worker.web.entity.vistiasIn;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class historico {



    @Attribute(required=false)
    private String periodo;
    @Attribute(required=false)
    private String consumo;
    @Attribute(required=false)
    private String codigo;
    @Attribute(required=false)
    private String lectura;
    @Attribute(required=false)
    private String x;

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getConsumo() {
        return consumo;
    }

    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    @Attribute(required=false)
    private String y;

}
