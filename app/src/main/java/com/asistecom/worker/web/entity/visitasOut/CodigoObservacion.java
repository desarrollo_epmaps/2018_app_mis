package com.asistecom.worker.web.entity.visitasOut;


import org.simpleframework.xml.Attribute;

public class CodigoObservacion {

    @Attribute(required=false, name = "codigo")
    private String codigo;
    @Attribute(required=false, name = "tipo")
    private String tipo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}


