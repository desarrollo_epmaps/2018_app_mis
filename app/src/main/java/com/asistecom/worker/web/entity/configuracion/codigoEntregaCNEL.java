package com.asistecom.worker.web.entity.configuracion;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class codigoEntregaCNEL {
    @Attribute(required=false, name = "idEntrega")
    public String idEntrega;
    @Attribute(required=false, name = "descEntrega")
    public String descEntrega;
    @Attribute(required=false, name = "tipoEntrega")
    public String tipoEntrega;

    public String getIdEntrega() {
        return idEntrega;
    }

    @Override
    public String toString() {
        return idEntrega + '-' + descEntrega;
    }

    public void setIdEntrega(String idEntrega) {
        this.idEntrega = idEntrega;
    }

    public String getDescEntrega() {
        return descEntrega;
    }

    public void setDescEntrega(String descEntrega) {
        this.descEntrega = descEntrega;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }
}
