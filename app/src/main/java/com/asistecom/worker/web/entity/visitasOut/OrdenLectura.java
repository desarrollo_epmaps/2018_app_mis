package com.asistecom.worker.web.entity.visitasOut;

import com.asistecom.worker.web.entity.vistiasIn.parametro;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root
@Order(elements = {"ordenLectura","cuentaContrato","periodo","cecgsecu","ruta","secuencia","tipoEnvio","fechaLecturaActual","horaLecturaActual",
        "tipoActividad","observacionAlfanumerica","numeroDeInstalacion","numeroDeInstalacionPrincipal","idLector"})
public class OrdenLectura {

    public ArrayList<Parametro> getParametro() {
        return parametro;
    }

    public void setParametro(ArrayList<Parametro> parametro) {
        this.parametro = parametro;
    }

    @Element(required=false, data = false)
    private String numeroDeInstalacionPrincipal;

    @ElementList(name="parametros", entry="parametro",required = false)
    ArrayList<com.asistecom.worker.web.entity.visitasOut.Parametro> parametro = new ArrayList<>();

    @Element(required=false, data = false)
    private String ordenLectura;
    @Element(required=false, data = false)
    private String cuentaContrato;

    public boolean isEnServicioOut() {
        return enServicioOut;
    }

    public void setEnServicioOut(boolean enServicioOut) {
        this.enServicioOut = enServicioOut;
    }

    @Element(required=false, data = false)
    private boolean enServicioOut;

    @Element(required=false, data = false)
    private String idProyecto;

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    @Element(required=false, data = false)
    private String numFactura;

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    public String getNumeroDeInstalacionPrincipal() {
        return numeroDeInstalacionPrincipal;
    }

    public void setNumeroDeInstalacionPrincipal(String numeroDeInstalacionPrincipal) {
        this.numeroDeInstalacionPrincipal = numeroDeInstalacionPrincipal;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    @Element(required=false, data = false)
    private String ciclo;

    @Element(required=false, data = false)
    private String processedvisits;
    @Element(required=false, data = false)
    private String continuousCommunication;

    public String getProcessedvisits() {
        return processedvisits;
    }

    public void setProcessedvisits(String processedvisits) {
        this.processedvisits = processedvisits;
    }

    public String getContinuousCommunication() {
        return continuousCommunication;
    }

    public void setContinuousCommunication(String continuousCommunication) {
        this.continuousCommunication = continuousCommunication;
    }

    public String getClosedvisits() {
        return closedvisits;
    }

    public void setClosedvisits(String closedvisits) {
        this.closedvisits = closedvisits;
    }

    @Element(required=false, data = false)
    private String closedvisits;

    @Element(required=false, data = false)
    private String ruta;
    @Element(required=false, data = false)
    private String periodo;
    @Element(required=false, data = false)
    private String cecgsecu;
    @Element(required=false, data = false)
    private String secuencia;
    @Element(required=false, data = false)
    private String tipoEnvio;
    @Element(required=false, data = false)
    private EntregaFactura entregaFactura;
    @Element(required=false, data = false)
    private String numeroDeInstalacion;
    @Element(required=false, data = false)
    private String tipoEmisionFactura;
    @Element(required=false, data = false)
    private String sector;

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }
    @Element(required=false, data = false)
    private String intentosImpresion;

    public String getIntentosImpresion() {
        return intentosImpresion;
    }

    public void setIntentosImpresion(String intentosImpresion) {
        this.intentosImpresion = intentosImpresion;
    }

    public String getIntentosLectura() {
        return intentosLectura;
    }

    public void setIntentosLectura(String intentosLectura) {
        this.intentosLectura = intentosLectura;
    }

    @Element(required=false, data = false)
    private String intentosLectura;




    @Element(required=false, data = false)
    private String observacionAlfanumerica;
    @Element(required=false, data = false)
    private String claseLectura;
    @Element(required=false, data = false)
    private String lecturaActual;
    @Element(required=false, data = false)
    private String lecturaReal;
    @Element(required=false, data = false)
    private String fechaLecturaActual;
    @Element(required=false, data = false)
    private String tipoActividad;
    @Element(required=false, data = false)
    private boolean sobrante;
    @Element(required=false)
    private medidor medidor;

    public com.asistecom.worker.web.entity.visitasOut.medidor getMedidor() {
        return medidor;
    }

    public void setMedidor(com.asistecom.worker.web.entity.visitasOut.medidor medidor) {
        this.medidor = medidor;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public boolean isSobrante() {
        return sobrante;
    }

    public void setSobrante(boolean sobrante) {
        this.sobrante = sobrante;
    }

    @Element(required=false, data = false)
    private String horaLecturaActual;

    public List<String> getFoto() {
        return foto;
    }

    public void setFoto(ArrayList<String> foto) {
        this.foto = foto;
    }

    @ElementList(name="fotografias", entry="foto")
    ArrayList<String> foto = new ArrayList<String>();




    @Element(required=false, data = false)
    private String idLector;
    @Element(required=false, data = false)
    private String version;

    public String getOrdenLectura() {
        return ordenLectura;
    }

    public void setOrdenLectura(String ordenLectura) {
        this.ordenLectura = ordenLectura;
    }

    public String getCuentaContrato() {
        return cuentaContrato;
    }

    public void setCuentaContrato(String cuentaContrato) {
        this.cuentaContrato = cuentaContrato;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCecgsecu() {
        return cecgsecu;
    }

    public void setCecgsecu(String cecgsecu) {
        this.cecgsecu = cecgsecu;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public EntregaFactura getEntregaFactura() {
        return entregaFactura;
    }

    public void setEntregaFactura(EntregaFactura entregaFactura) {
        this.entregaFactura = entregaFactura;
    }

    public String getNumeroDeInstalacion() {
        return numeroDeInstalacion;
    }

    public void setNumeroDeInstalacion(String numeroDeInstalacion) {
        this.numeroDeInstalacion = numeroDeInstalacion;
    }

    public String getTipoEmisionFactura() {
        return tipoEmisionFactura;
    }

    public void setTipoEmisionFactura(String tipoEmisionFactura) {
        this.tipoEmisionFactura = tipoEmisionFactura;
    }

    @ElementList(name="codigosObservacion", entry="codigoObservacion")
    ArrayList<CodigoObservacion> codigoObservacion = new ArrayList<>();


    @ElementList(name="NotificacionesEntregadas", entry="notificacion")
    ArrayList<Notificacion> notificacion = new ArrayList<Notificacion>();

    public List<Notificacion> getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(ArrayList<Notificacion> notificacion) {
        this.notificacion = notificacion;
    }

    @ElementList(name="mantenimientos", entry="idMantenimientoLiviano")
    ArrayList<String> idMantenimientoLiviano = new ArrayList<String>();

    public ArrayList<String> getIdMantenimientoLiviano() {
        return idMantenimientoLiviano;
    }

    public void setIdMantenimientoLiviano(ArrayList<String> idMantenimientoLiviano) {
        this.idMantenimientoLiviano = idMantenimientoLiviano;
    }

    public List<Coordenada> getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(ArrayList<Coordenada> coordenada) {
        this.coordenada = coordenada;
    }

    @ElementList(name="coordenadas", entry="coordenada")
    ArrayList<Coordenada> coordenada = new ArrayList<Coordenada>();


    public ArrayList<CodigoObservacion> getCodigoObservacion() {
        return codigoObservacion;
    }

    public void setCodigoObservacion(ArrayList<CodigoObservacion> codigoObservacion) {
        this.codigoObservacion = codigoObservacion;
    }

    public String getObservacionAlfanumerica() {
        return observacionAlfanumerica;
    }

    public void setObservacionAlfanumerica(String observacionAlfanumerica) {
        this.observacionAlfanumerica = observacionAlfanumerica;
    }

    public String getClaseLectura() {
        return claseLectura;
    }

    public void setClaseLectura(String claseLectura) {
        this.claseLectura = claseLectura;
    }

    public String getLecturaActual() {
        return lecturaActual;
    }

    public void setLecturaActual(String lecturaActual) {
        this.lecturaActual = lecturaActual;
    }

    public String getLecturaReal() {
        return lecturaReal;
    }

    public void setLecturaReal(String lecturaReal) {
        this.lecturaReal = lecturaReal;
    }

    public String getFechaLecturaActual() {
        return fechaLecturaActual;
    }

    public void setFechaLecturaActual(String fechaLecturaActual) {
        this.fechaLecturaActual = fechaLecturaActual;
    }

    public String getHoraLecturaActual() {
        return horaLecturaActual;
    }

    public void setHoraLecturaActual(String horaLecturaActual) {
        this.horaLecturaActual = horaLecturaActual;
    }

    public String getIdLector() {
        return idLector;
    }

    public void setIdLector(String idLector) {
        this.idLector = idLector;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public OrdenLectura() {
        this.ordenLectura = ordenLectura;

        entregaFactura = new EntregaFactura();

    }
}
