package com.asistecom.worker.web.entity.vistiasIn;

import org.simpleframework.xml.Attribute;

public class parametro {

    @Attribute(required=false)
    private String tipo;
    @Attribute(required=false)
    private String atributo;
    @Attribute(required=false)
    private String lectura;

    public String getOrdenTrabajo() {
        return ordenTrabajo;
    }

    public void setOrdenTrabajo(String ordenTrabajo) {
        this.ordenTrabajo = ordenTrabajo;
    }

    @Attribute(required=false)
    private String ordenTrabajo;


    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo = atributo;
    }
}
