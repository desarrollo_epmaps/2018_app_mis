package com.asistecom.worker.web.entity.workblock;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Finished {
    @Attribute
    public String path;
}
