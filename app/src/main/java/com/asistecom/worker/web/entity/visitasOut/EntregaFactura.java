package com.asistecom.worker.web.entity.visitasOut;

import android.text.Editable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class EntregaFactura {

    @Element(required=false, data = false)
    private String idEntregaFactura;
    @Element(required=false, data = false)
    private String fechaEntrega;

    public EntregaFactura() {
    }

    @Element(required=false, data = false)
    private String horaEntrega;

    public String getIdEntregaFactura() {
        return idEntregaFactura;
    }

    public void setIdEntregaFactura(String idEntregaFactura) {
        this.idEntregaFactura = idEntregaFactura;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getHoraEntrega() {
        return horaEntrega;
    }

    public void setHoraEntrega(String horaEntrega) {
        this.horaEntrega = horaEntrega;
    }



}

