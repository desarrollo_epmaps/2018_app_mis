package com.asistecom.worker.web.entity.visitasOut;


import org.simpleframework.xml.Element;

public class Coordenada {

    @Element(required=false, data = false)
    private String coord_X = "";
    @Element(required=false, data = false)
    private String coord_Y = "";

    public String getCoord_X() {
        return coord_X;
    }

    public void setCoord_X(String coord_X) {
        this.coord_X = coord_X;
    }

    public String getCoord_Y() {
        return coord_Y;
    }

    public void setCoord_Y(String coord_Y) {
        this.coord_Y = coord_Y;
    }
}
