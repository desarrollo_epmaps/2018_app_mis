package com.asistecom.worker.web.entity.indstriales;

public class lecturasIndustrias {

    public String parametro;
    public String unidad;
    public String valor;

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getOrdeneLectura() {
        return ordeneLectura;
    }

    public void setOrdeneLectura(String ordeneLectura) {
        this.ordeneLectura = ordeneLectura;
    }

    public String ordeneLectura;

}
