package com.asistecom.worker.web.entity.vistiasIn;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class informacionAdicional {
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getImprimir() {
        return imprimir;
    }

    public void setImprimir(String imprimir) {
        this.imprimir = imprimir;
    }

    @Element(required=false, data = false)
    private String nombreCliente;
    @Element(required=false, data = false)
    private String numeroTelefono;
    @Element(required=false, data = false)
    private String imprimir;
}
