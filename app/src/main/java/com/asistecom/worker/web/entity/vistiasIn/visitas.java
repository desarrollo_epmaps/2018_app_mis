package com.asistecom.worker.web.entity.vistiasIn;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class visitas {

    @Element
    public OrdenLectura ordenLectura;


    @Override
    public String toString() {
        return "C: "+ordenLectura.cuentaContrato + " S: " +ordenLectura.numeroMedidor  + " M: " + ordenLectura.modelo + "--"+ordenLectura.tipoInstalacion;
    }


    public visitas()
    {
        ordenLectura = new OrdenLectura();
    }
}
