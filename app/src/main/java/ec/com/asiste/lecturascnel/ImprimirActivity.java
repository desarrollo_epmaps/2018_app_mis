package ec.com.asiste.lecturascnel;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ImprimirActivity extends AppCompatActivity {

    final Context context = this;
    private Button buttonDES;
    TextView evento;
    private Button btnBuscarDispositivo;
    private BluetoothAdapter bAdapter;
    private ArrayList<BluetoothDevice> arrayDevices;

    private ListView lvDispositivos ;
    ArrayList<String> mArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imprimir);
        evento = (TextView)findViewById(R.id.tv_imp_event);
        evento.setText("BUSCANDO");
        sendFile();

    }
    public void accionSalir(View view)
    {

        finish();
    }
    public void accionReimp(View view)
    {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.desbloqueo_reimpresion, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.et_des_codigo);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                //result.setText(userInput.getText());
                                sendFile();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }


    private void sendFile() {
        Connection connection = null;

        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);



        connection = new BluetoothConnection(prefe.getString("zebra",""));
        evento.setText("CONECTANDO");
        try {
            //helper.showLoadingDialog("Sending file to printer ...");
            connection.open();
            evento.setText("CONECTADO");
            ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);

            testSendFile(printer);
            connection.close();
            evento.setText("FINALIZADO");
        } catch (ConnectionException e) {
            //helper.showErrorDialogOnGuiThread(e.getMessage());
        } catch (ZebraPrinterLanguageUnknownException e) {
            // helper.showErrorDialogOnGuiThread(e.getMessage());
        } finally {
            //helper.dismissLoadingDialog();
        }
    }
    private void testSendFile(ZebraPrinter printer) {
        try {
            File filepath = getFileStreamPath("TEST.LBL");
            createDemoFile(printer, "TEST.LBL");
            printer.sendFileContents(filepath.getAbsolutePath());

        } catch (ConnectionException e1) {
            //helper.showErrorDialogOnGuiThread("Error sending file to printer");
        } catch (IOException e) {
            // helper.showErrorDialogOnGuiThread("Error creating file");
        }
    }


    private void createDemoFile(ZebraPrinter printer, String fileName) throws IOException {

        evento.setText("IMPRIMIENDO");
        FileOutputStream os = this.openFileOutput(fileName, Context.MODE_PRIVATE);

        byte[] configLabel = null;

        PrinterLanguage pl = printer.getPrinterControlLanguage();
        if (pl == PrinterLanguage.ZPL) {
            configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
            configLabel = "^XA\\r\\n^MMC\\r\\n^PW799\\r\\n^LL2264\\r\\n^LS0\\r\\n^FO512,1696^GFA,01536,01536,00024,:Z64:\\r\\neJytVEFrE0EY/XbTkFhhs4vGKNTuGi8hSNVW6CpLNkLAi4IFgyep0IsnyYK6KWzcbSu4in9BiJ5KEPEHiN0q0otKL6IHg6Me9FRmEWnA2PX7tjG25FLEb5jM5PF4vHnzzQL8Uw1t2VfL9FvUmmmtOCw2xYfNGBZb8WpPWiPW0mQqXLE5/pVlN3JlGWA5v0s65Gmg3QMt5g+3Wh4uyycO7DuYO5G0VpKXCU6ttKsBrnPa7nHTHE2whduM8AxrtRYBhOVjByYsIQXHoUZ8QQ1du4Gbz+NK/tDcA0+7pcU6pW/FVgvXm5OW1ZjOpdbXz8Q6btRov8O1oHmamJfE/OH52M/PjWJsCL0KMi0KuQcl+uVG66sDgYhrpN8cwFNR92P7Eh/ElzqtmF9T9yYvONlytqZCjvihctGuAdQlI9PVVXa0Y3quB9Iaw4SQ74w6atdxOY5PUQBqxCFZRT+6pJe6ussjHrGfDHE83SbfcAznmhVaYRhyMKNVUOzzAGNrR+pGgZ9kjH1nDMYe41mHcbpd4tdmmcXCZYZ8NJvE6zY3jLqh89kmj/nmEh0ap+kg7tSnYv0yxomYglnpI4gX6sA4C9HK1bXewUdGHCqBhwztgLtB3VTB+9FJv0R83unhVGgf9U3hhxVyR0ZcxqtAfQn5R/SMt4H8Euo/7vPJfyZwQ8bVLfpSrJ9gJeRnPHCf9fhqnfwneAPTyQXgqj3/m3xg5CcRwFWpzyf7wNE/Fxg4SdIv9/kd4iNu/LnoXI30a7OYDuVpTGEXKdhQGW5cwfyvYP5hwKDQbwzuoP/zs3wzfxmtA83UE+OUodc7HPP8wrY0EjfcrtPgEeqvB6BksV9JP8V086leYlGHMeyTLXzqn2nMh3M3wGgqUKmgf1EriIuF3V5ea2rFba0q48jSC6BSpqo46AHvsCpUO2aDkK1ms8rmvgS+5C/qY4mvbBvH3v/+3OvuDTt5Hd/J0IXYPtWM7gd3/Hn8qmjb+Kez7Zft+y+swEK+8td+MH/3jf9oAbx8czt/z4fy2VcvhKBGOkN9+zMTfuDfez6gbyvtt+0P9iTQu8X0/+Bp8Hw/nU6D6O08gMFCt5Q8xLM3/lv9Bljoch4=:6876\\r\\n^FT491,1886^A0I,11,12^FH\\^FD@P13^FS\\r\\n^FT513,1885^A0I,11,12^FH\\^FD@P12^FS\\r\\n^FT534,1885^A0I,11,12^FH\\^FD@P11^FS\\r\\n^FT556,1885^A0I,11,12^FH\\^FD@P10^FS\\r\\n^FT578,1885^A0I,11,12^FH\\^FD@P09^FS\\r\\n^FT601,1885^A0I,11,12^FH\\^FD@P08^FS\\r\\n^FT623,1885^A0I,11,12^FH\\^FD@P07^FS\\r\\n^FT646,1885^A0I,11,12^FH\\^FD@P06^FS\\r\\n^FT670,1885^A0I,11,12^FH\\^FD@P05^FS\\r\\n^FT694,1885^A0I,11,12^FH\\^FD@P04^FS\\r\\n^FT492,2018^A0I,11,12^FH\\^FD@C13^FS\\r\\n^FT515,2018^A0I,11,12^FH\\^FD@C12^FS\\r\\n^FT536,2018^A0I,11,12^FH\\^FD@C11^FS\\r\\n^FT558,2018^A0I,11,12^FH\\^FD@C10^FS\\r\\n^FT580,2018^A0I,11,12^FH\\^FD@C09^FS\\r\\n^FT601,2018^A0I,11,12^FH\\^FD@C08^FS\\r\\n^FT624,2018^A0I,11,12^FH\\^FD@C07^FS\\r\\n^FT648,2018^A0I,11,12^FH\\^FD@C06^FS\\r\\n^FT717,1885^A0I,11,12^FH\\^FD@P03^FS\\r\\n^FT741,1885^A0I,11,12^FH\\^FD@P02^FS\\r\\n^FT85,2469^A0I,23,24^FH\\^FD@valorAPagar^FS\\r\\n^FT354,2469^A0I,23,24^FH\\^FD@fechaVencimiento^FS\\r\\n^FT413,2516^A0I,17,16^FH\\^FD@numAutorizacion^FS\\r\\n^FT274,2564^A0I,17,16^FH\\^FD@fechaVencimiento^FS\\r\\n^FT304,2587^A0I,17,16^FH\\^FD@fechaEmision^FS\\r\\n^FT305,2611^A0I,17,16^FH\\^FD@numDocumento^FS\\r\\n^FT413,2610^A0I,17,16^FH\\^FD@etiquetaDoc^FS\\r\\n^FT443,2022^A0I,17,16^FH\\^FD1.1 SERVICIO ELECTRICO (SE) Y SAPG^FS\\r\\n^FT772,1582^A0I,17,16^FH\\^FD3. PLANES DE FINANCIAMIENTO AUTORIZADOS POR EL CONSUMIDOR ^FS\\r\\n^FT771,1669^A0I,17,16^FH\\^FD2. VALORES PENDIENTES^FS\\r\\n^FT771,2218^A0I,17,16^FH\\^FD1. FACTURACION SERVICIO ELECTRICO Y ALUMBRADO PUBLICO^FS\\r\\n^FT771,2439^A0I,17,16^FH\\^FDINFORMACI\\E3N DEL CONSUMIDOR^FS\\r\\n^FT84,2500^A0I,17,16^FH\\^FDA Pagar^FS\\r\\n^FT354,2498^A0I,17,16^FH\\^FDFecha Vencimiento ^FS\\r\\n^FT414,2540^A0I,17,16^FH\\^FDN\\A3mero Autorizaci\\A2n: ^FS\\r\\n^FT336,2681^A0I,17,16^FH\\^FD@dirEstablecimiento^FS\\r\\n^FT413,2564^A0I,17,16^FH\\^FDFecha Vencimiento: ^FS\\r\\n^FT412,2634^A0I,17,16^FH\\^FD@telefono^FS\\r\\n^FT414,2587^A0I,17,16^FH\\^FDFecha Emisi\\A2n: ^FS\\r\\n^FT411,2657^A0I,17,16^FH\\^FD@ciudad^FS\\r\\n^FT413,2681^A0I,17,16^FH\\^FDSucursal: ^FS\\r\\n^FT672,2018^A0I,11,12^FH\\^FD@C05^FS\\r\\n^FT766,1885^A0I,11,12^FH\\^FD@P01^FS\\r\\n^FT695,2018^A0I,11,12^FH\\^FD@C04^FS\\r\\n^FT719,2018^A0I,11,12^FH\\^FD@C03^FS\\r\\n^FT742,2018^A0I,11,12^FH\\^FD@C02^FS\\r\\n^FT767,2018^A0I,11,12^FH\\^FD@C01^FS\\r\\n^FT64,1805^A0I,17,16^FH\\^FD@rvalor_rubro11^FS\\r\\n^FT442,1806^A0I,17,16^FH\\^FD@rubro11^FS\\r\\n^FT64,1767^A0I,17,16^FH\\^FD@rvalor_rubro13^FS\\r\\n^FT65,1863^A0I,17,16^FH\\^FD@rvalor_rubro_8^FS\\r\\n^FT443,1864^A0I,17,16^FH\\^FD@rubro_8^FS\\r\\n^FT65,1825^A0I,17,16^FH\\^FD@rvalor_rubro10^FS\\r\\n^FT442,1768^A0I,17,16^FH\\^FD@rubro13^FS\\r\\n^FT526,1764^A0I,17,16^FH\\^FD@st_valor_total^FS\\r\\n^FT767,1764^A0I,17,16^FH\\^FD@st_total^FS\\r\\n^FT767,1780^A0I,14,14^FH\\^FD@s6_subsidio^FS\\r\\n^FT767,1794^A0I,14,14^FH\\^FD@s5_subsidio^FS\\r\\n^FT767,1808^A0I,14,14^FH\\^FD@s4_subsidio^FS\\r\\n^FT767,1823^A0I,14,14^FH\\^FD@s3_subsidio^FS\\r\\n^FT768,1837^A0I,14,14^FH\\^FD@s2_subsidio^FS\\r\\n^FT526,1780^A0I,14,14^FH\\^FD@s6_valor^FS\\r\\n^FT526,1794^A0I,14,14^FH\\^FD@s5_valor^FS\\r\\n^FT526,1808^A0I,14,14^FH\\^FD@s4_valor^FS\\r\\n^FT526,1822^A0I,14,14^FH\\^FD@s3_valor^FS\\r\\n^FT526,1836^A0I,14,14^FH\\^FD@s2_valor^FS\\r\\n^FT526,1850^A0I,14,14^FH\\^FD@s1_valor^FS\\r\\n^FT768,1850^A0I,14,14^FH\\^FD@s1_subsidio^FS\\r\\n^FT443,1826^A0I,17,16^FH\\^FD@rubro10^FS\\r\\n^FT64,1786^A0I,17,16^FH\\^FD@rvalor_rubro12^FS\\r\\n^FT67,1385^A0I,20,19^FH\\^FD@valor_tablaResValPag_1^FS\\r\\n^FT65,1901^A0I,17,16^FH\\^FD@rvalor_rubro_6^FS\\r\\n^FT769,1387^A0I,20,19^FH\\^FD@concepto_tablaResValPag_1 (1+2+3) ^FS\\r\\n^FT65,1882^A0I,17,16^FH\\^FD@rvalor_rubro_7^FS\\r\\n^FT65,1844^A0I,17,16^FH\\^FD@rvalor_rubro_9^FS\\r\\n^FT67,1323^A0I,17,16^FH\\^FDTIEMPO^FS\\r\\n^FT213,1323^A0I,17,16^FH\\^FDPLAZO^FS\\r\\n^FT67,1300^A0I,17,16^FH\\^FD@tiempoForma_pago^FS\\r\\n^FT213,1299^A0I,17,16^FH\\^FD@plazoForma_pago^FS\\r\\n^FT309,1298^A0I,17,16^FH\\^FD@valorForma_pago^FS\\r\\n^FT769,1235^A0I,17,16^FH\\^FD@mensajes^FS\\r\\n^FT768,1299^A0I,17,16^FH\\^FD@descForma_pago^FS\\r\\n^FT308,1322^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT768,1323^A0I,17,16^FH\\^FDFORMA DE PAGO^FS\\r\\n^FT442,1787^A0I,17,16^FH\\^FD@rubro12^FS\\r\\n^FT443,1902^A0I,17,16^FH\\^FD@rubro_6^FS\\r\\n^FT65,1939^A0I,17,16^FH\\^FD@rvalor_rubro_4^FS\\r\\n^FT443,1883^A0I,17,16^FH\\^FD@rubro_7^FS\\r\\n^FT697,1865^A0I,14,14^FH\\^FDSUBSIDIOS DEL GOBIERNO^FS\\r\\n^FT443,1845^A0I,17,16^FH\\^FD@rubro_9^FS\\r\\n^FT443,1940^A0I,17,16^FH\\^FD@rubro_4^FS\\r\\n^FT66,560^A0I,17,16^FH\\^FD@t3_valorTercero^FS\\r\\n^FT68,766^A0I,17,16^FH\\^FD@t2_valorTercero^FS\\r\\n^FT371,562^A0I,17,16^FH\\^FD@t3_conceptoValorTercero^FS\\r\\n^FT68,971^A0I,17,16^FH\\^FD@t1_valorTercero^FS\\r\\n^FT372,767^A0I,17,16^FH\\^FD@t2_conceptoValorTercero^FS\\r\\n^FT67,1636^A0I,20,19^FH\\^FD@valorTotal_rubro_2^FS\\r\\n^FT373,973^A0I,17,16^FH\\^FD@t1_conceptoValorTercero^FS\\r\\n^FT67,1420^A0I,17,16^FH\\^FD@valorTotal_rubro_2^FS\\r\\n^FT67,1440^A0I,17,16^FH\\^FD@valorTotal_rubro_3^FS\\r\\n^FT67,1554^A0I,20,19^FH\\^FD@valorTotal_rubro_3^FS\\r\\n^FT769,1637^A0I,20,19^FH\\^FD@tablaTotal_rubro_2^FS\\r\\n^FT769,1441^A0I,17,16^FH\\^FD@tablaTotal_rubro_3^FS\\r\\n^FT770,1421^A0I,17,16^FH\\^FD@tablaTotal_rubro_2^FS\\r\\n^FT769,1556^A0I,20,19^FH\\^FD@tablaTotal_rubro_3^FS\\r\\n^FT369,528^A0I,17,16^FH\\^FD@t3_etiqueta^FS\\r\\n^FT65,526^A0I,17,16^FH\\^FD@t3_valorRecaudacion^FS\\r\\n^FT493,1262^A0I,17,16^FH\\^FDMENSAJE AL CONSUMIDOR^FS\\r\\n^FT371,580^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT524,1492^A0I,17,16^FH\\^FDTOTAL A PAGAR^FS\\r\\n^FT371,734^A0I,17,16^FH\\^FD@t2_etiqueta^FS\\r\\n^FT66,579^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT66,732^A0I,17,16^FH\\^FD@t2_valorRecaudacion^FS\\r\\n^FT371,939^A0I,17,16^FH\\^FD@t1_etiqueta^FS\\r\\n^FT761,465^A0I,17,16^FH\\^FD@t3_razonSocialComprador^FS\\r\\n^FT552,488^A0I,17,16^FH\\^FD@t3_identificacionComprador^FS\\r\\n^FT761,488^A0I,17,16^FH\\^FD@t3_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT544,399^A0I,17,16^FH\\^FDRESUMEN DE VALORES A PAGAR^FS\\r\\n^FT61,343^A0I,17,16^FH\\^FD@valor_tablaResValPag_2^FS\\r\\n^FT60,368^A0I,17,16^FH\\^FD@valor_tablaResValPag_1^FS\\r\\n^FT63,310^A0I,17,16^FH\\^FD@totalAPagar^FS\\r\\n^FT774,306^A0I,20,19^FH\\^FDTOTAL A PAGAR (USD)^FS\\r\\n^FT772,344^A0I,17,16^FH\\^FD@concepto_tablaResValPag_2^FS\\r\\n^FT772,368^A0I,17,16^FH\\^FD@concepto_tablaResValPag_1^FS\\r\\n^FT761,441^A0I,17,16^FH\\^FD@t3_direccionSuministro^FS\\r\\n^FT372,786^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT762,508^A0I,17,16^FH\\^FD@t3_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT373,992^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT67,937^A0I,17,16^FH\\^FD@t1_valorRecaudacion^FS\\r\\n^FT762,529^A0I,17,16^FH\\^FD@t3_fechaEmision_recaudacionTerceros^FS\\r\\n^FT68,785^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT68,990^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT66,1461^A0I,17,16^FH\\^FD@valorTotal_rubro_1^FS\\r\\n^FT762,671^A0I,17,16^FH\\^FD@t2_razonSocialComprador^FS\\r\\n^FT553,693^A0I,17,16^FH\\^FD@t2_identificacionComprador^FS\\r\\n^FT763,694^A0I,17,16^FH\\^FD@t2_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT763,591^A0I,17,16^FH\\^FD@t3_canton^FS\\r\\n^FT762,552^A0I,17,16^FH\\^FD@t3_rucBenef^FS\\r\\n^FT762,647^A0I,17,16^FH\\^FD@t2_direccionSuministro^FS\\r\\n^FT763,610^A0I,17,16^FH\\^FD@sustentoLegal_3^FS\\r\\n^FT768,1463^A0I,17,16^FH\\^FD@tablaTotal_rubro_1^FS\\r\\n^FT764,714^A0I,17,16^FH\\^FD@t2_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT558,1072^A0I,17,16^FH\\^FDORDEN DE COBRO POR CUENTA DE TERCEROS^FS\\r\\n^FT763,572^A0I,17,16^FH\\^FD@t3_benef^FS\\r\\n^FT65,1920^A0I,17,16^FH\\^FD@rvalor_rubro_5^FS\\r\\n^FT764,735^A0I,17,16^FH\\^FD@t2_fechaEmision_recaudacionTerceros^FS\\r\\n^FT65,1977^A0I,17,16^FH\\^FD@rvalor_rubro_2^FS\\r\\n^FT767,1353^A0I,17,16^FH\\^FDFORMA DE PAGO^FS\\r\\n^FT763,852^A0I,17,16^FH\\^FD@t1_direccionSuministro^FS\\r\\n^FT763,876^A0I,17,16^FH\\^FD@t1_razonSocialComprador^FS\\r\\n^FT545,899^A0I,17,16^FH\\^FD@t1_identificacionComprador^FS\\r\\n^FT763,899^A0I,17,16^FH\\^FD@t1_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT764,797^A0I,17,16^FH\\^FD@t2_canton^FS\\r\\n^FT763,758^A0I,17,16^FH\\^FD@t2_rucBenef^FS\\r\\n^FT65,1958^A0I,17,16^FH\\^FD@rvalor_rubro_3^FS\\r\\n^FT764,816^A0I,17,16^FH\\^FD@sustentoLegal_2^FS\\r\\n^FT764,919^A0I,17,16^FH\\^FD@t1_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT765,1002^A0I,17,16^FH\\^FD@t1_canton^FS\\r\\n^FT765,940^A0I,17,16^FH\\^FD@t1_fechaEmision_recaudacionTerceros^FS\\r\\n^FT764,778^A0I,17,16^FH\\^FD@t2_benef^FS\\r\\n^FT764,963^A0I,17,16^FH\\^FD@t1_rucBenef^FS\\r\\n^FT765,983^A0I,17,16^FH\\^FD@t1_benef^FS\\r\\n^FT443,1921^A0I,17,16^FH\\^FD@rubro_5^FS\\r\\n^FT765,1021^A0I,17,16^FH\\^FD@sustentoLegal_1^FS\\r\\n^FT757,1043^A0I,17,16^FH\\^FDESTOS VALORES NO FORMAN PARTE DE LOS INFGRESOS DE LA EMPRESA ELECTRICA^FS\\r\\n^FT65,1996^A0I,17,16^FH\\^FD@rvalor_rubro_1^FS\\r\\n^FT443,1978^A0I,17,16^FH\\^FD@rubro_2^FS\\r\\n^FT443,1959^A0I,17,16^FH\\^FD@rubro_3^FS\\r\\n^FT443,1997^A0I,17,16^FH\\^FD@rubro_1^FS\\r\\n^FT198,2298^A0I,17,16^FH\\^FD@parroquia^FS\\r\\n^FT74,2054^A0I,17,16^FH\\^FD@valores^FS\\r\\n^FT191,2054^A0I,17,16^FH\\^FD@unidad^FS\\r\\n^FT278,2052^A0I,17,16^FH\\^FD@cons_mensual^FS\\r\\n^FT362,2052^A0I,17,16^FH\\^FD@anterior^FS\\r\\n^FT764,2053^A0I,17,16^FH\\^FD@descripcion^FS\\r\\n^FT440,2053^A0I,17,16^FH\\^FD@actual^FS\\r\\n^FT443,2145^A0I,17,16^FH\\^FD@fechaHasta^FS\\r\\n^FT713,2144^A0I,17,16^FH\\^FD@fechaDesde^FS\\r\\n^FT163,2129^A0I,17,16^FH\\^FD@penBajoFactPot^FS\\r\\n^FT164,2152^A0I,17,16^FH\\^FD@factorPotencia^FS\\r\\n^FT163,2176^A0I,17,16^FH\\^FD@factorCorr^FS\\r\\n^FT452,2169^A0I,17,16^FH\\^FD@diasFact^FS\\r\\n^FT660,2168^A0I,17,16^FH\\^FD@tipoCons^FS\\r\\n^FT695,2190^A0I,17,16^FH\\^FD@numeroMed^FS\\r\\n^FT633,2252^A0I,17,16^FH\\^FD@direccionEnvio^FS\\r\\n^FT632,2275^A0I,17,16^FH\\^FD@direccionSuministro^FS\\r\\n^FT479,2296^A0I,17,16^FH\\^FD@canton^FS\\r\\n^FT692,2297^A0I,17,16^FH\\^FD@provincia^FS\\r\\n^FT546,2320^A0I,17,16^FH\\^FD@tarifa^FS\\r\\n^FT548,2342^A0I,17,16^FH\\^FD@cuen^FS\\r\\n^FT549,2363^A0I,17,16^FH\\^FD@identificacionComprador^FS\\r\\n^FT549,2385^A0I,17,16^FH\\^FD@razonSocialComprador^FS\\r\\n^FT550,2403^A0I,20,19^FH\\^FD@cuentaContrato^FS\\r\\n^FT279,2297^A0I,17,16^FH\\^FDParroquia : ^FS\\r\\n^FT544,2295^A0I,17,16^FH\\^FDCant\\A2n : ^FS\\r\\n^FT271,2129^A0I,17,16^FH\\^FDPenalizaci\\A2n : ^FS\\r\\n^FT499,2146^A0I,17,16^FH\\^FDHasta : ^FS\\r\\n^FT271,2152^A0I,17,16^FH\\^FDF. Potencia : ^FS\\r\\n^FT74,2078^A0I,17,16^FH\\^FDVALORES^FS\\r\\n^FT271,2174^A0I,17,16^FH\\^FDF. Correcci\\A2n : ^FS\\r\\n^FT191,2077^A0I,17,16^FH\\^FDUND^FS\\r\\n^FT279,2099^A0I,17,16^FH\\^FDCONSUMO^FS\\r\\n^FT279,2077^A0I,17,16^FH\\^FDMENSUAL^FS\\r\\n^FT363,2098^A0I,17,16^FH\\^FDLECTURA^FS\\r\\n^FT765,2077^A0I,17,16^FH\\^FDDESCRIPCION^FS\\r\\n^FT363,2076^A0I,17,16^FH\\^FDANTERIOR^FS\\r\\n^FT439,2077^A0I,17,16^FH\\^FDACTUAL^FS\\r\\n^FT439,2099^A0I,17,16^FH\\^FDLECTURA^FS\\r\\n^FT499,2169^A0I,17,16^FH\\^FDD\\A1as : ^FS\\r\\n^FO18,332^GB769,0,1^FS\\r\\n^FT774,2145^A0I,17,16^FH\\^FDDesde : ^FS\\r\\n^FO16,390^GB768,0,1^FS\\r\\n^FO3,551^GB366,0,1^FS\\r\\n^FO18,424^GB769,0,1^FS\\r\\n^FO18,429^GB769,0,1^FS\\r\\n^FT772,2168^A0I,17,16^FH\\^FDTipo Consumo : ^FS\\r\\n^FT773,2191^A0I,17,16^FH\\^FDMedidor : ^FS\\r\\n^FT773,2251^A0I,17,16^FH\\^FDDirecci\\A2n Envio : ^FS\\r\\n^FT773,2274^A0I,17,16^FH\\^FDDirecci\\A2n Servicio : ^FS\\r\\n^FT773,2297^A0I,17,16^FH\\^FDProvincia : ^FS\\r\\n^FT772,2363^A0I,17,16^FH\\^FD@tipoIdentificacionComprador^FS\\r\\n^FT773,2319^A0I,17,16^FH\\^FDTarifa Arcontel : ^FS\\r\\n^FT773,2342^A0I,17,16^FH\\^FDC\\A2digo \\E9nico El\\82ctrico Nacional: ^FS\\r\\n^FT772,2385^A0I,17,16^FH\\^FD@tipoRazonSocialComprador^FS\\r\\n^FT772,2403^A0I,20,19^FH\\^FDCuenta Contrato: ^FS\\r\\n^BY2,3,41^FT772,2485^BCI,,Y,N\\r\\n^FD>:@codigoBarras^FS\\r\\n^FO750,1903^GB0,107,16^FS\\r\\n^FO477,1902^GB0,107,16^FS\\r\\n^FO498,1902^GB0,106,16^FS\\r\\n^FO519,1902^GB0,107,16^FS\\r\\n^FO17,1284^GB768,0,1^FS\\r\\n^FO4,757^GB366,0,1^FS\\r\\n^FO20,635^GB768,0,1^FS\\r\\n^FO5,962^GB366,0,1^FS\\r\\n^FO20,840^GB769,0,1^FS\\r\\n^FO22,1065^GB768,0,1^FS\\r\\n^FO23,1093^GB768,0,1^FS\\r\\n^FO26,1098^GB768,0,1^FS\\r\\n^FO18,1289^GB768,0,1^FS\\r\\n^FO26,1344^GB768,0,1^FS\\r\\n^FO17,1411^GB769,0,1^FS\\r\\n^FO20,1484^GB768,0,1^FS\\r\\n^FO16,1573^GB768,0,1^FS\\r\\n^FO21,1374^GB769,0,1^FS\\r\\n^FO21,1378^GB768,0,1^FS\\r\\n^FO541,1902^GB0,107,16^FS\\r\\n^FO18,1515^GB768,0,1^FS\\r\\n^FO19,1518^GB768,0,1^FS\\r\\n^FO13,1660^GB768,0,1^FS\\r\\n^FO16,1603^GB768,0,1^FS\\r\\n^FO16,1607^GB768,0,1^FS\\r\\n^FO446,1756^GB341,0,1^FS\\r\\n^FO447,1880^GB341,0,1^FS\\r\\n^FO10,1691^GB769,0,1^FS\\r\\n^FO11,1693^GB768,0,1^FS\\r\\n^FO563,1903^GB0,106,16^FS\\r\\n^FO586,1903^GB0,106,16^FS\\r\\n^FO607,1902^GB0,107,16^FS\\r\\n^FO726,1903^GB0,107,16^FS\\r\\n^FO630,1903^GB0,107,16^FS\\r\\n^FO702,1903^GB0,107,16^FS\\r\\n^FO17,2043^GB769,0,1^FS\\r\\n^FO654,1903^GB0,107,16^FS\\r\\n^FO18,2122^GB768,0,1^FS\\r\\n^FO678,1903^GB0,107,16^FS\\r\\n^FO21,2209^GB768,0,1^FS\\r\\n^FO17,2428^GB768,0,1^FS\\r\\n^FO20,2238^GB768,0,1^FS\\r\\n^FO20,2241^GB768,0,1^FS\\r\\n^FO18,2431^GB768,0,1^FS\\r\\n^PQ1,0,1,Y^XZ\\r\\n".getBytes();

        } else if (pl == PrinterLanguage.CPCL) {
            String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
            configLabel = cpclConfigLabel.getBytes();
        }
        else {
            String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 PRM\r\n" + "PRINT\r\n";

            cpclConfigLabel = "^XA\\r\\n^MMC\\r\\n^PW799\\r\\n^LL2264\\r\\n^LS0\\r\\n^FO512,1696^GFA,01536,01536,00024,:Z64:\\r\\neJytVEFrE0EY/XbTkFhhs4vGKNTuGi8hSNVW6CpLNkLAi4IFgyep0IsnyYK6KWzcbSu4in9BiJ5KEPEHiN0q0otKL6IHg6Me9FRmEWnA2PX7tjG25FLEb5jM5PF4vHnzzQL8Uw1t2VfL9FvUmmmtOCw2xYfNGBZb8WpPWiPW0mQqXLE5/pVlN3JlGWA5v0s65Gmg3QMt5g+3Wh4uyycO7DuYO5G0VpKXCU6ttKsBrnPa7nHTHE2whduM8AxrtRYBhOVjByYsIQXHoUZ8QQ1du4Gbz+NK/tDcA0+7pcU6pW/FVgvXm5OW1ZjOpdbXz8Q6btRov8O1oHmamJfE/OH52M/PjWJsCL0KMi0KuQcl+uVG66sDgYhrpN8cwFNR92P7Eh/ElzqtmF9T9yYvONlytqZCjvihctGuAdQlI9PVVXa0Y3quB9Iaw4SQ74w6atdxOY5PUQBqxCFZRT+6pJe6ussjHrGfDHE83SbfcAznmhVaYRhyMKNVUOzzAGNrR+pGgZ9kjH1nDMYe41mHcbpd4tdmmcXCZYZ8NJvE6zY3jLqh89kmj/nmEh0ap+kg7tSnYv0yxomYglnpI4gX6sA4C9HK1bXewUdGHCqBhwztgLtB3VTB+9FJv0R83unhVGgf9U3hhxVyR0ZcxqtAfQn5R/SMt4H8Euo/7vPJfyZwQ8bVLfpSrJ9gJeRnPHCf9fhqnfwneAPTyQXgqj3/m3xg5CcRwFWpzyf7wNE/Fxg4SdIv9/kd4iNu/LnoXI30a7OYDuVpTGEXKdhQGW5cwfyvYP5hwKDQbwzuoP/zs3wzfxmtA83UE+OUodc7HPP8wrY0EjfcrtPgEeqvB6BksV9JP8V086leYlGHMeyTLXzqn2nMh3M3wGgqUKmgf1EriIuF3V5ea2rFba0q48jSC6BSpqo46AHvsCpUO2aDkK1ms8rmvgS+5C/qY4mvbBvH3v/+3OvuDTt5Hd/J0IXYPtWM7gd3/Hn8qmjb+Kez7Zft+y+swEK+8td+MH/3jf9oAbx8czt/z4fy2VcvhKBGOkN9+zMTfuDfez6gbyvtt+0P9iTQu8X0/+Bp8Hw/nU6D6O08gMFCt5Q8xLM3/lv9Bljoch4=:6876\\r\\n^FT491,1886^A0I,11,12^FH\\^FD@P13^FS\\r\\n^FT513,1885^A0I,11,12^FH\\^FD@P12^FS\\r\\n^FT534,1885^A0I,11,12^FH\\^FD@P11^FS\\r\\n^FT556,1885^A0I,11,12^FH\\^FD@P10^FS\\r\\n^FT578,1885^A0I,11,12^FH\\^FD@P09^FS\\r\\n^FT601,1885^A0I,11,12^FH\\^FD@P08^FS\\r\\n^FT623,1885^A0I,11,12^FH\\^FD@P07^FS\\r\\n^FT646,1885^A0I,11,12^FH\\^FD@P06^FS\\r\\n^FT670,1885^A0I,11,12^FH\\^FD@P05^FS\\r\\n^FT694,1885^A0I,11,12^FH\\^FD@P04^FS\\r\\n^FT492,2018^A0I,11,12^FH\\^FD@C13^FS\\r\\n^FT515,2018^A0I,11,12^FH\\^FD@C12^FS\\r\\n^FT536,2018^A0I,11,12^FH\\^FD@C11^FS\\r\\n^FT558,2018^A0I,11,12^FH\\^FD@C10^FS\\r\\n^FT580,2018^A0I,11,12^FH\\^FD@C09^FS\\r\\n^FT601,2018^A0I,11,12^FH\\^FD@C08^FS\\r\\n^FT624,2018^A0I,11,12^FH\\^FD@C07^FS\\r\\n^FT648,2018^A0I,11,12^FH\\^FD@C06^FS\\r\\n^FT717,1885^A0I,11,12^FH\\^FD@P03^FS\\r\\n^FT741,1885^A0I,11,12^FH\\^FD@P02^FS\\r\\n^FT85,2469^A0I,23,24^FH\\^FD@valorAPagar^FS\\r\\n^FT354,2469^A0I,23,24^FH\\^FD@fechaVencimiento^FS\\r\\n^FT413,2516^A0I,17,16^FH\\^FD@numAutorizacion^FS\\r\\n^FT274,2564^A0I,17,16^FH\\^FD@fechaVencimiento^FS\\r\\n^FT304,2587^A0I,17,16^FH\\^FD@fechaEmision^FS\\r\\n^FT305,2611^A0I,17,16^FH\\^FD@numDocumento^FS\\r\\n^FT413,2610^A0I,17,16^FH\\^FD@etiquetaDoc^FS\\r\\n^FT443,2022^A0I,17,16^FH\\^FD1.1 SERVICIO ELECTRICO (SE) Y SAPG^FS\\r\\n^FT772,1582^A0I,17,16^FH\\^FD3. PLANES DE FINANCIAMIENTO AUTORIZADOS POR EL CONSUMIDOR ^FS\\r\\n^FT771,1669^A0I,17,16^FH\\^FD2. VALORES PENDIENTES^FS\\r\\n^FT771,2218^A0I,17,16^FH\\^FD1. FACTURACION SERVICIO ELECTRICO Y ALUMBRADO PUBLICO^FS\\r\\n^FT771,2439^A0I,17,16^FH\\^FDINFORMACI\\E3N DEL CONSUMIDOR^FS\\r\\n^FT84,2500^A0I,17,16^FH\\^FDA Pagar^FS\\r\\n^FT354,2498^A0I,17,16^FH\\^FDFecha Vencimiento ^FS\\r\\n^FT414,2540^A0I,17,16^FH\\^FDN\\A3mero Autorizaci\\A2n: ^FS\\r\\n^FT336,2681^A0I,17,16^FH\\^FD@dirEstablecimiento^FS\\r\\n^FT413,2564^A0I,17,16^FH\\^FDFecha Vencimiento: ^FS\\r\\n^FT412,2634^A0I,17,16^FH\\^FD@telefono^FS\\r\\n^FT414,2587^A0I,17,16^FH\\^FDFecha Emisi\\A2n: ^FS\\r\\n^FT411,2657^A0I,17,16^FH\\^FD@ciudad^FS\\r\\n^FT413,2681^A0I,17,16^FH\\^FDSucursal: ^FS\\r\\n^FT672,2018^A0I,11,12^FH\\^FD@C05^FS\\r\\n^FT766,1885^A0I,11,12^FH\\^FD@P01^FS\\r\\n^FT695,2018^A0I,11,12^FH\\^FD@C04^FS\\r\\n^FT719,2018^A0I,11,12^FH\\^FD@C03^FS\\r\\n^FT742,2018^A0I,11,12^FH\\^FD@C02^FS\\r\\n^FT767,2018^A0I,11,12^FH\\^FD@C01^FS\\r\\n^FT64,1805^A0I,17,16^FH\\^FD@rvalor_rubro11^FS\\r\\n^FT442,1806^A0I,17,16^FH\\^FD@rubro11^FS\\r\\n^FT64,1767^A0I,17,16^FH\\^FD@rvalor_rubro13^FS\\r\\n^FT65,1863^A0I,17,16^FH\\^FD@rvalor_rubro_8^FS\\r\\n^FT443,1864^A0I,17,16^FH\\^FD@rubro_8^FS\\r\\n^FT65,1825^A0I,17,16^FH\\^FD@rvalor_rubro10^FS\\r\\n^FT442,1768^A0I,17,16^FH\\^FD@rubro13^FS\\r\\n^FT526,1764^A0I,17,16^FH\\^FD@st_valor_total^FS\\r\\n^FT767,1764^A0I,17,16^FH\\^FD@st_total^FS\\r\\n^FT767,1780^A0I,14,14^FH\\^FD@s6_subsidio^FS\\r\\n^FT767,1794^A0I,14,14^FH\\^FD@s5_subsidio^FS\\r\\n^FT767,1808^A0I,14,14^FH\\^FD@s4_subsidio^FS\\r\\n^FT767,1823^A0I,14,14^FH\\^FD@s3_subsidio^FS\\r\\n^FT768,1837^A0I,14,14^FH\\^FD@s2_subsidio^FS\\r\\n^FT526,1780^A0I,14,14^FH\\^FD@s6_valor^FS\\r\\n^FT526,1794^A0I,14,14^FH\\^FD@s5_valor^FS\\r\\n^FT526,1808^A0I,14,14^FH\\^FD@s4_valor^FS\\r\\n^FT526,1822^A0I,14,14^FH\\^FD@s3_valor^FS\\r\\n^FT526,1836^A0I,14,14^FH\\^FD@s2_valor^FS\\r\\n^FT526,1850^A0I,14,14^FH\\^FD@s1_valor^FS\\r\\n^FT768,1850^A0I,14,14^FH\\^FD@s1_subsidio^FS\\r\\n^FT443,1826^A0I,17,16^FH\\^FD@rubro10^FS\\r\\n^FT64,1786^A0I,17,16^FH\\^FD@rvalor_rubro12^FS\\r\\n^FT67,1385^A0I,20,19^FH\\^FD@valor_tablaResValPag_1^FS\\r\\n^FT65,1901^A0I,17,16^FH\\^FD@rvalor_rubro_6^FS\\r\\n^FT769,1387^A0I,20,19^FH\\^FD@concepto_tablaResValPag_1 (1+2+3) ^FS\\r\\n^FT65,1882^A0I,17,16^FH\\^FD@rvalor_rubro_7^FS\\r\\n^FT65,1844^A0I,17,16^FH\\^FD@rvalor_rubro_9^FS\\r\\n^FT67,1323^A0I,17,16^FH\\^FDTIEMPO^FS\\r\\n^FT213,1323^A0I,17,16^FH\\^FDPLAZO^FS\\r\\n^FT67,1300^A0I,17,16^FH\\^FD@tiempoForma_pago^FS\\r\\n^FT213,1299^A0I,17,16^FH\\^FD@plazoForma_pago^FS\\r\\n^FT309,1298^A0I,17,16^FH\\^FD@valorForma_pago^FS\\r\\n^FT769,1235^A0I,17,16^FH\\^FD@mensajes^FS\\r\\n^FT768,1299^A0I,17,16^FH\\^FD@descForma_pago^FS\\r\\n^FT308,1322^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT768,1323^A0I,17,16^FH\\^FDFORMA DE PAGO^FS\\r\\n^FT442,1787^A0I,17,16^FH\\^FD@rubro12^FS\\r\\n^FT443,1902^A0I,17,16^FH\\^FD@rubro_6^FS\\r\\n^FT65,1939^A0I,17,16^FH\\^FD@rvalor_rubro_4^FS\\r\\n^FT443,1883^A0I,17,16^FH\\^FD@rubro_7^FS\\r\\n^FT697,1865^A0I,14,14^FH\\^FDSUBSIDIOS DEL GOBIERNO^FS\\r\\n^FT443,1845^A0I,17,16^FH\\^FD@rubro_9^FS\\r\\n^FT443,1940^A0I,17,16^FH\\^FD@rubro_4^FS\\r\\n^FT66,560^A0I,17,16^FH\\^FD@t3_valorTercero^FS\\r\\n^FT68,766^A0I,17,16^FH\\^FD@t2_valorTercero^FS\\r\\n^FT371,562^A0I,17,16^FH\\^FD@t3_conceptoValorTercero^FS\\r\\n^FT68,971^A0I,17,16^FH\\^FD@t1_valorTercero^FS\\r\\n^FT372,767^A0I,17,16^FH\\^FD@t2_conceptoValorTercero^FS\\r\\n^FT67,1636^A0I,20,19^FH\\^FD@valorTotal_rubro_2^FS\\r\\n^FT373,973^A0I,17,16^FH\\^FD@t1_conceptoValorTercero^FS\\r\\n^FT67,1420^A0I,17,16^FH\\^FD@valorTotal_rubro_2^FS\\r\\n^FT67,1440^A0I,17,16^FH\\^FD@valorTotal_rubro_3^FS\\r\\n^FT67,1554^A0I,20,19^FH\\^FD@valorTotal_rubro_3^FS\\r\\n^FT769,1637^A0I,20,19^FH\\^FD@tablaTotal_rubro_2^FS\\r\\n^FT769,1441^A0I,17,16^FH\\^FD@tablaTotal_rubro_3^FS\\r\\n^FT770,1421^A0I,17,16^FH\\^FD@tablaTotal_rubro_2^FS\\r\\n^FT769,1556^A0I,20,19^FH\\^FD@tablaTotal_rubro_3^FS\\r\\n^FT369,528^A0I,17,16^FH\\^FD@t3_etiqueta^FS\\r\\n^FT65,526^A0I,17,16^FH\\^FD@t3_valorRecaudacion^FS\\r\\n^FT493,1262^A0I,17,16^FH\\^FDMENSAJE AL CONSUMIDOR^FS\\r\\n^FT371,580^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT524,1492^A0I,17,16^FH\\^FDTOTAL A PAGAR^FS\\r\\n^FT371,734^A0I,17,16^FH\\^FD@t2_etiqueta^FS\\r\\n^FT66,579^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT66,732^A0I,17,16^FH\\^FD@t2_valorRecaudacion^FS\\r\\n^FT371,939^A0I,17,16^FH\\^FD@t1_etiqueta^FS\\r\\n^FT761,465^A0I,17,16^FH\\^FD@t3_razonSocialComprador^FS\\r\\n^FT552,488^A0I,17,16^FH\\^FD@t3_identificacionComprador^FS\\r\\n^FT761,488^A0I,17,16^FH\\^FD@t3_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT544,399^A0I,17,16^FH\\^FDRESUMEN DE VALORES A PAGAR^FS\\r\\n^FT61,343^A0I,17,16^FH\\^FD@valor_tablaResValPag_2^FS\\r\\n^FT60,368^A0I,17,16^FH\\^FD@valor_tablaResValPag_1^FS\\r\\n^FT63,310^A0I,17,16^FH\\^FD@totalAPagar^FS\\r\\n^FT774,306^A0I,20,19^FH\\^FDTOTAL A PAGAR (USD)^FS\\r\\n^FT772,344^A0I,17,16^FH\\^FD@concepto_tablaResValPag_2^FS\\r\\n^FT772,368^A0I,17,16^FH\\^FD@concepto_tablaResValPag_1^FS\\r\\n^FT761,441^A0I,17,16^FH\\^FD@t3_direccionSuministro^FS\\r\\n^FT372,786^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT762,508^A0I,17,16^FH\\^FD@t3_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT373,992^A0I,17,16^FH\\^FDCONCEPTO^FS\\r\\n^FT67,937^A0I,17,16^FH\\^FD@t1_valorRecaudacion^FS\\r\\n^FT762,529^A0I,17,16^FH\\^FD@t3_fechaEmision_recaudacionTerceros^FS\\r\\n^FT68,785^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT68,990^A0I,17,16^FH\\^FDVALOR^FS\\r\\n^FT66,1461^A0I,17,16^FH\\^FD@valorTotal_rubro_1^FS\\r\\n^FT762,671^A0I,17,16^FH\\^FD@t2_razonSocialComprador^FS\\r\\n^FT553,693^A0I,17,16^FH\\^FD@t2_identificacionComprador^FS\\r\\n^FT763,694^A0I,17,16^FH\\^FD@t2_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT763,591^A0I,17,16^FH\\^FD@t3_canton^FS\\r\\n^FT762,552^A0I,17,16^FH\\^FD@t3_rucBenef^FS\\r\\n^FT762,647^A0I,17,16^FH\\^FD@t2_direccionSuministro^FS\\r\\n^FT763,610^A0I,17,16^FH\\^FD@sustentoLegal_3^FS\\r\\n^FT768,1463^A0I,17,16^FH\\^FD@tablaTotal_rubro_1^FS\\r\\n^FT764,714^A0I,17,16^FH\\^FD@t2_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT558,1072^A0I,17,16^FH\\^FDORDEN DE COBRO POR CUENTA DE TERCEROS^FS\\r\\n^FT763,572^A0I,17,16^FH\\^FD@t3_benef^FS\\r\\n^FT65,1920^A0I,17,16^FH\\^FD@rvalor_rubro_5^FS\\r\\n^FT764,735^A0I,17,16^FH\\^FD@t2_fechaEmision_recaudacionTerceros^FS\\r\\n^FT65,1977^A0I,17,16^FH\\^FD@rvalor_rubro_2^FS\\r\\n^FT767,1353^A0I,17,16^FH\\^FDFORMA DE PAGO^FS\\r\\n^FT763,852^A0I,17,16^FH\\^FD@t1_direccionSuministro^FS\\r\\n^FT763,876^A0I,17,16^FH\\^FD@t1_razonSocialComprador^FS\\r\\n^FT545,899^A0I,17,16^FH\\^FD@t1_identificacionComprador^FS\\r\\n^FT763,899^A0I,17,16^FH\\^FD@t1_tipoIdentificacionComprador_recaudacionTerceros^FS\\r\\n^FT764,797^A0I,17,16^FH\\^FD@t2_canton^FS\\r\\n^FT763,758^A0I,17,16^FH\\^FD@t2_rucBenef^FS\\r\\n^FT65,1958^A0I,17,16^FH\\^FD@rvalor_rubro_3^FS\\r\\n^FT764,816^A0I,17,16^FH\\^FD@sustentoLegal_2^FS\\r\\n^FT764,919^A0I,17,16^FH\\^FD@t1_cuentaContrato_recaudacionTerceros^FS\\r\\n^FT765,1002^A0I,17,16^FH\\^FD@t1_canton^FS\\r\\n^FT765,940^A0I,17,16^FH\\^FD@t1_fechaEmision_recaudacionTerceros^FS\\r\\n^FT764,778^A0I,17,16^FH\\^FD@t2_benef^FS\\r\\n^FT764,963^A0I,17,16^FH\\^FD@t1_rucBenef^FS\\r\\n^FT765,983^A0I,17,16^FH\\^FD@t1_benef^FS\\r\\n^FT443,1921^A0I,17,16^FH\\^FD@rubro_5^FS\\r\\n^FT765,1021^A0I,17,16^FH\\^FD@sustentoLegal_1^FS\\r\\n^FT757,1043^A0I,17,16^FH\\^FDESTOS VALORES NO FORMAN PARTE DE LOS INFGRESOS DE LA EMPRESA ELECTRICA^FS\\r\\n^FT65,1996^A0I,17,16^FH\\^FD@rvalor_rubro_1^FS\\r\\n^FT443,1978^A0I,17,16^FH\\^FD@rubro_2^FS\\r\\n^FT443,1959^A0I,17,16^FH\\^FD@rubro_3^FS\\r\\n^FT443,1997^A0I,17,16^FH\\^FD@rubro_1^FS\\r\\n^FT198,2298^A0I,17,16^FH\\^FD@parroquia^FS\\r\\n^FT74,2054^A0I,17,16^FH\\^FD@valores^FS\\r\\n^FT191,2054^A0I,17,16^FH\\^FD@unidad^FS\\r\\n^FT278,2052^A0I,17,16^FH\\^FD@cons_mensual^FS\\r\\n^FT362,2052^A0I,17,16^FH\\^FD@anterior^FS\\r\\n^FT764,2053^A0I,17,16^FH\\^FD@descripcion^FS\\r\\n^FT440,2053^A0I,17,16^FH\\^FD@actual^FS\\r\\n^FT443,2145^A0I,17,16^FH\\^FD@fechaHasta^FS\\r\\n^FT713,2144^A0I,17,16^FH\\^FD@fechaDesde^FS\\r\\n^FT163,2129^A0I,17,16^FH\\^FD@penBajoFactPot^FS\\r\\n^FT164,2152^A0I,17,16^FH\\^FD@factorPotencia^FS\\r\\n^FT163,2176^A0I,17,16^FH\\^FD@factorCorr^FS\\r\\n^FT452,2169^A0I,17,16^FH\\^FD@diasFact^FS\\r\\n^FT660,2168^A0I,17,16^FH\\^FD@tipoCons^FS\\r\\n^FT695,2190^A0I,17,16^FH\\^FD@numeroMed^FS\\r\\n^FT633,2252^A0I,17,16^FH\\^FD@direccionEnvio^FS\\r\\n^FT632,2275^A0I,17,16^FH\\^FD@direccionSuministro^FS\\r\\n^FT479,2296^A0I,17,16^FH\\^FD@canton^FS\\r\\n^FT692,2297^A0I,17,16^FH\\^FD@provincia^FS\\r\\n^FT546,2320^A0I,17,16^FH\\^FD@tarifa^FS\\r\\n^FT548,2342^A0I,17,16^FH\\^FD@cuen^FS\\r\\n^FT549,2363^A0I,17,16^FH\\^FD@identificacionComprador^FS\\r\\n^FT549,2385^A0I,17,16^FH\\^FD@razonSocialComprador^FS\\r\\n^FT550,2403^A0I,20,19^FH\\^FD@cuentaContrato^FS\\r\\n^FT279,2297^A0I,17,16^FH\\^FDParroquia : ^FS\\r\\n^FT544,2295^A0I,17,16^FH\\^FDCant\\A2n : ^FS\\r\\n^FT271,2129^A0I,17,16^FH\\^FDPenalizaci\\A2n : ^FS\\r\\n^FT499,2146^A0I,17,16^FH\\^FDHasta : ^FS\\r\\n^FT271,2152^A0I,17,16^FH\\^FDF. Potencia : ^FS\\r\\n^FT74,2078^A0I,17,16^FH\\^FDVALORES^FS\\r\\n^FT271,2174^A0I,17,16^FH\\^FDF. Correcci\\A2n : ^FS\\r\\n^FT191,2077^A0I,17,16^FH\\^FDUND^FS\\r\\n^FT279,2099^A0I,17,16^FH\\^FDCONSUMO^FS\\r\\n^FT279,2077^A0I,17,16^FH\\^FDMENSUAL^FS\\r\\n^FT363,2098^A0I,17,16^FH\\^FDLECTURA^FS\\r\\n^FT765,2077^A0I,17,16^FH\\^FDDESCRIPCION^FS\\r\\n^FT363,2076^A0I,17,16^FH\\^FDANTERIOR^FS\\r\\n^FT439,2077^A0I,17,16^FH\\^FDACTUAL^FS\\r\\n^FT439,2099^A0I,17,16^FH\\^FDLECTURA^FS\\r\\n^FT499,2169^A0I,17,16^FH\\^FDD\\A1as : ^FS\\r\\n^FO18,332^GB769,0,1^FS\\r\\n^FT774,2145^A0I,17,16^FH\\^FDDesde : ^FS\\r\\n^FO16,390^GB768,0,1^FS\\r\\n^FO3,551^GB366,0,1^FS\\r\\n^FO18,424^GB769,0,1^FS\\r\\n^FO18,429^GB769,0,1^FS\\r\\n^FT772,2168^A0I,17,16^FH\\^FDTipo Consumo : ^FS\\r\\n^FT773,2191^A0I,17,16^FH\\^FDMedidor : ^FS\\r\\n^FT773,2251^A0I,17,16^FH\\^FDDirecci\\A2n Envio : ^FS\\r\\n^FT773,2274^A0I,17,16^FH\\^FDDirecci\\A2n Servicio : ^FS\\r\\n^FT773,2297^A0I,17,16^FH\\^FDProvincia : ^FS\\r\\n^FT772,2363^A0I,17,16^FH\\^FD@tipoIdentificacionComprador^FS\\r\\n^FT773,2319^A0I,17,16^FH\\^FDTarifa Arcontel : ^FS\\r\\n^FT773,2342^A0I,17,16^FH\\^FDC\\A2digo \\E9nico El\\82ctrico Nacional: ^FS\\r\\n^FT772,2385^A0I,17,16^FH\\^FD@tipoRazonSocialComprador^FS\\r\\n^FT772,2403^A0I,20,19^FH\\^FDCuenta Contrato: ^FS\\r\\n^BY2,3,41^FT772,2485^BCI,,Y,N\\r\\n^FD>:@codigoBarras^FS\\r\\n^FO750,1903^GB0,107,16^FS\\r\\n^FO477,1902^GB0,107,16^FS\\r\\n^FO498,1902^GB0,106,16^FS\\r\\n^FO519,1902^GB0,107,16^FS\\r\\n^FO17,1284^GB768,0,1^FS\\r\\n^FO4,757^GB366,0,1^FS\\r\\n^FO20,635^GB768,0,1^FS\\r\\n^FO5,962^GB366,0,1^FS\\r\\n^FO20,840^GB769,0,1^FS\\r\\n^FO22,1065^GB768,0,1^FS\\r\\n^FO23,1093^GB768,0,1^FS\\r\\n^FO26,1098^GB768,0,1^FS\\r\\n^FO18,1289^GB768,0,1^FS\\r\\n^FO26,1344^GB768,0,1^FS\\r\\n^FO17,1411^GB769,0,1^FS\\r\\n^FO20,1484^GB768,0,1^FS\\r\\n^FO16,1573^GB768,0,1^FS\\r\\n^FO21,1374^GB769,0,1^FS\\r\\n^FO21,1378^GB768,0,1^FS\\r\\n^FO541,1902^GB0,107,16^FS\\r\\n^FO18,1515^GB768,0,1^FS\\r\\n^FO19,1518^GB768,0,1^FS\\r\\n^FO13,1660^GB768,0,1^FS\\r\\n^FO16,1603^GB768,0,1^FS\\r\\n^FO16,1607^GB768,0,1^FS\\r\\n^FO446,1756^GB341,0,1^FS\\r\\n^FO447,1880^GB341,0,1^FS\\r\\n^FO10,1691^GB769,0,1^FS\\r\\n^FO11,1693^GB768,0,1^FS\\r\\n^FO563,1903^GB0,106,16^FS\\r\\n^FO586,1903^GB0,106,16^FS\\r\\n^FO607,1902^GB0,107,16^FS\\r\\n^FO726,1903^GB0,107,16^FS\\r\\n^FO630,1903^GB0,107,16^FS\\r\\n^FO702,1903^GB0,107,16^FS\\r\\n^FO17,2043^GB769,0,1^FS\\r\\n^FO654,1903^GB0,107,16^FS\\r\\n^FO18,2122^GB768,0,1^FS\\r\\n^FO678,1903^GB0,107,16^FS\\r\\n^FO21,2209^GB768,0,1^FS\\r\\n^FO17,2428^GB768,0,1^FS\\r\\n^FO20,2238^GB768,0,1^FS\\r\\n^FO20,2241^GB768,0,1^FS\\r\\n^FO18,2431^GB768,0,1^FS\\r\\n^PQ1,0,1,Y^XZ\\r\\n";
                    configLabel = cpclConfigLabel.getBytes();
        }
        os.write(configLabel);
        os.flush();
        os.close();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView

                mArrayAdapter.add(device.getName() + "\n" + device.getAddress());

            }
        }
    };

}
