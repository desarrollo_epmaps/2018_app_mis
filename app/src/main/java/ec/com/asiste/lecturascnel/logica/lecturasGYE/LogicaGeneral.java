package ec.com.asiste.lecturascnel.logica.lecturasGYE;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.asistecom.worker.web.entity.vistiasIn.visitas;

import java.util.ArrayList;

import ec.com.asiste.lecturascnel.logica.Parametros;

public class LogicaGeneral {
    public String ls_lectura;
    public String ls_codObs1 ;
    public String ls_codObs2 ;
    public String ls_codObs3;
    public String ls_obsLect ;
    public Integer ls_fotos;
    public double ls_consumo;
    public visitas ls_visita ;

    public LogicaGeneral() {
    }

    public LogicaGeneral(String ls_lectura, String ls_codObs1, String ls_codObs2, String ls_codObs3, String ls_obsLect, Integer ls_fotos, visitas ls_visita) {
        this.ls_lectura = ls_lectura;
        this.ls_codObs1 = ls_codObs1;
        this.ls_codObs2 = ls_codObs2;
        this.ls_codObs3 = ls_codObs3;
        this.ls_obsLect = ls_obsLect;
        this.ls_fotos = ls_fotos;
        this.ls_visita = ls_visita;
    }
    public String procesoGeneral() {
        double consumo;
        if (ls_visita.ordenLectura.numeroMedidor != "") {
            //Cuenta con medidor
            if (ls_codObs1 != "") {
                ArrayList<String> codigos = new ArrayList<String>();
                codigos.add("281");
                codigos.add("282");
                //Tiene codigo de impedimento
                if (!(codigos).contains(ls_visita.ordenLectura.guia) && Parametros.intentos_legalizacion < 1) {
                    //MessageBox.Show("VERIFIQUE NUEVAMENTE, ESTE MEDIDOR ES MAYOR O IGUAL A UNA PULGADA DEBE OBTENER LA LECTURA", "IMPORTANTE");
                    Parametros.intentos_legalizacion++;
                    return "";
                } else if ((codigos).contains(ls_visita.ordenLectura.guia) && ls_visita.ordenLectura.origenPrevio.equals("1") && Parametros.intentos_legalizacion < 1){ //Lector debe Verificar
                    //MessageBox.Show("VERIFIQUE NUEVAMENTE, MEDIDOR SI TIENE LECTURA TOMADA MES PREVIO ", "IMPORTANTE");
                    Parametros.intentos_legalizacion++;
                    return "";
                } else { //Gestion Impedimento de lectura
                    if (!(codigos).contains(ls_visita.ordenLectura.guia) && Parametros.intentos_legalizacion++ == 3) {
                        //MessageBox.Show("CUENTA BLOQUEADA MEDIDOR MAYOR O IGUAL A UNA PULGADA", "MENSAJE DEL SISTEMA");
                       Parametros.observacionCritica += " CUENTA BLOQUEADA MEDIDOR MAYOR O IGUAL A UNA PULGADA";
                       Parametros.numero_fotos = 1;
                    }else
                    new ImpedimentosLecturas();
                }
            }
        }else {//No tiene codigo de impedimento
            String ls_lectAnterior = ls_visita.ordenLectura.lecturaAnterior;
            Parametros.observacionCritica = "";
            Parametros.consumo_calculado_gs = "";
            Parametros.numero_fotos = 0;
            Parametros.observ_cod = "";
            if (!ls_lectAnterior.equals("") && !Parametros.lecturaCampo.equals(""))
            {
                int ln_consumo = Integer.parseInt(Parametros.lecturaCampo) -Integer.parseInt(ls_visita.ordenLectura.lecturaAnterior);
                int ln_cph2 = Integer.parseInt(ls_visita.ordenLectura.consumoPromedio) * 2;
                int ln_ca = Integer.parseInt(ls_visita.ordenLectura.lecturaMax);
                int ln_cb =  Integer.parseInt(ls_visita.ordenLectura.lecturaMin);
                if (ln_consumo == 0) {//Gestion Lecturas Identica
                    ArrayList<String> codigos = new ArrayList<String>();
                    codigos.add("095");
                    codigos.add("103");
                    if ((codigos.contains(ls_codObs3) || codigos.contains(ls_codObs2))) {
                        //95 PREDIO DESOCUPADO 103 SOLAR VACIO
                          Parametros.observacionCritica  = "SMARTFLEX DEBE GENERAR VLI QUE SE CIERRA DE FORMA AUTOMATICA";
                          Parametros.consumo_calculado_gs = "0";
                          Parametros.numero_fotos = 1;
                        return "";
                    }
                    if (Parametros.intentoslectura.equals(3)) { //Bloquear registro
                        //MessageBox.Show("CUENTA BLOQUEADA", "MENSAJE DEL SISTEMA");
                       Parametros.observacionCritica += " CUENTA BLOQUEADA";
                       return "";
                    }
                    //GlobalVar.lg_cdImpedimento = "";
                    //GlobalVar.lg_cdObservacion1 = "";
                    //GlobalVar.lg_cdObservacion2 = "";
                    return "VLI";
                }
                else if (ln_consumo < 0){

                    AlertDialog.Builder builderVL = new AlertDialog.Builder(Parametros.contexto);
                    builderVL
                            .setTitle("CONFIRMAR")
                            .setMessage("EXISTE DESVIOS EN LA LECTURA DESEA VERIFICAR?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog.Builder builderVM = new AlertDialog.Builder(Parametros.contexto);
                                    builderVM
                                            .setTitle("CONFIRMAR")
                                            .setMessage("LA LECTURA ES DEL MEDIDOR " + ls_visita.ordenLectura.getNumeroMedidor() + "?")
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //Yes button clicked, do something
                                                   // p_ConsumoNegativoLDM();
                                                }
                                            })
                                            .setNegativeButton("NO", null)						//Do nothing on no
                                            .show();


                                }
                            })
                            .setNegativeButton("NO", null)						//Do nothing on no
                            .show();


                    //Gestion Consumo Negativo LDM


                }
                else if (ln_consumo > ln_cph2 && ln_consumo > 20) {    //Gestion Incremento IEC

                    AlertDialog.Builder builderVL = new AlertDialog.Builder(Parametros.contexto);
                    builderVL
                            .setTitle("CONFIRMAR")
                            .setMessage("EXISTE DESVIOS EN LA LECTURA DESEA VERIFICAR?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog.Builder builderVM = new AlertDialog.Builder(Parametros.contexto);
                                    builderVM
                                            .setTitle("CONFIRMAR")
                                            .setMessage("LA LECTURA ES DEL MEDIDOR " + ls_visita.ordenLectura.getNumeroMedidor() + "?")
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //Yes button clicked, do something
                                                    // p_ConsumoNegativoLDM();
                                                }
                                            })
                                            .setNegativeButton("NO", null)						//Do nothing on no
                                            .show();


                                }
                            })
                            .setNegativeButton("NO", null)						//Do nothing on no
                            .show();
                }
            }
        }


             return "";
        }


    public double getLs_consumo() {
        return ls_consumo;
    }

    public void setLs_consumo(double ls_consumo) {
        this.ls_consumo = ls_consumo;
    }

    public visitas getLs_visita() {
        return ls_visita;
    }

    public void setLs_visita(visitas ls_visita) {
        this.ls_visita = ls_visita;
    }

    public String getLs_lectura() {
        return ls_lectura;
    }

    public void setLs_lectura(String ls_lectura) {
        this.ls_lectura = ls_lectura;
    }

    public String getLs_codObs1() {
        return ls_codObs1;
    }

    public void setLs_codObs1(String ls_codObs1) {
        this.ls_codObs1 = ls_codObs1;
    }

    public String getLs_codObs2() {
        return ls_codObs2;
    }

    public void setLs_codObs2(String ls_codObs2) {
        this.ls_codObs2 = ls_codObs2;
    }

    public String getLs_codObs3() {
        return ls_codObs3;
    }

    public void setLs_codObs3(String ls_codObs3) {
        this.ls_codObs3 = ls_codObs3;
    }

    public String getLs_obsLect() {
        return ls_obsLect;
    }

    public void setLs_obsLect(String ls_obsLect) {
        this.ls_obsLect = ls_obsLect;
    }

    public Integer getLs_fotos() {
        return ls_fotos;
    }

    public void setLs_fotos(Integer ls_fotos) {
        this.ls_fotos = ls_fotos;
    }

    public double getL_consumo() {
        return ls_consumo;
    }

    public void setL_consumo(double l_consumo) {
        this.ls_consumo = l_consumo;
    }
}
