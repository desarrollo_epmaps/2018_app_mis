package ec.com.asiste.lecturascnel.logica;

import java.util.ArrayList;
import java.util.List;

public class fileResumenRuta {

    String ruta ;
    String path;
    String cantidad;
    List<String> archivos;

    public fileResumenRuta()
    {

        setRuta("");
        setCantidad("");
        setArchivos(new ArrayList<String>());
    }

    public List<String> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<String> archivos) {
        this.archivos = archivos;
    }

    @Override
    public String toString() {
        return  ruta +" - "+ cantidad;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
