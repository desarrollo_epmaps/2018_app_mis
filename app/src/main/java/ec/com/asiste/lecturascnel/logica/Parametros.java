package ec.com.asiste.lecturascnel.logica;

import android.content.Context;
import android.support.annotation.NonNull;

import com.asistecom.worker.web.entity.configuracion.codigoCNEL;
import com.asistecom.worker.web.entity.vistiasIn.historico;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Parametros {

    public static int pedirReves = 1;
    public static int tiempoAutoEnvio = 1;
    public static int fotos_lecturas = 1;
    public static int foto_entregas = 1;
    public static int foto_sobrante = 3;
    public static int foto_industriales = 0;
    public static int video_industriales = 1;
    public static File dir_videos = null;
    public static List<codigoCNEL> codigos = new ArrayList<>();
    public static visitas vt_busqueda = null;
    public static visitas vt_busqueda_S = null;
    public static visitas vt_actual = null;
    public static String version = "0.0.0.1";
    public static String observacionCritica = "";
    public static String intentoslectura = "";
    public static String consumo_calculado_gs = "";
    public static String observ_cod = "";
    public static String lecturaCampo = "";
    public static String ipPublica = "http://186.3.117.126";//"http://192.168.2.219"http://186.3.117.126
    public static String puertoPublico = "8082";
    public static String ipInterna = "http://186.3.117.126";
    public static String puertoInterna = "8082";
    public static String mac = "00:17:E9:BE:E1:0B";
    public static String dir_fotosC = "";
    public static List<ResumenRuta> resumen = new ArrayList<ResumenRuta>();
    public static String zebra = "0:0:0:0:0";
    public static String getVersion() {
        return version;
    }
    public static ResumenRuta res = null;
    public static String idProyecto = "";
    public static int intentos_legalizacion  = 0;
    public static int numero_fotos  = 0;
    public static String impresora = "";
    public static String servicio = "/wis/servicio";

    public static int getPedirReves() {
        return pedirReves;
    }

    public static void setPedirReves(int pedirReves) {
        Parametros.pedirReves = pedirReves;
    }

    public static int getTiempoAutoEnvio() {
        return tiempoAutoEnvio;
    }

    public static void setTiempoAutoEnvio(int tiempoAutoEnvio) {
        Parametros.tiempoAutoEnvio = tiempoAutoEnvio;
    }

    public static int getFotos_lecturas() {
        return fotos_lecturas;
    }

    public static void setFotos_lecturas(int fotos_lecturas) {
        Parametros.fotos_lecturas = fotos_lecturas;
    }

    public static int getFoto_entregas() {
        return foto_entregas;
    }

    public static void setFoto_entregas(int foto_entregas) {
        Parametros.foto_entregas = foto_entregas;
    }

    public static int getFoto_sobrante() {
        return foto_sobrante;
    }

    public static void setFoto_sobrante(int foto_sobrante) {
        Parametros.foto_sobrante = foto_sobrante;
    }

    public static int getFoto_industriales() {
        return foto_industriales;
    }

    public static void setFoto_industriales(int foto_industriales) {
        Parametros.foto_industriales = foto_industriales;
    }

    public static int getVideo_industriales() {
        return video_industriales;
    }

    public static void setVideo_industriales(int video_industriales) {
        Parametros.video_industriales = video_industriales;
    }

    public static File getDir_videos() {
        return dir_videos;
    }

    public static void setDir_videos(File dir_videos) {
        Parametros.dir_videos = dir_videos;
    }

    public static List<codigoCNEL> getCodigos() {
        return codigos;
    }

    public static void setCodigos(List<codigoCNEL> codigos) {
        Parametros.codigos = codigos;
    }

    public static visitas getVt_busqueda() {
        return vt_busqueda;
    }

    public static void setVt_busqueda(visitas vt_busqueda) {
        Parametros.vt_busqueda = vt_busqueda;
    }

    public static visitas getVt_busqueda_S() {
        return vt_busqueda_S;
    }

    public static void setVt_busqueda_S(visitas vt_busqueda_S) {
        Parametros.vt_busqueda_S = vt_busqueda_S;
    }

    public static visitas getVt_actual() {
        return vt_actual;
    }

    public static void setVt_actual(visitas vt_actual) {
        Parametros.vt_actual = vt_actual;
    }

    public static String getIntentoslectura() {
        return intentoslectura;
    }

    public static void setIntentoslectura(String intentoslectura) {
        Parametros.intentoslectura = intentoslectura;
    }

    public static String getIdProyecto() {
        return idProyecto;
    }

    public static void setIdProyecto(String idProyecto) {
        Parametros.idProyecto = idProyecto;
    }

    public static String getServicioF() {
        return servicioF;
    }

    public static void setServicioF(String servicioF) {
        Parametros.servicioF = servicioF;
    }

    public static String getServicioConfig() {
        return servicioConfig;
    }

    public static void setServicioConfig(String servicioConfig) {
        Parametros.servicioConfig = servicioConfig;
    }

    public static String getProyecto() {
        return proyecto;
    }

    public static void setProyecto(String proyecto) {
        Parametros.proyecto = proyecto;
    }

    public static String getPassword9999() {
        return password9999;
    }

    public static void setPassword9999(String password9999) {
        Parametros.password9999 = password9999;
    }

    public static String servicioF = "/wis/files/uploadFile";
    public static String servicioConfig = "/wis/config";
    public static String proyecto = "CNEL";
    public static Context contexto;
    public static String usuario = "";
    public static String MAC = "";
    public static String nomberUsuario = "";
    public static File dir_fotos = null;
    public static File dir_files = null;
    public static File dir_archivos = null;
    public static String password9999 = "7537";

    public static String getObservacionCritica() {
        return observacionCritica;
    }

    public static void setObservacionCritica(String observacionCritica) {
        Parametros.observacionCritica = observacionCritica;
    }

    public static int getIntentos_legalizacion() {
        return intentos_legalizacion;
    }

    public static void setIntentos_legalizacion(int intentos_legalizacion) {
        Parametros.intentos_legalizacion = intentos_legalizacion;
    }

    public static int getNumero_fotos() {
        return numero_fotos;
    }

    public static void setNumero_fotos(int numero_fotos) {
        Parametros.numero_fotos = numero_fotos;
    }



    public static String getConsumo_calculado_gs() {
        return consumo_calculado_gs;
    }

    public static void setConsumo_calculado_gs(String consumo_calculado_gs) {
        Parametros.consumo_calculado_gs = consumo_calculado_gs;
    }

    public static String getObserv_cod() {
        return observ_cod;
    }

    public static void setObserv_cod(String observ_cod) {
        Parametros.observ_cod = observ_cod;
    }

    public static String getLecturaCampo() {
        return lecturaCampo;
    }

    public static void setLecturaCampo(String lecturaCampo) {
        Parametros.lecturaCampo = lecturaCampo;
    }

    public static String getDir_fotosC() {
        return dir_fotosC;
    }

    public static void setDir_fotosC(String dir_fotosC) {
        Parametros.dir_fotosC = dir_fotosC;
    }

    public static List<ResumenRuta> getResumen() {
        return resumen;
    }

    public static void setResumen(List<ResumenRuta> resumen) {
        Parametros.resumen = resumen;
    }

    public static String getZebra() {
        return zebra;
    }

    public static void setZebra(String zebra) {
        Parametros.zebra = zebra;
    }

    public static ResumenRuta getRes() {
        return res;
    }

    public static void setRes(ResumenRuta res) {
        Parametros.res = res;
    }

    public static File getDir_archivos() {
        return dir_archivos;
    }

    public static void setDir_archivos(File dir_archivos) {
        Parametros.dir_archivos = dir_archivos;
    }


    public static void setVersion(String version) {
        Parametros.version = version;
    }

    public static String getIpPublica() {
        return ipPublica;
    }

    public static void setIpPublica(String ipPublica) {
        Parametros.ipPublica = ipPublica;
    }

    public static String getPuertoPublico() {
        return puertoPublico;
    }

    public static void setPuertoPublico(String puertoPublico) {
        Parametros.puertoPublico = puertoPublico;
    }

    public static String getIpInterna() {
        return ipInterna;
    }

    public static void setIpInterna(String ipInterna) {
        Parametros.ipInterna = ipInterna;
    }

    public static String getPuertoInterna() {
        return puertoInterna;
    }

    public static void setPuertoInterna(String puertoInterna) {
        Parametros.puertoInterna = puertoInterna;
    }

    public static String getServicio() {
        return servicio;
    }

    public static void setServicio(String servicio) {
        Parametros.servicio = servicio;
    }

    public static Context getContexto() {
        return contexto;
    }

    public static void setContexto(Context contexto) {
        Parametros.contexto = contexto;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        Parametros.usuario = usuario;
    }

    public static String getMAC() {
        return MAC;
    }

    public static void setMAC(String MAC) {
        Parametros.MAC = MAC;
    }

    public static String getMac() {
        return mac;
    }

    public static void setMac(String mac) {
        Parametros.mac = mac;
    }

    public static String getImpresora() {
        return impresora;
    }

    public static void setImpresora(String impresora) {
        Parametros.impresora = impresora;
    }

    public static String getNomberUsuario() {
        return nomberUsuario;
    }

    public static void setNomberUsuario(String nomberUsuario) {
        Parametros.nomberUsuario = nomberUsuario;
    }

    public static File getDir_fotos() {
        return dir_fotos;
    }

    public static void setDir_fotos(File dir_fotos) {
        Parametros.dir_fotos = dir_fotos;
    }

    public static File getDir_files() {
        return dir_files;
    }

    public static void setDir_files(File dir_files) {
        Parametros.dir_files = dir_files;
    }

    public static List<String> getPrinters() {
        return printers;
    }

    public static void setPrinters(List<String> printers) {
        Parametros.printers = printers;
    }


    public static List<String> printers = new List<String>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @NonNull
        @Override
        public Iterator<String> iterator() {
            return null;
        }

        @NonNull
        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @NonNull
        @Override
        public <T> T[] toArray(@NonNull T[] a) {
            return null;
        }

        @Override
        public boolean add(String s) {
            printers.add(s);
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(@NonNull Collection<? extends String> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, @NonNull Collection<? extends String> c) {
            return false;
        }

        @Override
        public boolean removeAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public String get(int index) {
            return null;
        }

        @Override
        public String set(int index, String element) {
            return null;
        }

        @Override
        public void add(int index, String element) {

        }

        @Override
        public String remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @NonNull
        @Override
        public ListIterator<String> listIterator() {
            return null;
        }

        @NonNull
        @Override
        public ListIterator<String> listIterator(int index) {
            return null;
        }

        @NonNull
        @Override
        public List<String> subList(int fromIndex, int toIndex) {
            return null;
        }
    };
}
