package ec.com.asiste.lecturascnel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.zxing.Result;
import com.google.zxing.common.StringUtils;

import ec.com.asiste.lecturascnel.logica.util;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        // Log.v("tag", rawResult.getText()); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        if( EntregaActivity.cuenta.getText().length() == 0)
        {
            String resultado = rawResult.getText();
            if(util.isNumeric(resultado))
            {
                EntregaActivity.cuenta.setText(rawResult.getText());
            }
            else
            {
                EntregaActivity.cuenta.setText(resultado.substring(1));
            }


        onBackPressed();}
        else
        {
            EntregaActivity.noti.setText(rawResult.getText());
            onBackPressed();}

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }
}