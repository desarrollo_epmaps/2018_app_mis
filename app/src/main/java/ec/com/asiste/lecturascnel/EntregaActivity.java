package ec.com.asiste.lecturascnel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.configuracion.codigoEntregaCNEL;
import com.asistecom.worker.web.entity.visitasOut.CodigoObservacion;
import com.asistecom.worker.web.entity.visitasOut.Coordenada;
import com.asistecom.worker.web.entity.visitasOut.EntregaFactura;
import com.asistecom.worker.web.entity.visitasOut.Notificacion;
import com.asistecom.worker.web.entity.visitasOut.medidor;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Nombrar_fotos;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.SimpleGestureFilter;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.util;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.util.formatearfechaString;
import static ec.com.asiste.lecturascnel.logica.util.formatearhora;
import static ec.com.asiste.lecturascnel.logica.util.getCurrentTimeStamp;
import static ec.com.asiste.lecturascnel.logica.util.rellenarCerosIzquierda;

public class EntregaActivity extends AppCompatActivity implements LocationListener, SimpleGestureFilter.SimpleGestureListener {

    Button bfoto;
    String nombre_foto = "";
    private visitas vt = new visitas();
    Spinner s_obs, s_imp;
    private SimpleGestureFilter detector;
    TextView tv_cuenta,tv_ciclo, tv_sector, tv_ruta, tv_seciencia, tv_cliente, tv_direccion,tv_sr,tv_srf,tv_marca,tv_fotos;
    EditText  et_contrato;
    EditText et_lectura;
    EditText et_obs;
    static EditText cuenta;
    static EditText noti;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    TextView txtLat;
    String lat = "";
    String lonn = "";
    String sat = "";
    String dao = "";
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    ArrayList<String> fotos = new ArrayList<String>();
    TextView tv_gps;
    String mCurrentPhotoPath;
    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrega);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        btn = (Button) findViewById(R.id.bt_e_scan);
        detector = new SimpleGestureFilter(EntregaActivity.this, this);
        //s_imp = (Spinner)findViewById(R.id.spinner_e_imp) ;
        s_obs = (Spinner)findViewById(R.id.spinner_e_obs) ;
        bfoto =(Button)findViewById(R.id.bt_ent_foto);
        tv_gps = (TextView) findViewById(R.id.tv_e_gps);

        tv_gps = (TextView) findViewById(R.id.tv_e_gps);
        tv_cliente = (TextView)findViewById(R.id.tv_e_nombre);
        tv_sector  = (TextView)findViewById(R.id.tv_e_sector);

        tv_direccion  = (TextView)findViewById(R.id.tv_e_dir);
        bfoto.setEnabled(false);
        //et_contrato  = (EditText) findViewById(R.id.et_contrato);
        tv_cuenta  = (TextView) findViewById(R.id.tv_e_cta);
        tv_marca  = (TextView) findViewById(R.id.tv_e_marca);
        tv_sr  = (TextView) findViewById(R.id.tv_e_sr);
        tv_srf = (TextView) findViewById(R.id.tv_e_srf);
        et_lectura  = (EditText) findViewById(R.id.et_e_lectura);
        et_obs = (EditText) findViewById(R.id.et_e_obs);
        cuenta  = (EditText) findViewById(R.id.et_e_baracuenta);
        noti = (EditText) findViewById(R.id.et_e_baranoti);
        cargarR("");
        cuenta.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("")) {
                    //buscar cuenta
                    cargarR("");
                    //mapVisita(vt);
                    bfoto.setEnabled(true);
                }
                else
                {
                    bfoto.setEnabled(false);
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        //cargarR("");
        //mapVisita(vt);
        try {
            cargarObs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EntregaActivity.this, ScanActivity.class);
                startActivity(intent);
            }
        });
    }
    public void limpiar()
    {

        bfoto.setText("FOTO("+String.valueOf(0)+")");
        try {

        }
        catch (Exception ex)
        {

        }
        et_lectura.setText("");
        et_obs.setText("");
        cuenta.setText("");
        s_obs.setSelection(0);
        fotos.clear();
        bfoto.setEnabled(false);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    public void cargarR(String previa )
    {
        try {

            sqLite sq = new sqLite();
            vt = new visitas();
            vt = sq.cuentaR_sql(Parametros.res, previa);
        }
        catch (Exception ex)
        {

        }

    }
    public void cargarB(String previa )
    {
        try {

            sqLite sq = new sqLite();
            vt = new visitas();
            vt = sq.cuenta_sql(previa);
        }
        catch (Exception ex)
        {

        }

    }
    public void cargarObs() throws Exception {


        List<codigoEntregaCNEL> cobs = new ArrayList<codigoEntregaCNEL>();
        codigoEntregaCNEL cod_vacio = new codigoEntregaCNEL();
        cod_vacio.idEntrega = "";
        cod_vacio.tipoEntrega = "";
        cod_vacio.descEntrega = "";

        cobs.add(cod_vacio);
        String cods = fileManager.ReadFile(getApplicationContext(),Parametros.dir_archivos.toString(),"/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class,cods);
        for (codigoEntregaCNEL cc: v.getCodigoEntrega()
                ) {


                cobs.add(cc);

        }

        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cobs);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_obs.setAdapter(adapter2);




    }
    public void cargarL(String previa )
    {

        try {
            sqLite sq = new sqLite();
            vt = new visitas();
            vt = sq.cuentaL_sql(Parametros.res, previa);
        }
        catch (Exception ex)
        {}

    }
    public void mapVisita(visitas vt)
    {
        bfoto.setText("FOTO("+String.valueOf(0)+")");
        try {

        }
        catch (Exception ex)
        {

        }
        et_lectura.setText("");
        et_obs.setText("");

        //s_obs.setSelection(0);
        fotos.clear();
        try {


            //tv_ciclo.setText(vt.ordenLectura.porcion);
            tv_direccion.setText(vt.ordenLectura.direccion);
           //tv_seciencia.setText(vt.ordenLectura.secuencia);
            tv_sector.setText(vt.ordenLectura.sector.toString()+"-"+vt.ordenLectura.ruta.toString()+"-"+ vt.ordenLectura.secuencia);
           // tv_ruta.setText(vt.ordenLectura.ruta);
            tv_cliente.setText(vt.ordenLectura.informacionAdicional.getNombreCliente());
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);

            try {
                tv_sr.setText(vt.ordenLectura.numeroMedidor);
            }catch (Exception ex)
            {}
            try {
                tv_srf.setText(vt.ordenLectura.numeroDeInstalacion);
            }catch (Exception ex)
            {}
            try {
                tv_marca.setText(vt.ordenLectura.fabricante);
            }catch (Exception ex)
            {}

            //et_contrato.setText(vt.ordenLectura.ordenLectura);
        }
        catch (Exception exx)
        {

            int a = 0;
        }


    }
    @Override
    public void onSwipe(int direction) {

        //Detect the swipe gestures and display toast
        String showToastMessage = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                showToastMessage = "You have Swiped Right.";
                cargarL(vt.ordenLectura.ordenLectura);
                //mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                showToastMessage = "You have Swiped Left.";
                cargarR(vt.ordenLectura.ordenLectura);
                //mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                showToastMessage = "You have Swiped Down.";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                showToastMessage = "You have Swiped Up.";
                break;

        }
        Toast.makeText(this, showToastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {

    }


    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);

        lat = location.getLatitude()+"";
        lonn =   location.getLongitude()+"";
        tv_gps.setText("GPS: "+ lat + "  ,  " + lonn );
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                fotos.add(util.renombraFotoVideo(nombre_foto));
                int cantidad = fotos.size();
                //tv_fotos.setText(String.valueOf(cantidad));
                bfoto.setText("FOTO("+String.valueOf(cantidad)+")");
            }
        }
        catch (Exception ex)
        {}

    }

    public void tomar_foto_l(View view) throws IOException {
        dispatchTakePictureIntent();


    }
    public boolean validar_foto_video()
    {
        boolean resultado = false;
        int f = 0;
        int v = 0;

        for (String vv : fotos
                ) {
            if(vv.contains(".jpg"))
            {
                f += 1;
            }
            else if (vv.contains(".mp4"))
            {

                v += 1;

            }

        }

        if( f >= Parametros.foto_entregas)
        {
            resultado = true;
        }

        return resultado;

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    public void guarda_file() throws Exception {
        com.asistecom.worker.web.entity.visitasOut.OrdenLectura odl = new com.asistecom.worker.web.entity.visitasOut.OrdenLectura();
        String orden = "1"+util.rellenarCerosIzquierda(vt.ordenLectura.cecgsecu.toString(),4)
                +rellenarCerosIzquierda(cuenta.getText().toString(),12)+"18";
        odl.setOrdenLectura(orden);
        odl.setCecgsecu(vt.ordenLectura.getCecgsecu());
        odl.setMedidor(new medidor());
        odl.setNumeroDeInstalacionPrincipal("");
        odl.setClaseLectura("0");
        Coordenada coord = new Coordenada() ;
        coord.setCoord_X(lat);
        coord.setCoord_Y(lonn);
        odl.setNumFactura("0");
        ArrayList<Coordenada> liscord = new ArrayList<Coordenada>();
        liscord.add(coord);
        odl.setCiclo(vt.ordenLectura.porcion.toString());
        odl.setCoordenada(liscord);
        odl.setCuentaContrato(cuenta.getText().toString());
        odl.setEnServicioOut(true);
        odl.setClosedvisits(vt.ordenLectura.close);
        odl.setProcessedvisits(vt.ordenLectura.processed);
        odl.setContinuousCommunication(vt.ordenLectura.continius);
        odl.setFoto(fotos);
        Date currentTime = Calendar.getInstance().getTime();
        odl.setFechaLecturaActual(formatearfechaString(currentTime));
        odl.setHoraLecturaActual(formatearhora(currentTime));
        odl.setLecturaActual(et_lectura.getText().toString());
        odl.setLecturaReal(et_lectura.getText().toString());
        odl.setIdLector(Parametros.usuario.toString());
        odl.setTipoActividad(vt.ordenLectura.actividadLectura);
        EntregaFactura entregaFactura = new EntregaFactura();
        entregaFactura.setFechaEntrega(formatearfechaString(currentTime));
        entregaFactura.setHoraEntrega(formatearhora(currentTime));

        codigoEntregaCNEL ce = (codigoEntregaCNEL) s_obs.getSelectedItem();
        entregaFactura.setIdEntregaFactura(ce.idEntrega);
        odl.setEntregaFactura(entregaFactura);
        if(noti.getText().length() > 0 )
        {

            ArrayList<Notificacion> ne = new ArrayList<Notificacion>();
            Notificacion nt = new Notificacion();
            nt.setId(noti.getText().toString());
            nt.setCodEntrega(ce.idEntrega);
            ne.add(nt);
            odl.setNotificacion(ne);

        }
        odl.setTipoActividad(vt.ordenLectura.actividadLectura.toString());
        odl.setNumeroDeInstalacion(vt.ordenLectura.numeroDeInstalacion);
        odl.setPeriodo(vt.ordenLectura.getPeriodo());
        odl.setRuta(vt.ordenLectura.getRuta());
        odl.setSecuencia(vt.ordenLectura.getSecuencia());
        odl.setTipoEmisionFactura("E");
        odl.setTipoEnvio("1");
        odl.setSobrante(false);
        odl.setIdProyecto(vt.ordenLectura.idProyecto);
        odl.setObservacionAlfanumerica(et_obs.getText().toString());
        CodigoObservacion c = new CodigoObservacion();
        c.setCodigo("1");
        ArrayList<CodigoObservacion> cc = new ArrayList<>();


        odl.setCodigoObservacion(cc);

        String version = Parametros.version;
        odl.setVersion(version);
        Serializer serializer = new Persister();
        File result = new File( FilenameUtils.getPath(vt.ordenLectura.pathout)+"sob_"+orden+".xml");
        try {
            com.asistecom.worker.web.entity.visitasOut.visitas vv = new com.asistecom.worker.web.entity.visitasOut.visitas();
            vv.setOrdenLectura(odl);
            serializer.write(vv, result);
            //sqLite ss = new sqLite();
            //ss.marcarLeido(vt.ordenLectura.ordenLectura.toString());
            String showToastMessage = "GUARDADO.";
            cargarR("");
            Toast.makeText(this, showToastMessage, Toast.LENGTH_LONG).show();
            limpiar();


        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {


            }
        });

    }
    public void guardarLectura(View v) throws Exception {

        if(validar_foto_video())
        {

            guarda_file();
        }
        else
        {

            Toast.makeText(this,"Tome las fotos necesarias",Toast.LENGTH_LONG).show();
        }

    }
    public void takePicAndDisplayIt(View view) throws IOException {
        Nombrar_fotos nf = new Nombrar_fotos();
        String nombre = nf.foto_cnel(vt.ordenLectura.cuentaContrato, getCurrentTimeStamp(),vt.ordenLectura.periodo);
        File file =  file =  util.createImageFile(Parametros.dir_fotos+"/"+nombre);
        fotos.add(nombre);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, Uri.fromFile(file));
        if (intent.resolveActivity(getPackageManager()) != null) {

            try {
                file =  util.createImageFile(Parametros.dir_fotos+"/"+nombre);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        }
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private File createImageFile() throws IOException, ParseException {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String orden = "1"+util.rellenarCerosIzquierda(vt.ordenLectura.cecgsecu.toString(),4)
                +rellenarCerosIzquierda(cuenta.getText().toString(),12)+"18";
        String nom = nf.foto_cnel(cuenta.getText().toString() , orden , Parametros.idProyecto, formatearfechaString(now),formatearhora(now),vt.ordenLectura.periodo, vt.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName


        File image = new File(storageDir+"/"+nom);
         nombre_foto = nom;


        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
