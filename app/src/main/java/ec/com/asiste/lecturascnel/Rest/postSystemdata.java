package ec.com.asiste.lecturascnel.Rest;

import android.os.AsyncTask;

import com.asistecom.worker.web.entity.Resultado;
import com.asistecom.worker.web.entity.resources.Resource;
import com.asistecom.worker.web.entity.resources.Resources;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import ec.com.asiste.lecturascnel.logica.Parametros;

public class postSystemdata extends AsyncTask<String , Void , String> {

    public postSystemdata(String s) {
        doInBackground("");
    }

    public postSystemdata() {
        doInBackground("");
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Serializer serializer = new Persister();
        Resources r = null  ;

        //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");

        try {
            //Resources r = serializer.read(Resources.class, source);
            r = serializer.read(Resources.class,s.toString());
            final Resources finalR = r;
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    //new postSystemdata("").execute("", "", "");

                    getSystemdata runner2= new getSystemdata();
                    runner2.execute(finalR,"","");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected String doInBackground(String... strings) {
        URL url = null;
        try {
            url = new URL(Parametros.ipInterna+ ":"+Parametros.puertoInterna+Parametros.servicioConfig+"/systemdata/"+Parametros.idProyecto);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        connection.setRequestProperty("content-type", "application/xml");
        connection.setDoOutput(true);
        connection.setConnectTimeout(30000);
        OutputStreamWriter wr = null;
        try {
            wr = new OutputStreamWriter(connection.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            wr.write("<resources>\t<resource name=\"\" lastUpdate=\"\"></resource></resources>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wr.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (connection.getResponseCode() != 200) {
                //throw new BetterLuckNextTimeException(...);

                return "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())

            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        String line = null;

        StringBuilder sb = new StringBuilder();
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (line != null) {
            sb.append(line);
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.disconnect();

        return sb.toString();

    }

}
