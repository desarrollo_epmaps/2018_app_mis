package ec.com.asiste.lecturascnel;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.visitasOut.Parametro;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.dblite.AdminSQLiteOpenHelper;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.fileResumenRuta;
import ec.com.asiste.lecturascnel.logica.util;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity {
    EditText et_usuario, et_pdw;
    Button bt_login, bt_datosBase;
    Spinner sp_proyecto;
    TextView tv_mac, tv_version, tv_proyecto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_usuario = (EditText) findViewById(R.id.et_l_usuario);
        et_pdw = (EditText) findViewById(R.id.et_l_pwd);
        tv_mac = (TextView) findViewById(R.id.tv_l_mac);
        tv_version = (TextView) findViewById(R.id.tv_l_version);
        tv_proyecto = (TextView)findViewById(R.id.tv_l_proyecto);
        sp_proyecto = (Spinner)findViewById(R.id.sp_l_proyecto);
        Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES)+"/compact";
        Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Parametros.dir_videos = getExternalFilesDir(Environment.DIRECTORY_MOVIES);
        Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        Parametros.dir_archivos = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        Parametros.contexto = getApplicationContext();
        sp_proyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(sp_proyecto.getSelectedItemPosition() > 0) {
                    SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferencias.edit();
                    try {
                        editor.putString("idProyecto", sp_proyecto.getSelectedItem().toString().split("-")[1]);
                    } catch (Exception ex) {
                        editor.putString("idProyecto", "0");
                    }
                    editor.commit();
                    validar_proyecto();
                }
                // Your code here
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        List<fileResumenRuta>  listado  = fileManager.listaRutas();
        cargarProyectos();
        validar_proyecto();

        try{

            File dir = new File(Parametros.dir_fotosC);
            dir.mkdir();

        }
        catch (Exception e)
        {}


        try {


            String[] perms = {"android.permission.READ_PHONE_STATE", "android.permission.CAMERA",
                    "android.permission.ACCESS_FINE_LOCATION","android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.WRITE_EXTERNAL_STORAGE","android.permission.ACCESS_FINE_LOCATION","android.permission.BLUETOOTH_ADMIN"};


            int permsRequestCode = 200;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }

        }
        catch (Exception permEx)
        {


        }

        tv_version.setText("V:"+util.getVersion(getApplicationContext())+"");
        Parametros.version = util.getVersion(getApplicationContext());
        tv_mac.setText("ID:"+util.getMAC(getApplicationContext()));

        Parametros.mac = util.getMAC(getApplicationContext());
        //writeVersion();
    }
    public void cargarProyectos()
    {

        List<String> cimp = new ArrayList<String>();



        cimp.add("");
        cimp.add("EPMAPS-11");
        cimp.add("CNELMAN-21");
        cimp.add("CNELGYE-31");
        cimp.add("CNELMIL-32");
        cimp.add("CNELERO-33");
        cimp.add("CNELESM-34");
        cimp.add("CNELSUC-35");
        cimp.add("INTFES-41");
        cimp.add("INTOT-42");
        cimp.add("TEST-99");


        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cimp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proyecto.setAdapter(adapter);
    }
    public boolean validar_proyecto()
    {
        boolean resultado = true;
        SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
        String id_proyecto =  preferencias.getString("idProyecto","0");
        if(id_proyecto.equals("0")|| id_proyecto.equals(""))
        {
            mostrar_proyecto();
            return false;
        }
        else
        {
            Parametros.idProyecto = id_proyecto;
            ocultar_proyecto();

        }
        return  resultado;

    }
    public void mostrar_proyecto()
    {
        tv_proyecto.setVisibility(View.VISIBLE);
        sp_proyecto.setVisibility(View.VISIBLE);
    }
    public void ocultar_proyecto()
    {
        tv_proyecto.setVisibility(View.INVISIBLE);
        sp_proyecto.setVisibility(View.INVISIBLE);
    }
    public void writeVersion()
    {

        File extStore = Environment.getExternalStorageDirectory();

        String filename =extStore+ "/SISTECOM.V";
        String fileContents =  util.getVersion(getApplicationContext());
        FileOutputStream outputStream;

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void accionlog(View view) throws Exception {
        String cods = fileManager.ReadFile(getApplicationContext(),Parametros.dir_archivos.toString(),"/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class,cods);
        Parametros.setFotos_lecturas(Integer.parseInt(v.getFotoLectura().toString()));
        Parametros.setFoto_entregas(Integer.parseInt(v.getFotoEntrega().toString()));
        Parametros.setFoto_sobrante(Integer.parseInt(v.getFotoSobrnate().toString()));
        Parametros.setPedirReves(Integer.parseInt(v.getPedirReves().toString()));
        Parametros.setTiempoAutoEnvio(Integer.parseInt(v.getTiempoAutoEnvio().toString()));
        try
        {

            if(et_usuario.getText().toString().equals("9999") && et_pdw.getText().toString().equals(Parametros.password9999))
            {
                SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                Parametros.zebra = preferencias.getString("zebra", "0:0:0:0");
                Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/compact";
                Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
                et_pdw.setText("");
                Intent i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
            }
            else
            {
                sqLite sql = new sqLite();
                //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
                if(sql.validar(et_usuario.getText().toString(), et_pdw.getText().toString())) {

                    SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferencias.edit();
                    editor.putString("usuario", et_usuario.getText().toString());
                    editor.commit();

                    Parametros.zebra = preferencias.getString("zebra", "0:0:0:0");
                    Parametros.dir_fotos = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    Parametros.dir_fotosC = getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/compact";
                    Parametros.dir_files = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
                    Intent i = new Intent(this, PrincipalActivity.class);
                    et_pdw.setText("");
                    startActivity(i);
                }
                else
                {

                    Toast.makeText(this, "ERROR USUARIO/CONTRASEÑA", Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (Exception ex)
        {
            Toast.makeText(this, "LOGIN ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    public void alerta()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("REGISTRAR DISPOSITIVO");
        builder.setMessage("Desea registrar?");

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                Toast toast = Toast.makeText(getApplicationContext(), "Registrando...", Toast.LENGTH_SHORT); toast.show();
                regg();
                dialog.dismiss();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast toast = Toast.makeText(getApplicationContext(), "Registro cancelado", Toast.LENGTH_SHORT); toast.show();
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    public void registroD(View view)
    {
       alerta();

    }
    public void regg()
    {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //new postSystemdata("").execute("", "", "");
                    try {
                        //tv_mac.setText("ID:"+util.getMAC(getApplicationContext()));
                        Parametros.setMAC(util.getMAC(getApplicationContext()));
                        String[] parametross = {"/" + et_usuario.getText().toString() + "/" + Parametros.getMAC().toString() + "/false", et_usuario.getText().toString(), Parametros.getMAC().toString()};
                        new postRegisterDevice(parametross);
                        Parametros.setContexto(getBaseContext());
                        new postSystemdata().execute("", "", "");
                    }
                    catch (Exception e)
                    {
                      int a = 0;
                    }

            }
        });
    }
}
