package ec.com.asiste.lecturascnel.dblite;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;


import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.vistiasIn.OrdenLectura;
import com.asistecom.worker.web.entity.vistiasIn.informacionAdicional;
import com.asistecom.worker.web.entity.vistiasIn.visitas;
import com.asistecom.worker.web.entity.workblock.Work;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.ResumenRuta;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.resumenRecin;
import ec.com.asiste.lecturascnel.logica.util;

public class sqLite {
    public sqLite() {
    }

    public boolean validar(String usr , String pwd)
    {

        boolean resultado = false;
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery(
                "select usuario from usuarios where codigo= " + usr + " and password = '"+ pwd     +"'", null);
        if(fila.moveToFirst())
        {
            Parametros.nomberUsuario = fila.getString(0);
            resultado= true;

        }
        if(resultado)
        {

            Parametros.setUsuario( usr.toString());
        }
        fila.close();
        bd.close();
        return resultado;

    }
    public void eliminarRuta(String ciclo , String sector , String ruta)
    {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto, "administracion", null, 1);


        SQLiteDatabase bd = admin.getWritableDatabase();
        try {
            sqLite sql = new sqLite();
            //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
            bd.execSQL("delete from recin where ciclo = '"+ util.rellenarCerosIzquierda(ciclo,2)+"' and sector = '"+sector+"' and ruta = '"+ruta+"'");
            System.out.println("execute delete");
        } catch (Exception ex) {

        }

    }
    public void insertarGlobal(String global)
    {
        try {
            String[] split = global.split(";");

            AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto, "administracion", null, 1);


            SQLiteDatabase bd = admin.getWritableDatabase();
            try {
                sqLite sql = new sqLite();
                //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
                bd.execSQL("delete from global");
                System.out.println("execute delete");
            } catch (Exception ex) {

            }

            for (String ss : split
                    ) {
                String[] gs = ss.split("!");

                String cuenta = gs[0].toString();
                String sr = gs[1].toString();
                String srf = gs[2].toString();

                String marca_medidor = gs[3].toString();
                String nombre = "";
                String direccion  = "";
                try {
                    nombre =   gs[4].toString();
                    direccion  =  gs[5].toString();
                }
                catch (Exception ex)
                {
                    System.out.println(ex.getMessage());
                }


                ContentValues registro = new ContentValues();
                registro.put("cuenta", cuenta);
                registro.put("sr", sr);
                registro.put("srf", srf);
                registro.put("nombre", nombre);
                registro.put("marca_medidor", marca_medidor);
                registro.put("direccion", direccion);
                bd.insert("global", null, registro);
                System.out.println("execute insert global");
            }

            System.out.println("execute insert global terminado ..................................");
            bd.close();
        }
        catch (Exception error){

            int a =0;
            System.out.println(error.getMessage());
        }




    }
    public void insertarUsuarios(String usuarios)
    {
        try {
            String[] split = usuarios.split(";");

            AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto, "administracion", null, 1);


            SQLiteDatabase bd = admin.getWritableDatabase();
            try {
                sqLite sql = new sqLite();
                //sql.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
                bd.execSQL("delete from usuarios");
                System.out.println("execute delete");
            } catch (Exception ex) {

            }

            for (String ss : split
                    ) {
                String[] partes = ss.split("\\|");
                int cod = Integer.parseInt(partes[1]);

                String proyecto = partes[2];
                String pass = partes[3];
                String usr = partes[1];
                String tip = partes[4];;

                ContentValues registro = new ContentValues();
                registro.put("codigo", cod);
                registro.put("usuario", usr);
                registro.put("password", pass);
                registro.put("proyecto", proyecto);
                registro.put("tipo", tip);
                bd.insert("usuarios", null, registro);
                System.out.println("execute insert");
            }


            bd.close();
        }
        catch (Exception error){

            int a =0;
        }




    }
    public boolean logintest(String usr, String pwd)
    {
        try {
            System.out.println("execute select ");
            boolean resultado = false;
            AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                    "administracion", null, 1);
            SQLiteDatabase bd = admin.getWritableDatabase();

            Cursor fila = bd.rawQuery(
                    "select * from usuarios where codigo= " + usr + " and password = '" + pwd + "'", null);
            if (fila.moveToFirst()) {

                return true;
            }
            bd.close();
            return resultado;
        }
        catch (Exception ex)
        {

            System.out.println("execute on create "+ex.getMessage());
            return false;
        }
    }
    public static List<String> splitEqually(String text, int size) {
        // Give the list the right capacity to start with. You could use an array
        // instead if you wanted.
        List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

        for (int start = 0; start < text.length(); start += size) {
            ret.add(text.substring(start, Math.min(text.length(), start + size)));
        }
        return ret;
    }
    public void insertarRECIN(visitas visitas)
    {
        List<String> split = splitEqually("", 72);

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,"administracion", null, 1);


        SQLiteDatabase bd = admin.getWritableDatabase();

        String	porcion	= visitas.ordenLectura.getPorcion();
        String	cecgsecu	=	visitas.ordenLectura.getCecgsecu();
        String	periodo	=	visitas.ordenLectura.getPeriodo();

        String	ordenLectura	=	visitas.ordenLectura.getOrdenLectura();
        String	cuentaContrato	=	visitas.ordenLectura.getCuentaContrato();
        String	ruta	=	visitas.ordenLectura.getRuta();
        String	sector	=	visitas.ordenLectura.getSector();
        String	mes	=	visitas.ordenLectura.getMes();
        String	secuencia	=	visitas.ordenLectura.getSecuencia();
        String	piso	=	visitas.ordenLectura.getPiso();
        String	manzana	=	visitas.ordenLectura.getManzana();
        String	departamento	=	visitas.ordenLectura.getDepartamento();
        String	direccion	=	visitas.ordenLectura.getDireccion();
        String	direccionTrans	=	visitas.ordenLectura.getDireccionTrans();
        String	direccionRef	=	visitas.ordenLectura.getDireccionRef();
        String	mensajeLector	=	visitas.ordenLectura.getMensajeLector();
        String	nombreSector	=	visitas.ordenLectura.getNombreSector();
        String	placaPredial	=	visitas.ordenLectura.getPlacaPredial();
        String	actividadLectura	=	visitas.ordenLectura.getActividadLectura();
        String	numeroDeInstalacion	=	visitas.ordenLectura.getNumeroDeInstalacion();
        String	idEstadoInstalacion	=	visitas.ordenLectura.getIdEstadoInstalacion();
        String	idEstadoMedidor	=	visitas.ordenLectura.getIdEstadoMedidor();
        String	numeroMedidor	=	visitas.ordenLectura.getNumeroMedidor();
        String	fabricante	=	visitas.ordenLectura.getFabricante();
        String	modelo	=	visitas.ordenLectura.getModelo();
        String	fechaLecturaAnterior	=	visitas.ordenLectura.getFechaLecturaAnterior();
        String	lecturaAnterior	=	visitas.ordenLectura.getLecturaAnterior();
        String	lecturaMin	=	visitas.ordenLectura.getLecturaMin();
        String	lecturaMax	=	visitas.ordenLectura.getLecturaMax();
        String	idCodigoUbicacion	=	visitas.ordenLectura.getIdCodigoUbicacion();
        String	tipoInstalacion	=	visitas.ordenLectura.getTipoInstalacion();
        String	consumoPromedio	=	visitas.ordenLectura.getConsumoPromedio();
        String	metodoConsumoPrevio	=	visitas.ordenLectura.getMetodoConsumoPrevio();
        String	nombreCliente	=	visitas.ordenLectura.getInformacionAdicional().getNombreCliente();
        String	numeroTelefono	=	visitas.ordenLectura.getInformacionAdicional().getNumeroTelefono();
        String	imprimir	=	visitas.ordenLectura.getInformacionAdicional().getImprimir();
        String	idProyecto	=	visitas.ordenLectura.getIdProyecto();

        ContentValues registro = new ContentValues();
        registro.put("porcion", porcion);
        registro.put("cecgsecu", cecgsecu);
        registro.put("periodo", periodo);

        registro.put("ordenLectura", ordenLectura);
        registro.put("cuentaContrato", cuentaContrato);
        registro.put("ruta", ruta);
        registro.put("sector", sector);
        registro.put("mes", mes);
        registro.put("secuencia", secuencia);
        registro.put("piso", piso);
        registro.put("manzana", manzana);
        registro.put("departamento", departamento);
        registro.put("direccion", direccion);
        registro.put("direccionTrans", direccionTrans);
        registro.put("direccionRef", direccionRef);
        registro.put("mensajeLector", mensajeLector);
        registro.put("nombreSector", nombreSector);
        registro.put("placaPredial", placaPredial);
        registro.put("actividadLectura", actividadLectura);
        registro.put("numeroDeInstalacion", numeroDeInstalacion);
        registro.put("idEstadoInstalacion", idEstadoInstalacion);
        registro.put("idEstadoMedidor", idEstadoMedidor);
        registro.put("numeroMedidor", numeroMedidor);
        registro.put("fabricante", fabricante);
        registro.put("modelo", modelo);
        registro.put("fechaLecturaAnterior", fechaLecturaAnterior);
        registro.put("lecturaAnterior", lecturaAnterior);
        registro.put("lecturaMin", lecturaMin);
        registro.put("lecturaMax", lecturaMax);
        registro.put("idCodigoUbicacion", idCodigoUbicacion);
        registro.put("tipoInstalacion", tipoInstalacion);
        registro.put("consumoPromedio", consumoPromedio);
        registro.put("metodoConsumoPrevio", metodoConsumoPrevio);
        registro.put("nombreCliente", nombreCliente);
        registro.put("numeroTelefono", numeroTelefono);
        registro.put("imprimir", imprimir);
        registro.put("idProyecto", idProyecto);

        bd.insert("recin", null, registro);
        System.out.println("execute insert Recin");



        bd.close();



    }

    public void insertarRECIN(visitas visitas, SQLiteDatabase bd)
    {



        if(bd.isOpen()){

        }
        else
        {
        }
        String	porcion	= visitas.ordenLectura.getPorcion();
        String	cecgsecu	=	visitas.ordenLectura.getCecgsecu();
        String	periodo	=	visitas.ordenLectura.getPeriodo();

        String	ordenLectura	=	visitas.ordenLectura.getOrdenLectura();
        String	cuentaContrato	=	visitas.ordenLectura.getCuentaContrato();
        String	ruta	=	visitas.ordenLectura.getRuta();
        String	sector	=	visitas.ordenLectura.getSector();
        String	mes	=	visitas.ordenLectura.getMes();
        String	secuencia	=	visitas.ordenLectura.getSecuencia();
        String	piso	=	visitas.ordenLectura.getPiso();
        String	manzana	=	visitas.ordenLectura.getManzana();
        String	departamento	=	visitas.ordenLectura.getDepartamento();
        String	direccion	=	visitas.ordenLectura.getDireccion();
        String	direccionTrans	=	visitas.ordenLectura.getDireccionTrans();
        String	direccionRef	=	visitas.ordenLectura.getDireccionRef();
        String	mensajeLector	=	visitas.ordenLectura.getMensajeLector();
        String	nombreSector	=	visitas.ordenLectura.getNombreSector();
        String	placaPredial	=	visitas.ordenLectura.getPlacaPredial();
        String	actividadLectura	=	visitas.ordenLectura.getActividadLectura();
        String	numeroDeInstalacion	=	visitas.ordenLectura.getNumeroDeInstalacion();
        String	idEstadoInstalacion	=	visitas.ordenLectura.getIdEstadoInstalacion();
        String	idEstadoMedidor	=	visitas.ordenLectura.getIdEstadoMedidor();
        String	numeroMedidor	=	visitas.ordenLectura.getNumeroMedidor();
        String	fabricante	=	visitas.ordenLectura.getFabricante();
        String	modelo	=	visitas.ordenLectura.getModelo();
        String	fechaLecturaAnterior	=	visitas.ordenLectura.getFechaLecturaAnterior();
        String	lecturaAnterior	=	visitas.ordenLectura.getLecturaAnterior();
        String	lecturaMin	=	visitas.ordenLectura.getLecturaMin();
        String	lecturaMax	=	visitas.ordenLectura.getLecturaMax();
        String	idCodigoUbicacion	=	visitas.ordenLectura.getIdCodigoUbicacion();
        String	tipoInstalacion	=	visitas.ordenLectura.getTipoInstalacion();
        String	consumoPromedio	=	visitas.ordenLectura.getConsumoPromedio();
        String	metodoConsumoPrevio	=	visitas.ordenLectura.getMetodoConsumoPrevio();
        String	nombreCliente	=	visitas.ordenLectura.getInformacionAdicional().getNombreCliente();
        String	numeroTelefono	=	visitas.ordenLectura.getInformacionAdicional().getNumeroTelefono();
        String	imprimir	=	visitas.ordenLectura.getInformacionAdicional().getImprimir();
        String	idProyecto	=	visitas.ordenLectura.getIdProyecto();

        ContentValues registro = new ContentValues();
        registro.put("porcion", porcion);
        registro.put("cecgsecu", cecgsecu);
        registro.put("periodo", periodo);

        registro.put("ordenLectura", ordenLectura);
        registro.put("cuentaContrato", cuentaContrato);
        registro.put("ruta", ruta);
        registro.put("sector", sector);
        registro.put("mes", mes);
        registro.put("secuencia", secuencia);
        registro.put("piso", piso);
        registro.put("manzana", manzana);
        registro.put("departamento", departamento);
        registro.put("direccion", direccion);
        registro.put("direccionTrans", direccionTrans);
        registro.put("direccionRef", direccionRef);
        registro.put("mensajeLector", mensajeLector);
        registro.put("nombreSector", nombreSector);
        registro.put("placaPredial", placaPredial);
        registro.put("actividadLectura", actividadLectura);
        registro.put("numeroDeInstalacion", numeroDeInstalacion);
        registro.put("idEstadoInstalacion", idEstadoInstalacion);
        registro.put("idEstadoMedidor", idEstadoMedidor);
        registro.put("numeroMedidor", numeroMedidor);
        registro.put("fabricante", fabricante);
        registro.put("modelo", modelo);
        registro.put("fechaLecturaAnterior", fechaLecturaAnterior);
        registro.put("lecturaAnterior", lecturaAnterior);
        registro.put("lecturaMin", lecturaMin);
        registro.put("lecturaMax", lecturaMax);
        registro.put("idCodigoUbicacion", idCodigoUbicacion);
        registro.put("tipoInstalacion", tipoInstalacion);
        registro.put("consumoPromedio", consumoPromedio);
        registro.put("metodoConsumoPrevio", metodoConsumoPrevio);
        registro.put("nombreCliente", nombreCliente);
        registro.put("numeroTelefono", numeroTelefono);
        registro.put("imprimir", imprimir);
        registro.put("idProyecto", idProyecto);
        bd.insert("recin", null, registro);
        System.out.println("execute insert Recin");



        bd.close();



    }
    public void insertarRECIN(visitas visitas, SQLiteDatabase bd,  Work wk, String inp ,String outp )
    {



        if(bd.isOpen()){

        }
        else
        {
        }
        String	porcion	= visitas.ordenLectura.getPorcion();
        String	cecgsecu	=	visitas.ordenLectura.getCecgsecu();
        String	periodo	=	visitas.ordenLectura.getPeriodo();

        String	ordenLectura	=	visitas.ordenLectura.getOrdenLectura();
        String	cuentaContrato	=	visitas.ordenLectura.getCuentaContrato();
        String	ruta	=	visitas.ordenLectura.getRuta();
        String	sector	=	visitas.ordenLectura.getSector();
        String	mes	=	visitas.ordenLectura.getMes();
        String	secuencia	=	visitas.ordenLectura.getSecuencia();
        String	piso	=	visitas.ordenLectura.getPiso();
        String	manzana	=	visitas.ordenLectura.getManzana();
        String	departamento	=	visitas.ordenLectura.getDepartamento();
        String	direccion	=	visitas.ordenLectura.getDireccion();
        String	direccionTrans	=	visitas.ordenLectura.getDireccionTrans();
        String	direccionRef	=	visitas.ordenLectura.getDireccionRef();
        String	mensajeLector	=	visitas.ordenLectura.getMensajeLector();
        String	nombreSector	=	visitas.ordenLectura.getNombreSector();
        String	placaPredial	=	visitas.ordenLectura.getPlacaPredial();
        String	actividadLectura	=	visitas.ordenLectura.getActividadLectura();
        String	numeroDeInstalacion	=	visitas.ordenLectura.getNumeroDeInstalacion();
        String	idEstadoInstalacion	=	visitas.ordenLectura.getIdEstadoInstalacion();
        String	idEstadoMedidor	=	visitas.ordenLectura.getIdEstadoMedidor();
        String	numeroMedidor	=	visitas.ordenLectura.getNumeroMedidor();
        String	fabricante	=	visitas.ordenLectura.getFabricante();
        String	modelo	=	visitas.ordenLectura.getModelo();
        String	fechaLecturaAnterior	=	visitas.ordenLectura.getFechaLecturaAnterior();
        String	lecturaAnterior	=	visitas.ordenLectura.getLecturaAnterior();
        String	lecturaMin	=	visitas.ordenLectura.getLecturaMin();
        String	lecturaMax	=	visitas.ordenLectura.getLecturaMax();
        String	idCodigoUbicacion	=	visitas.ordenLectura.getIdCodigoUbicacion();
        String	tipoInstalacion	=	visitas.ordenLectura.getTipoInstalacion();
        String	consumoPromedio	=	visitas.ordenLectura.getConsumoPromedio();
        String	metodoConsumoPrevio	=	visitas.ordenLectura.getMetodoConsumoPrevio();
        String	nombreCliente	=	visitas.ordenLectura.getInformacionAdicional().getNombreCliente();
        String	numeroTelefono	=	visitas.ordenLectura.getInformacionAdicional().getNumeroTelefono();
        String	imprimir	=	visitas.ordenLectura.getInformacionAdicional().getImprimir();
        String idProyecto = visitas.ordenLectura.getIdProyecto();
        String	ciclo	=	visitas.ordenLectura.getCiclo();

        ContentValues registro = new ContentValues();
        registro.put("porcion", porcion);
        registro.put("cecgsecu", cecgsecu);
        registro.put("periodo", periodo);
        registro.put("ciclo", ciclo);
        registro.put("ordenLectura", ordenLectura);
        registro.put("cuentaContrato", cuentaContrato);
        registro.put("ruta", ruta);
        registro.put("sector", sector);
        registro.put("mes", mes);
        registro.put("secuencia", secuencia);
        registro.put("piso", piso);
        registro.put("manzana", manzana);
        registro.put("departamento", departamento);
        registro.put("direccion", direccion);
        registro.put("direccionTrans", direccionTrans);
        registro.put("direccionRef", direccionRef);
        registro.put("mensajeLector", mensajeLector);
        registro.put("nombreSector", nombreSector);
        registro.put("placaPredial", placaPredial);
        registro.put("actividadLectura", actividadLectura);
        registro.put("numeroDeInstalacion", numeroDeInstalacion);
        registro.put("idEstadoInstalacion", idEstadoInstalacion);
        registro.put("idEstadoMedidor", idEstadoMedidor);
        registro.put("numeroMedidor", numeroMedidor);
        registro.put("fabricante", fabricante);
        registro.put("modelo", modelo);
        registro.put("fechaLecturaAnterior", fechaLecturaAnterior);
        registro.put("lecturaAnterior", lecturaAnterior);
        registro.put("lecturaMin", lecturaMin);
        registro.put("lecturaMax", lecturaMax);
        registro.put("idCodigoUbicacion", idCodigoUbicacion);
        registro.put("tipoInstalacion", tipoInstalacion);
        registro.put("consumoPromedio", consumoPromedio);
        registro.put("metodoConsumoPrevio", metodoConsumoPrevio);
        registro.put("nombreCliente", nombreCliente);
        registro.put("numeroTelefono", numeroTelefono);
        registro.put("imprimir", imprimir);
        registro.put("trasmitido", "N");
        registro.put("leido", "N");
        registro.put("processed", wk.getProcessedvisits().path);
        registro.put("close", wk.getClosedvisits().path);
        registro.put("pathin", inp);
        registro.put("pathout", outp);
        registro.put("continius", wk.getProcessedvisits().path);
        registro.put("idProyecto",idProyecto);
        //registro.put("workblock", "");

        bd.insert("recin", null, registro);
        System.out.println("execute insert Recin");







    }
    public void insertarWORK(Work wk)
    {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,"administracion", null, 1);


        SQLiteDatabase bd = admin.getWritableDatabase();

        String	name	= wk.getName();
        String	location	=	wk.getPendingworks().path;
        String	processing	=	wk.getProcessedvisits().path;

        String	finished	=	wk.getClosedvisits().path;
        String	postResult	=	wk.getContinuousCommunication().path;
        String	filePath	=	wk.getFilePath().path;



        ContentValues registro = new ContentValues();
        registro.put("name", name);
        registro.put("location", location);
        registro.put("processing", processing);

        registro.put("finished", finished);
        registro.put("filePath", filePath);


        bd.insert("works", null, registro);
        System.out.println("execute insert works");



        bd.close();



    }
    public ArrayList<ResumenRuta> resumen_sql()
    {
        ArrayList<ResumenRuta> resultado = new ArrayList<ResumenRuta>();


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";

        qr = "select actividadLectura, porcion,sector,ruta,count(), sum(case leido when 'S' then 1 else 0 end ) from recin  group by actividadLectura,porcion,sector,ruta";

        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            ResumenRuta vt= new ResumenRuta();
            String t = fila.getString(0);
            if(t.equals("1"))
            {
                vt.tipo = "LECTURA";
            }
            else if(t.equals("2"))
            {
                vt.tipo = "ENTREGA";
            }
            else if(t.equals("3"))
            {
                vt.tipo = "INDUSTRIALES";
            }
            else
            {
                vt.tipo = "INSPECCION";
            }

            vt.porcion = fila.getString(1);
            vt.sector = fila.getString(2);
            vt.ruta = fila.getString(3);
            double result = 0;
            double totall = 0;
            double realizado = 0;
            try
            {
                totall = Double.valueOf(fila.getString(4)) ;
                realizado = Double.valueOf(fila.getString(5)) ;
                result = (realizado/totall)*100;


            }
            catch (Exception er)
            {}


            vt.total=fila.getString(4)+"/"+fila.getString(5) + "  " + String.format("%.2f", result)  + "%" ;
            resultado.add(vt);

        }
        bd.close();
        return resultado;





    }
    public visitas cuentaR_sql(String previa)
    {

        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;
        if(previa == "")
        {
            qr = "select * from recin where trasmitido = 'N' order by sector, ruta , secuencia, cuentaContrato  LIMIT 1,0  ";
            encontrado = true;
        }
        else
        {
            qr = "select * from recin where trasmitido = 'N' order by sector, ruta , secuencia, cuentaContrato";

        }
        Cursor fila = bd.rawQuery(
                "select * from recin ", null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            if(encontrado)
            {
                return vt;
            }
            if(vt.ordenLectura.ordenLectura.toString().equals(previa))
            {

                encontrado = true;
            }
        }
        bd.close();
        return vt;





    }
    public visitas cuentaR_sql(ResumenRuta rm ,String  previa)
    {
        String tipo = "";
        if(rm.tipo.equals("LECTURA"))
        {
            tipo = "1";
        }
        if(rm.tipo.equals("ENTREGA"))
        {
            tipo = "2";
        }
        if(rm.tipo.equals("INDUSTRIALES"))
        {
            tipo = "3";
        }
        if(rm.tipo.equals("INSPECCION"))
        {
            tipo = "10114";
        }
        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;
        if(previa == "")
        {
            qr = "select * from recin where leido = 'N' and sector = '"+rm.getSector()+"' and ruta = '"+rm.getRuta()+"' and  actividadLectura = '"+tipo+"'  order by sector, ruta , secuencia, cuentaContrato ";
            encontrado = true;
        }
        else
        {
            qr = "select * from recin where leido = 'N' and sector = '"+rm.getSector()+"' and ruta = '"+rm.getRuta()+"' and  actividadLectura = '"+tipo+"'  order by sector, ruta , secuencia, cuentaContrato    ";

        }
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            if(encontrado)
            {
                return vt;
            }
            if(vt.ordenLectura.ordenLectura.toString().equals(previa))
            {

                encontrado = true;
            }
        }
        bd.close();
        return vt;





    }
    public visitas cuentaL_sql(String previa)
    {

        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;
        Integer a = 0;
        if(previa == "")
        {
            qr = "select * from recin where trasmitido = 'N' order by sector, ruta , secuencia, cuentaContrato  LIMIT 1,0  ";
            encontrado = true;
        }
        else
        {
            qr = "select * from recin where trasmitido = 'N' order by sector, ruta , secuencia, cuentaContrato";

        }
        Cursor fila = bd.rawQuery(
                "select * from recin ", null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            if(encontrado)
            {

            }
            if(vt.ordenLectura.ordenLectura.toString().equals(previa))
            {
                if(a == 0)
                {}
                else {
                    return vtl;
                }
            }
            vtl = vt;
            a +=1;
        }
        bd.close();
        return vt;





    }
    public visitas cuentaL_sql(ResumenRuta rm, String previa)
    {
        String tipo = "";
        if(rm.tipo.equals("LECTURA"))
        {
            tipo = "1";
        }
        if(rm.tipo.equals("ENTREGA"))//INDUSTRIALES
        {
            tipo = "2";
        }
        if(rm.tipo.equals("INSPECCION"))
        {
            tipo = "10114";
        }
        if(rm.tipo.equals("INDUSTRIALES"))
        {
            tipo = "3";
        }
        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;
        Integer a = 0;
        if(previa == "")
        {
            qr = "select * from recin where leido = 'N' and sector = '"+rm.getSector()+"' and ruta = '"+rm.getRuta()+"' and  actividadLectura = '"+tipo+"'  order by sector, ruta , secuencia, cuentaContrato  ";
            encontrado = true;
        }
        else
        {
            qr = "select * from recin where leido = 'N' and sector = '"+rm.getSector()+"' and ruta = '"+rm.getRuta()+"' and  actividadLectura = '"+tipo+"'  order by sector, ruta , secuencia, cuentaContrato  ";

        }
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            if(encontrado)
            {

            }
            if(vt.ordenLectura.ordenLectura.toString().equals(previa))
            {
                if(a == 0)
                {}
                else {
                    return vtl;
                }
            }
            vtl = vt;
            a +=1;
        }
        bd.close();
        return vt;





    }

    public void marcarLeido(String orden)
    {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,"administracion", null, 1);


        SQLiteDatabase bd = admin.getWritableDatabase();
        bd.execSQL("update recin set leido = 'S' where ordenLectura = '"+orden+"';");

        System.out.println("execute update "+orden);



        bd.close();



    }

    public void marcarTrans(String orden)
    {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,"administracion", null, 1);


        SQLiteDatabase bd = admin.getWritableDatabase();
        bd.execSQL("update recin set trasmitido = 'S' where ordenLectura = '"+orden+"';");

        System.out.println("execute update "+orden);



        bd.close();



    }

    public ArrayList<resumenRecin> resumen_sqlRIN()
    {
        ArrayList<resumenRecin> resultado = new ArrayList<resumenRecin>();


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";

        qr = "select ordenLectura,pathout,processed from recin where trasmitido = 'N' and leido = 'S' ";

        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            resumenRecin vt= new resumenRecin();

            vt.setOrden(fila.getString(0));
            vt.setPath(fila.getString(1));
            vt.setUri(fila.getString(2));
            resultado.add(vt);

        }
        fila.close();
        bd.close();
        return resultado;





    }

    public visitas cuenta_sql(String previa)
    {

        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;
        if(previa == "")
        {
            vt= new visitas();
            encontrado = true;
            return  vt;
        }
        else
        {
            qr = "select * from recin where cuentaContrato = '"+previa+"'";

        }
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
                if(vt.ordenLectura.cuentaContrato.equals(previa))
                {

                   vtl = vt;
                }


                encontrado = true;
        }
        fila.close();
        bd.close();
        return vtl;





    }
    public boolean cuentaLeido(String previa)
    {

        visitas vt = new visitas();
        visitas vtl = new visitas();
        visitas vtr = new visitas();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

            qr = "select * from recin where cuentaContrato = '"+previa+"' and leido = 'S'";


        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {
            vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            if(vt.ordenLectura.cuentaContrato.equals(previa))
            {
                encontrado = true;

                vtl = vt;
            }



        }
        fila.close();
        bd.close();
        return encontrado;





    }


}
