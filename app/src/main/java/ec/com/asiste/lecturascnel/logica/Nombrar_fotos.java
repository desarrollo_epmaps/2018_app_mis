package ec.com.asiste.lecturascnel.logica;

import static ec.com.asiste.lecturascnel.logica.util.rellenarCerosIzquierda;

public class Nombrar_fotos {

    public String foto_cnel(String cuenta, String fecha, String periodo) {

        return periodo+"_"+cuenta+"_"+fecha+".jpg";

    }
    public String foto_cnel(String cuenta,String contrato, String proyecto ,  String fecha,String hora, String periodo, String ciclo) {

        return proyecto +"_"+ rellenarCerosIzquierda(ciclo,2)+"_"+
        periodo+"_"+rellenarCerosIzquierda(contrato, 20)+"_"+rellenarCerosIzquierda(cuenta, 15)+"_"+fecha+"_"+hora+".jpg";

    }
    public String video_cnel(String cuenta, String fecha, String periodo) {

        return periodo+"_"+cuenta+"_"+fecha+".mp4";

    }
    public String video_cnel(String cuenta,String contrato, String proyecto ,  String fecha,String hora, String periodo, String ciclo) {

        return proyecto +"_"+
                periodo+"_"+rellenarCerosIzquierda(contrato, 20)+
                rellenarCerosIzquierda(ciclo,2)+"_"+
                "_"+rellenarCerosIzquierda(cuenta, 15)+"_"+fecha+"_"+hora+".mp4";

    }
}
