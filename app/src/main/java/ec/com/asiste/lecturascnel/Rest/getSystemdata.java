package ec.com.asiste.lecturascnel.Rest;


import android.os.AsyncTask;
import android.util.Log;

import com.asistecom.worker.web.entity.resources.Resource;
import com.asistecom.worker.web.entity.resources.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;

public class getSystemdata extends AsyncTask<Resources , Void , String> {



    @Override
    protected String doInBackground(Resources... resources) {
        for ( Resource r: resources[0].getResources()
             ) {
            URL url = null;

            try {
                url = new URL(Parametros.ipInterna+ ":"+Parametros.puertoInterna+Parametros.servicioConfig+"/systemdata/"+Parametros.idProyecto+"/"+r.getName().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            String server_response = "";
            HttpURLConnection urlConnection = null;

            try {
                //url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){


                    //salvar leer usuarios
                    fileManager.saveReadStream(urlConnection.getInputStream(),r.getName(), Parametros.dir_archivos.toString());
                    server_response = fileManager.readStream(urlConnection.getInputStream());
                    Log.v("CatalogClient", server_response);
                }
                else
                {
                    BufferedReader reader = null;
                    try {
                        reader = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream())

                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String line = null;

                    StringBuilder sb = new StringBuilder();
                    try {
                        line = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    while (line != null) {
                        sb.append(line);
                        try {
                            line = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    urlConnection.disconnect();

                    return sb.toString();

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        try
        {
            sqLite sqLite = new sqLite();
            sqLite.insertarUsuarios(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/Lectores.txt"));
            sqLite.insertarGlobal(fileManager.ReadFile(Parametros.contexto, Parametros.dir_archivos.toString(), "/global.txt"));
        }
        catch(Exception ex)
        {}



        return  "";

    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }


    public void execute(Resources finalR, String s, String s1) {
        doInBackground(finalR);
    }
}
