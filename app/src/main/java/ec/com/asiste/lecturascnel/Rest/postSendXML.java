package ec.com.asiste.lecturascnel.Rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import ec.com.asiste.lecturascnel.SendActivity;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;

public class postSendXML extends AsyncTask<String , Void , String> {

    public postSendXML(String[] s) {
        doInBackground(s);
    }
    String mensaje ="";
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        try {
            SendActivity.logg.setText(mensaje);
        }
        catch (Exception ex)
        {}
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = null;
            try {
                url = new URL(Parametros.ipInterna + ":" + Parametros.puertoInterna + Parametros.servicio + strings[0]);
                System.out.println("................................" + strings[0]);
                //url = new URL("http://192.168.1.152:8080/wis/servicio/registerDevice"+strings[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) url.openConnection();
                System.out.println("2..............................." + strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("errrrrr................................" + strings[0]);
            }
            try {
                connection.setRequestMethod("POST");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            String authString = "terminalLectura" + ":" + "password";
            byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
            String authStringEnc = new String(authEncBytes);
            connection.addRequestProperty("Authorization", "Basic " + authStringEnc);
            connection.setRequestProperty("content-type", "application/xml");
            SharedPreferences preferencias = Parametros.getContexto().getSharedPreferences("datos", Context.MODE_PRIVATE);
            String d = preferencias.getString("token", "");
            connection.setRequestProperty("User-Agent", d);
            connection.setDoOutput(true);
            connection.setConnectTimeout(30000);
            OutputStreamWriter wr = null;
            try {
                wr = new OutputStreamWriter(connection.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                wr.write(strings[1]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                wr.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int resp = 0;
            try {
                resp = connection.getResponseCode();
                mensaje = "ENVIANDO " + resp;
                publishProgress(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (resp == 200) {//obtengo factura
                //throw new BetterLuckNextTimeException(...);
                System.out.println("okkkkk................................" + strings[0]);

                sqLite db = new sqLite();
                db.marcarTrans(strings[2]);
                //mover file
                fileManager.moveFile(strings[3] + "/RECOUT", strings[4], strings[3] + "/TRANSMITIDO", strings[4]);


            }
            if (resp == 202) { //ok sin factura
                //throw new BetterLuckNextTimeException(...);
                System.out.println("okkkkk................................" + strings[0]);

                sqLite db = new sqLite();
                db.marcarTrans(strings[2]);
                //mover file
                fileManager.moveFile(strings[3] + "/RECOUT", strings[4], strings[3] + "/TRANSMITIDO", strings[4]);


            } else {
                //throw new BetterLuckNextTimeException(...);

                return "";
            }

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())

                );
            } catch (IOException e) {
                e.printStackTrace();
            }

            String line = null;

            StringBuilder sb = new StringBuilder();
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (line != null) {
                sb.append(line);
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
            return "";
        }
        catch (Exception exx)
        {
            return "";
        }
    }
}
