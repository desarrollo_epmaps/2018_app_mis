package ec.com.asiste.lecturascnel;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.vistiasIn.OrdenLectura;
import com.asistecom.worker.web.entity.vistiasIn.informacionAdicional;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import java.util.ArrayList;

import ec.com.asiste.lecturascnel.dblite.AdminSQLiteOpenHelper;
import ec.com.asiste.lecturascnel.logica.Parametros;

public class BuscarLActivity extends AppCompatActivity {

    visitas svisita = new visitas();
    TextView tv ;
    ListView lv ;
    RadioButton rb_sr, rb_srf, rb_cuenta, rb_secuencia, rb_global;
    EditText et_parametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_l);
        lv = (ListView)findViewById(R.id.lv_busqueda);
        rb_cuenta = (RadioButton)findViewById(R.id.rb_b_cuenta);
        rb_secuencia = (RadioButton)findViewById(R.id.rb_b_secuencia);
        rb_sr = (RadioButton)findViewById(R.id.rb_b_sr);

        rb_global = (RadioButton)findViewById(R.id.rb_b_global);
        et_parametro = (EditText) findViewById(R.id.et_b_parametro);
        tv = (TextView) findViewById(R.id.tv_busqueda_select);

    }
    public void buscar()
    {
        if(rb_sr.isChecked())
        {
            buscarSR();
        }
        else if(rb_secuencia.isChecked())
        {
            buscarSecuencia();
        }
        else if(rb_cuenta.isChecked())
        {
            buscarCuenta();
        }
        else if(rb_global.isChecked())
        {
            buscarGlobal();
        }


    }
    public void accionS(View view)
    {

        if(!rb_global.isChecked())
        {
            Parametros.vt_busqueda = svisita;
            finish();
        }else
            Parametros.vt_busqueda_S = svisita;
            finish();
        }

    public void accionB(View view)
    {
        buscar();
        Toast.makeText(getApplicationContext(),"ENCONTRADO: "+lv.getAdapter().getCount(),Toast.LENGTH_LONG).show();
    }
    public void accionSob(View view)
    {
        Intent i = new Intent(this, SobranteActivity.class );
        startActivity(i);
    }

    public void buscarSRF()
    {
        ArrayList<visitas> lvisitas = new ArrayList<>();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

        qr = "select * from recin where trasmitido = 'N' and numeroDeInstalacion like  '%"+ et_parametro.getText()+"%' order by sector, ruta , secuencia, cuentaContrato";
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {


            visitas  vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            lvisitas.add(vt);
            //}


        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, lvisitas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final visitas item = (visitas) parent.getItemAtPosition(position);
                svisita = item;
                tv.setText(svisita.toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });

        bd.close();

    }

    public void buscarSR()
    {
        ArrayList<visitas> lvisitas = new ArrayList<>();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

        qr = "select * from recin where trasmitido = 'N' and numeroMedidor like  '%"+ et_parametro.getText()+"%' order by sector, ruta , secuencia, cuentaContrato";
        //qr = "select * from recin where trasmitido = 'N' and like('%"+ et_parametro.getText()+"%',cuentaContrato)"; //like('%neon%',name)
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {


            visitas  vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            lvisitas.add(vt);
            //}

        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, lvisitas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final visitas item = (visitas) parent.getItemAtPosition(position);
                svisita = item;
                tv.setText(svisita.toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });
        bd.close();
    }
    public void buscarSecuencia()
    {
        ArrayList<visitas> lvisitas = new ArrayList<>();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

        qr = "select * from recin where trasmitido = 'N' and secuencia =  '"+ et_parametro.getText()+"' order by sector, ruta , secuencia, cuentaContrato";
        //qr = "select * from recin where trasmitido = 'N' and like('%"+ et_parametro.getText()+"%',cuentaContrato)"; //like('%neon%',name)
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {


            visitas  vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            lvisitas.add(vt);
            //}

        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, lvisitas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final visitas item = (visitas) parent.getItemAtPosition(position);
                svisita = item;
                tv.setText(svisita.toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });
        bd.close();

    }
    public void buscarCuenta()
    {
        ArrayList<visitas> lvisitas = new ArrayList<>();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

        qr = "select * from recin where trasmitido = 'N' and cuentaContrato like  '%"+ et_parametro.getText()+"%' order by sector, ruta , secuencia, cuentaContrato";
        //qr = "select * from recin where trasmitido = 'N' and like('%"+ et_parametro.getText()+"%',cuentaContrato)"; //like('%neon%',name)
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {


            visitas  vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            lvisitas.add(vt);
            //}


        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, lvisitas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final visitas item = (visitas) parent.getItemAtPosition(position);
                svisita = item;
                tv.setText(svisita.toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });
        bd.close();
    }
    public void buscarGlobal()
    {
        ArrayList<visitas> lvisitas = new ArrayList<>();
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String qr = "";
        boolean encontrado = false;

       // qr = "select * from recin where trasmitido = 'N' and cuentaContrato like  '%"+ et_parametro.getText()+"%' order by sector, ruta , secuencia, cuentaContrato";
        qr = "select * from global  where sr = '"+ et_parametro.getText()+"' or srf = '"+ et_parametro.getText()+"'";
        Cursor fila = bd.rawQuery(
                qr, null);
        while (fila.moveToNext()) {


            visitas  vt= new visitas();
            vt.ordenLectura= new OrdenLectura();
            vt.ordenLectura.porcion = fila.getString(0);
            vt.ordenLectura.cecgsecu = fila.getString(1);
            vt.ordenLectura.periodo = fila.getString(2);
            vt.ordenLectura.ordenLectura = fila.getString(3);
            vt.ordenLectura.cuentaContrato = fila.getString(4);
            ////add ciclo 2019-01-19
            vt.ordenLectura.ciclo = fila.getString(5);

            vt.ordenLectura.ruta = fila.getString(6);
            vt.ordenLectura.sector = fila.getString(7);
            vt.ordenLectura.mes = fila.getString(8);
            vt.ordenLectura.secuencia = fila.getString(9);
            vt.ordenLectura.piso = fila.getString(10);
            vt.ordenLectura.manzana = fila.getString(11);
            vt.ordenLectura.departamento = fila.getString(12);
            vt.ordenLectura.direccion = fila.getString(13);
            vt.ordenLectura.direccionTrans = fila.getString(14);
            vt.ordenLectura.direccionRef = fila.getString(15);
            vt.ordenLectura.mensajeLector = fila.getString(16);

            vt.ordenLectura.nombreSector = fila.getString(17);
            vt.ordenLectura.placaPredial = fila.getString(18);
            vt.ordenLectura.actividadLectura = fila.getString(19);
            vt.ordenLectura.numeroDeInstalacion = fila.getString(20);
            vt.ordenLectura.idEstadoInstalacion = fila.getString(21);
            vt.ordenLectura.idEstadoMedidor = fila.getString(22);
            vt.ordenLectura.numeroMedidor = fila.getString(23);
            vt.ordenLectura.fabricante = fila.getString(24);
            vt.ordenLectura.modelo = fila.getString(25);
            vt.ordenLectura.fechaLecturaAnterior = fila.getString(26);
            vt.ordenLectura.lecturaAnterior = fila.getString(27);
            vt.ordenLectura.lecturaMin = fila.getString(28);
            vt.ordenLectura.lecturaMax = fila.getString(29);
            vt.ordenLectura.idCodigoUbicacion = fila.getString(30);
            vt.ordenLectura.tipoInstalacion = fila.getString(31);
            vt.ordenLectura.consumoPromedio = fila.getString(32);
            vt.ordenLectura.metodoConsumoPrevio = fila.getString(33);
            vt.ordenLectura.informacionAdicional = new informacionAdicional();
            vt.ordenLectura.informacionAdicional.setNombreCliente( fila.getString(34));
            vt.ordenLectura.informacionAdicional.setNumeroTelefono(fila.getString(35));
            vt.ordenLectura.informacionAdicional.setImprimir(fila.getString(36));
            vt.ordenLectura.setLeido(fila.getString(39));
            vt.ordenLectura.setProcessed(fila.getString(40));
            vt.ordenLectura.setClose(fila.getString(41));
            vt.ordenLectura.setContinius(fila.getString(42));
            vt.ordenLectura.setPathin(fila.getString(43));
            vt.ordenLectura.setPathout(fila.getString(44));
            vt.ordenLectura.setIdProyecto(fila.getString(45));
            lvisitas.add(vt);
            //}


        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, lvisitas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final visitas item = (visitas) parent.getItemAtPosition(position);
                svisita = item;
                tv.setText(svisita.toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });
        bd.close();
    }
}
