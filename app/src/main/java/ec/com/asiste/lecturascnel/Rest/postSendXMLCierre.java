package ec.com.asiste.lecturascnel.Rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.visitasOut.visitas;

import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

import ec.com.asiste.lecturascnel.CIerreActivity;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.fileResumenRuta;

public class postSendXMLCierre extends AsyncTask<String , Void , String> {
    fileResumenRuta ruta;
    String orden = "";
    String respt = "";
    public postSendXMLCierre(fileResumenRuta s) {
        ruta = s;
        doInBackground("");
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        /// validar cero archivos en ruta
        CIerreActivity.tv_log.setText("TERMINADO");
        if(fileManager.cantidadRuta(ruta.getRuta()) == 0)
        {
            CIerreActivity.tv_log.setText("TERMINADO CON ERRORES");
            sqLite  sql = new sqLite();
            String[] partes = ruta.getRuta().split("-");
            sql.eliminarRuta(partes[1],partes[2],partes[3].substring(0,partes[3].length()-1));
            //eliminar carpeta
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        CIerreActivity.tv_log.setText(orden + "/"+respt);
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url = null;
        String xmll = "";
        visitas rv = null;

        for (String ss : ruta.getArchivos()
             ) {
            try {
                xmll = fileManager.ReadFile(Parametros.getContexto(),

                        FilenameUtils.getPath(ss),
                        ""+FilenameUtils.getName(ss));
                Serializer serializer = new Persister();

                //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");

                try {
                    //Resources r = serializer.read(Resources.class, source);
                    rv = serializer.read(visitas.class, xmll);
                }
                catch (Exception exx)
                {

                }
                url = new URL(Parametros.ipInterna+ ":"+Parametros.puertoInterna+Parametros.servicio+rv.OrdenLectura.getClosedvisits());

                //url = new URL("http://192.168.1.152:8080/wis/servicio/registerDevice"+strings[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) url.openConnection();
                System.out.println("2..............................."+strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("errrrrr................................"+strings[0]);
            }
            try {
                connection.setRequestMethod("POST");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            String authString = "terminalLectura" + ":" + "password";
            byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
            String authStringEnc = new String(authEncBytes);
            connection.addRequestProperty("Authorization", "Basic "+ authStringEnc);
            connection.setRequestProperty("content-type", "application/xml");
            SharedPreferences preferencias = Parametros.getContexto().getSharedPreferences("datos", Context.MODE_PRIVATE);
            String d=preferencias.getString("token", "");
            connection.setRequestProperty("User-Agent", d);
            connection.setDoOutput(true);
            connection.setConnectTimeout(30000);
            OutputStreamWriter wr = null;
            try {
                wr = new OutputStreamWriter(connection.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                wr.write(xmll);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                wr.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int resp = 0;
            try {
                resp = connection.getResponseCode();
                orden = rv.OrdenLectura.getOrdenLectura().toString();
                respt = String.valueOf(resp);
                publishProgress(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (resp == 200) {//obtengo factura
                //throw new BetterLuckNextTimeException(...);
                System.out.println("okkkkk................................"+strings[0]);

                sqLite db = new sqLite();
                db.marcarTrans(strings[2]);
                //mover file
                fileManager.deleteRecursive(new File(strings[0]));


            }
            if ( resp == 202) { //ok sin factura
                //throw new BetterLuckNextTimeException(...);
                System.out.println("okkkkk................................"+strings[0]);


                //mover file
                fileManager.deleteRecursive(new File(ss));


            }

            else  {
                //throw new BetterLuckNextTimeException(...);

            }
            connection.disconnect();

        }
        orden = "TERMINADO";
        respt = "";
        publishProgress(null);
        if(fileManager.cantidadRuta(ruta.getRuta()) == 0)
        {

            sqLite  sql = new sqLite();
            String[] partes = ruta.getRuta().split("-");
            sql.eliminarRuta(partes[0],partes[1],partes[2]);
            //eliminar carpeta
            fileManager.deleteRecursive(new File(Parametros.getDir_files().toString()+"/"+ruta.getRuta()));
        }
        return "";
    }
}

