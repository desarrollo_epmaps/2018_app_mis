package ec.com.asiste.lecturascnel.logica;

import android.app.Notification;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.vistiasIn.visitas;
import com.asistecom.worker.web.entity.workblock.Work;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class fileManager {


    public static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
    public static void copyFileUsingStream(String source, String dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
    public static  String ReadFile( Context context, String path , String fileName){
        String line = null;

        try {
            FileInputStream fileInputStream = new FileInputStream (new File(path, fileName));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ( (line = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(line);
            }
            fileInputStream.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            int a = 0;
        }
        catch(IOException ex) {

        }
        return line;
    }
    public static  String ReadFile( Context context, String pathfileName){
        String line = null;

        try {
            FileInputStream fileInputStream = new FileInputStream (new File(pathfileName));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ( (line = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(line);
            }
            fileInputStream.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {

        }
        catch(IOException ex) {

        }
        return line;
    }
    public static  boolean  deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
        return true;
    }
    public static String readFile(String pathh)
    {
        String resultado = "";



        return  resultado;
    }
    public static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public static Integer contarFotos()
    {
        try {
            File directorio = new File(Parametros.dir_fotos.toString());
            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".jpg")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            String[] arrArchivos = directorio.list(textFilter);
            int total  = arrArchivos.length;
            return total;
        }
        catch (Exception e)
        {
            return 0;
        }


    }
    public static Integer contarFotosC()
    {
        try {
            File directorio = new File(Parametros.dir_fotosC.toString());
            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".jpg")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            String[] arrArchivos = directorio.list(textFilter);
            int total  = arrArchivos.length;
            return total;
        }
        catch (Exception e)
        {
            return 0;
        }


    }

    public static String[]  listaSobranter()
    {
        List<String> resultado = new ArrayList<String>();
        try {
            File directorio = new File(Parametros.dir_files.toString());
            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.contains("sob_")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            String[] arrArchivos = directorio.list(textFilter);

            return arrArchivos;
        }
        catch (Exception e)
        {
            return null;
        }


    }
    public static List<String>  listaSobrante()
    {
        List<String>  resultado = new ArrayList<>();
        try {

            ///listar carpetas
            File[] directories = new File(Parametros.getDir_files().toString()).listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            });
            for (File fdr: directories
                 ) {

                File directorio = new File(fdr.toString()+"/RECOUT");
                FilenameFilter textFilter = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        String lowercaseName = name.toLowerCase();
                        if (lowercaseName.contains("sob_")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                String[] arrArchivos = directorio.list(textFilter);
                for (String ss:arrArchivos
                     ) {
                    resultado.add(fdr.toString()+"/RECOUT"+"/" + ss);
                }
            }



            return resultado;
        }
        catch (Exception e)
        {
            return null;
        }


    }

    //



    public static Integer contarVideos()
    {
        try {
            File directorio = new File(Parametros.dir_fotos.toString());
            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".mp4")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            String[] arrArchivos = directorio.list(textFilter);
            int total  = arrArchivos.length;
            return total;
        }
        catch (Exception e)
        {
            return 0;
        }


    }
    public static Integer contarVideosC()
    {
        try {
            File directorio = new File(Parametros.dir_fotosC.toString());
            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".mp4")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            String[] arrArchivos = directorio.list(textFilter);
            int total  = arrArchivos.length;
            return total;
        }
        catch (Exception e)
        {
            return 0;
        }


    }
    public static void moveFile(String inputPath, String inputFile, String outputPath, String outputFile) {
        System.out.println("mover "+inputFile);
        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + outputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();
            System.out.println("mover ok"+inputFile);

        }

        catch (FileNotFoundException fnfe1) {
            System.out.println("mover error"+inputFile +fnfe1.getMessage());
        }
        catch (Exception e) {
            System.out.println("mover error"+inputFile+e.getMessage());
        }

    }

    public static ArrayList<String> ListaFotosC()
    {
        File directorio = new File(Parametros.dir_fotosC.toString());
        String[] arrArchivos = directorio.list();
        ArrayList<String> files = new ArrayList<>();
        for(int i=0; i<arrArchivos.length; ++i){
            if(arrArchivos[i].contains(".jpg") || arrArchivos[i].contains(".mp4")) {
                files.add(arrArchivos[i]);
            }
        }

        return  files;
    }
    public static ArrayList<String> ListaFotosV()
    {
        File directorio = new File(Parametros.dir_fotos.toString());
        String[] arrArchivos = directorio.list();
        ArrayList<String> files = new ArrayList<>();
        for(int i=0; i<arrArchivos.length; ++i){
            if(arrArchivos[i].contains(".mp4")) {
                files.add(arrArchivos[i]);
            }
        }

        return  files;
    }
    public static ArrayList<String> ListaFotos()
    {
        File directorio = new File(Parametros.dir_fotos.toString());
        String[] arrArchivos = directorio.list();
        ArrayList<String> files = new ArrayList<>();
        for(int i=0; i<arrArchivos.length; ++i){
            if(arrArchivos[i].contains(".jpg") || arrArchivos[i].contains(".mp4") ) {
                files.add(arrArchivos[i]);
            }
        }

        return  files;
    }
    public static void saveReadStream(InputStream in, String filename, String dir) throws IOException {


        try {

            File file = new File(dir, filename);
            OutputStream output = new FileOutputStream(file);
            try {
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = in.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }
            }
            catch (Exception eee)
            {

            }



        } finally {
            in.close();
        }


    }



    public static List<fileResumenRuta>  listaRutas()
    {
        List<fileResumenRuta>  resultado = new ArrayList<>();
        try {

            ///listar carpetas
            File[] directories = new File(Parametros.getDir_files().toString()).listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            });
            for (File fdr: directories
            ) {
                fileResumenRuta frr = new fileResumenRuta();
                List<String> files = new ArrayList<>();
                frr.setRuta(fdr.getName());
                int total = 0;
                File directorio = new File(fdr.toString()+"/RECOUT");
                FilenameFilter textFilter = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        String lowercaseName = name.toLowerCase();
                        if (lowercaseName.contains(".xml")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                String[] arrArchivos = directorio.list(textFilter);
                for (String ss:arrArchivos
                ) {
                     total += 1;
                     files.add(fdr.toString()+"/RECOUT"+"/" + ss);


                }
                directorio = new File(fdr.toString()+"/TRANSMITIDO");
                FilenameFilter textFilter2 = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        String lowercaseName = name.toLowerCase();
                        if (lowercaseName.contains(".xml")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                String[] arrArchivos2 = directorio.list(textFilter2);
                for (String ss:arrArchivos2
                ) {
                    total += 1;
                    files.add(fdr.toString()+"/TRANSMITIDO"+"/" + ss);


                }
                frr.setArchivos(files);
                frr.setCantidad(String.valueOf(total));
                resultado.add(frr);
            }




        }
        catch (Exception e)
        {
            return null;
        }

        return resultado;
    }

    public static int cantidadRuta(String rutaNombre)
    {
       int resultado = 0;
        try {

            ///listar carpetas

                File directorio = new File(Parametros.getDir_files()+"/"+rutaNombre +"/RECOUT");
                FilenameFilter textFilter = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        String lowercaseName = name.toLowerCase();
                        if (lowercaseName.contains(".xml")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                String[] arrArchivos = directorio.list(textFilter);
                for (String ss:arrArchivos
                ) {
                    resultado += 1;



                }
                directorio = new File(Parametros.getDir_files()+"/"+rutaNombre+"/TRANSMITIDO");
                FilenameFilter textFilter2 = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        String lowercaseName = name.toLowerCase();
                        if (lowercaseName.contains(".xml")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                String[] arrArchivos2 = directorio.list(textFilter2);
                for (String ss:arrArchivos2
                ) {
                    resultado += 1;



                }






        }
        catch (Exception e)
        {
            return 9999;
        }

        return resultado;
    }
}
