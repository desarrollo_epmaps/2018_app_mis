package ec.com.asiste.lecturascnel;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.vistiasIn.visitas;

import java.io.File;
import java.util.List;

import ec.com.asiste.lecturascnel.Rest.postRegisterDevice;
import ec.com.asiste.lecturascnel.Rest.postSendXMLCierre;
import ec.com.asiste.lecturascnel.Rest.postSystemdata;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.fileResumenRuta;
import ec.com.asiste.lecturascnel.logica.util;

public class CIerreActivity extends AppCompatActivity {


    ListView vl;
    public static TextView tv_log;
    public TextView tv_ruta;
    EditText et_pwd;
    fileResumenRuta rutaSeleccionado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cierre);
        vl = (ListView)findViewById(R.id.lv_cierre_listado);
        tv_log = (TextView)findViewById(R.id.tv_cierre_log);
        tv_ruta = (TextView)findViewById(R.id.tv_cierre_ruta_select);
        et_pwd = (EditText)findViewById(R.id.et_cierre_pwd);
        cargarrutas();
    }
    public void accionCargar(View view)
    {
        cargarrutas();
    }
    public void accionCerrar(View view)
    {
        if(et_pwd.getText().toString().equals("1234")) {

            if(tv_ruta.getText().length() > 4)
            {
                cerrar();
            }
            else
            {
                Toast.makeText(this, "Selecione la ruta", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {

        }

    }
    public void cerrar()
    {
        if(rutaSeleccionado.getCantidad().equals("0"))
        {
            //borrar ruta DB
            String[] split = rutaSeleccionado.getRuta().split("-");
            sqLite sql = new sqLite();
            sql.eliminarRuta(split[0].trim(), split[1].trim(), split[2]);
            fileManager.deleteRecursive(new File(Parametros.getDir_files().toString()+
                    "/"+tv_ruta.getText()));


        }
        else {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //new postSystemdata("").execute("", "", "");
                        try {
                            //tv_mac.setText("ID:"+util.getMAC(getApplicationContext()));


                            new postSendXMLCierre(rutaSeleccionado);
                        }
                        catch (Exception e)
                        {
                            int a = 0;
                        }

                    }
                });
            }

    }
    public void cargarrutas()
    {
        List<fileResumenRuta> listado  = fileManager.listaRutas();
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, listado);
        vl.setAdapter(adapter);

        vl.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final fileResumenRuta item = (fileResumenRuta) parent.getItemAtPosition(position);
                rutaSeleccionado = item;
                tv_ruta.setText(rutaSeleccionado.getRuta().toString());
                view.animate().setDuration(2000).alpha(70)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //listado.remove(item);
                                //adapter.notifyDataSetChanged();
                                //view.setAlpha(1);
                            }
                        });

            }

        });


    }

}
