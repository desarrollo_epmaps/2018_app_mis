package ec.com.asiste.lecturascnel;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.asistecom.worker.web.entity.Resultado;
import com.asistecom.worker.web.entity.vistiasIn.visitas;
import com.asistecom.worker.web.entity.workblock.Work;
import com.asistecom.worker.web.entity.workblock.Works;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import ec.com.asiste.lecturascnel.Rest.pathRuta;
import ec.com.asiste.lecturascnel.dblite.AdminSQLiteOpenHelper;
import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.NotificationHelper;
import ec.com.asiste.lecturascnel.logica.util;

public class DescargarActivity extends AppCompatActivity {

    EditText tv;
    public static TextView log;
    String respuesta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descargar);
        tv = (EditText) findViewById(R.id.et_des_actividad );
        log = (TextView)findViewById(R.id.tv_des_log);
    }
    public void borrar()
    {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        bd.execSQL("delete from recin");
        bd.close();
    }
    public void accion(View view)
    {
        try
        {   //borrar();
            AsyncTaskRunnerPost runner = new AsyncTaskRunnerPost();
            runner.execute();}
        catch (Exception ex)
        {}
    }
    private static void decompressGzipFile(String gzipFile, String newFile) {
        try {
            FileInputStream fis = new FileInputStream(gzipFile);
            FileOutputStream fos = new FileOutputStream(newFile);
            GZIPInputStream gis = new GZIPInputStream(fis);

            byte[] buffer = new byte[2048];
            int len;
            while((len = gis.read(buffer)) != -1){
                fos.write(buffer, 0, len);
            }
            //close resources
            fos.close();
            gis.close();
        } catch (IOException e ) {
            e.printStackTrace();
        }

    }

    private static void compressGzipFile(String file, String gzipFile) {
        try {
            FileInputStream fis = new FileInputStream(file);
            FileOutputStream fos = new FileOutputStream(gzipFile);
            GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
            byte[] buffer = new byte[1024];
            int len;
            while((len=fis.read(buffer)) != -1){
                gzipOS.write(buffer, 0, len);
            }
            //close resources
            gzipOS.close();
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void unTarFile(String tarFile, String destFile) throws IOException{


        FileInputStream fis = new FileInputStream(tarFile);
        TarArchiveInputStream tis = new TarArchiveInputStream(fis);
        TarArchiveEntry tarEntry = null;

        // tarIn is a TarArchiveInputStream
        while ((tarEntry = tis.getNextTarEntry()) != null) {
            File outputFile = new File(destFile + File.separator + tarEntry.getName());

            if(tarEntry.isDirectory()){

                System.out.println("outputFile Directory ---- "
                        + outputFile.getAbsolutePath());
                if(!outputFile.exists()){
                    outputFile.mkdirs();
                }
            }else{
                //File outputFile = new File(destFile + File.separator + tarEntry.getName());
                System.out.println("outputFile File ---- " + outputFile.getAbsolutePath());
                outputFile.getParentFile().mkdirs();
                //outputFile.createNewFile();
                FileOutputStream fos = new FileOutputStream(outputFile);
                IOUtils.copy(tis, fos);
                fos.close();
            }
        }
        tis.close();
    }
    public void saveReadStream(InputStream in, String filename, final Work wk) throws IOException {


        try {

            File file = new File(Parametros.dir_files, filename+".gzip");
            OutputStream output = new FileOutputStream(file);
            try {
                //tv.setText("PROCESANDO");
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = in.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }
                //tv.setText("\n"+tv.getText()+ wk.getName()+" descargado \n Procesando....\n");
                output.flush();
                //DescargarActivity.log.setText("DECOMPRIMIR ....."+wk.getFilePath().path.toString());
                String filee = Parametros.dir_files.getPath()+"/"+filename+".gzip";
                String gzipFile = Parametros.dir_files.getPath()+"/"+filename+".gzip";
                String newFile =  Parametros.dir_files.getPath()+"/"+filename+".tar";
                File dir = new File(Parametros.dir_files.getPath()+"/"+filename);
                File dirin = new File(Parametros.dir_files.getPath()+"/"+filename+"/RECIN");
                File dirout = new File(Parametros.dir_files.getPath()+"/"+filename+"/RECOUT");
                File dirtrans = new File(Parametros.dir_files.getPath()+"/"+filename+"/TRANSMITIDO");
                dir.mkdir();
                dirin.mkdir();
                dirout.mkdir();
                dirtrans.mkdir();
                decompressGzipFile(gzipFile, newFile);
                unTarFile(Parametros.dir_files.getPath()+"/"+filename+".tar", Parametros.dir_files.getPath()+"/"+filename+"/RECIN");
                File folder = new File(Parametros.dir_files.getPath()+"/"+filename+"/RECIN");
                File[] listOfFiles = folder.listFiles();
                Serializer serializer = new Persister();
                AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Parametros.contexto,"administracion", null, 1);


                NotificationHelper noti = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    noti = new NotificationHelper(getApplicationContext());
                }
                Notification.Builder nb = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    noti.sendNotification(1100,"DESCARGA RUTA",wk.getName());
                }
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //new postSystemdata("").execute("", "", "");

                        pathRuta runner2= new pathRuta(wk);
                        runner2.execute("","","");
                    }
                });

                SQLiteDatabase bd = admin.getWritableDatabase();
                sqLite sqLite = new sqLite();
                sqLite.insertarWORK(wk);
                try {
                    //bd.execSQL("delete from recin");
                    System.out.println("execute delete recin");
                }
                catch (Exception ex)
                {}

                bd = admin.getWritableDatabase();
                for (int i = 0; i < listOfFiles.length; i++) {
                    if (listOfFiles[i].isFile()) {
                        try
                        {

                            serializer = new Persister();
                            visitas v = null;
                            v = serializer.read(visitas.class,listOfFiles[i]);
                            sqLite = new sqLite();
                            sqLite.insertarRECIN(v,bd, wk,listOfFiles[i].toString(),listOfFiles[i].toString().replace("RECIN","RECOUT"));

                        }
                        catch (Exception ex)
                        {
                            System.out.println("Error " + ex.getMessage());

                        }

                        System.out.println("File " + listOfFiles[i].getName());


                    } else if (listOfFiles[i].isDirectory()) {
                        System.out.println("Directory " + listOfFiles[i].getName());
                    }
                }
                bd.close();
                Integer a = listOfFiles.length;
                //tv.setText("\n"+tv.getText()+ wk.getName()+" EN BASE:" + a.toString()+ "\n");
                //tv.setText("\n"+tv.getText()+ " Eliminando files...\n" );
                System.out.println("Eliminando.....................................");
                fileManager fl = new fileManager();
                //fl.deleteRecursive(new File(Parametros.dir_files+"/"+wk.getName()) );


                fl.deleteRecursive(new File(Parametros.dir_files+"/"+wk.getName()+".tar") );
                fl.deleteRecursive(new File(Parametros.dir_files+"/"+wk.getName()+".gzip") );
                System.out.println("terminado .....................................");
                //tv.setText("\n"+tv.getText()+ " Terminado...\n" );
                //
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                output.close();

            }
        } finally {
            in.close();
        }


    }
    private class AsyncTaskRunnerPost extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(Parametros.ipPublica+ ":"+Parametros.puertoPublico+Parametros.servicio+"/pendingworks/"+Parametros.idProyecto+ "/"+util.rellenarCerosIzquierda( Parametros.getUsuario(),3)+"/"+Parametros.getMAC());
                HttpURLConnection connection = null;
                try {
                    connection = (HttpURLConnection) url.openConnection();
                    try {
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        String authString = "terminalLectura" + ":" + "password";
                        byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
                        String authStringEnc = new String(authEncBytes);
                        connection.addRequestProperty("Authorization", "Basic "+ authStringEnc);
                        //connection.setRequestProperty("Authorization", "Basic "+encoded);
                        connection.setRequestMethod("POST");
                        SharedPreferences preferencias = Parametros.getContexto().getSharedPreferences("datos", Context.MODE_PRIVATE);
                        String d=preferencias.getString("token", "");
                        connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                        connection.setRequestProperty("User-Agent", d);

                        //connection.setDoOutput(true);
                        connection.setConnectTimeout(300000);
                        int responseCode = connection.getResponseCode();
                        if(responseCode == HttpURLConnection.HTTP_OK){
                            BufferedReader reader = null;
                            try {
                                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                String line = null;

                                StringBuilder sb = new StringBuilder();
                                try {
                                    line = reader.readLine();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                while (line != null) {
                                    sb.append(line + "\n");
                                    try {
                                        line = reader.readLine();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        return e.getMessage();
                                    }
                                }
                                reader.close();
                                connection.disconnect();
                                return sb.toString();
                            } catch (IOException e) {
                                e.printStackTrace();
                                return e.getMessage();
                            }
                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return e.getMessage();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return e.getMessage();
            }
            return "No existe ruta";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Resultado r = null;
            Works rr = null;
            try
            {
                Serializer serializer = new Persister();

                //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");

                try {
                    //Resources r = serializer.read(Resources.class, source);
                    r = serializer.read(Resultado.class,s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    //Resources r = serializer.read(Resources.class, source);
                    rr = serializer.read(Works.class,s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            catch (Exception ex)
            {}
            if(r == null && rr == null)
            {

                tv.setText(s);
            }
            else  if(r == null && rr != null )
            {
                final Works finalRr = rr;
                String worksnames = "RUTAS ENCONTRADAS: \n";
                for (Work ss: rr.getWorks()
                        ) {
                    worksnames = worksnames +"\n"+ss.getName()+"\n";



                }
                tv.setText(worksnames);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //new postSystemdata("").execute("", "", "");

                        AsyncTaskRunnerGet runner2= new AsyncTaskRunnerGet();
                        runner2.execute(finalRr,"","");
                    }
                });

            }
            else  if(r != null && rr == null)
            {
                tv.setText(r.getMensaje());

            }

        }

    }


    private class AsyncTaskRunnerGet extends AsyncTask<Works, String, String> {
        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(Works... works) {
            URL url = null;
            try {
                url = new URL(Parametros.ipPublica+ ":"+Parametros.puertoPublico+Parametros.servicio+"pendingworks/"+Parametros.idProyecto+"/"+ util.rellenarCerosIzquierda(Parametros.idProyecto,3)+"/"+Parametros.getMAC());
                HttpURLConnection connection = null;
                try {

                    try {

                        for (Work wk : works[0].getWorks()
                                ) {
                            publishProgress("DESCARGANDO....."+wk.getName());
                            //DescargarActivity.log.setText("Descargando ....."+wk.getFilePath().path.toString());
                            url = new URL(Parametros.ipPublica+ ":"+Parametros.puertoPublico+Parametros.servicio+ wk.getPendingworks().path);
                            connection = (HttpURLConnection) url.openConnection();
                            //connection.setDoInput(true);
                            //connection.setDoOutput(true);
                            String authString = "123" + ":" + "123";
                            byte[] authEncBytes = android.util.Base64.encode(authString.getBytes(), android.util.Base64.DEFAULT);
                            String authStringEnc = new String(authEncBytes);
                            //connection.addRequestProperty("Authorization", "Basic "+ authStringEnc);
                            //connection.setRequestProperty("Authorization", "Basic "+encoded);
                            connection.setRequestMethod("GET");
                            SharedPreferences preferencias = Parametros.getContexto().getSharedPreferences("datos", Context.MODE_PRIVATE);
                            String d=preferencias.getString("token", "");
                            //connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                            //connection.setRequestProperty("User-Agent", d);

                            //connection.setDoOutput(true);
                            connection.setConnectTimeout(300000);
                            int responseCode = connection.getResponseCode();
                            if(responseCode == HttpURLConnection.HTTP_OK){


                                saveReadStream(connection.getInputStream(), wk.getName().toString(),wk);
                            }
                            publishProgress("TERMINADO");

                        }


                    } catch (ProtocolException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return e.getMessage();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return e.getMessage();
            }
            return "";
        }


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            DescargarActivity.log.setText(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Resultado r = null;
            Works rr = null;
            try
            {
                Serializer serializer = new Persister();

                //File source = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/sample.xml");

                try {
                    //Resources r = serializer.read(Resources.class, source);
                    r = serializer.read(Resultado.class,s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    //Resources r = serializer.read(Resources.class, source);
                    rr = serializer.read(Works.class,s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            catch (Exception ex)
            {}
            if(r == null && rr == null)
            {
                tv.setText(s);
            }
            else  if(r == null && rr != null )
            {
                tv.setText(r.getMensaje());

            }
            else  if(r != null && rr == null)
            {
                String worksnames = "";
                for (Work ss: rr.getWorks()
                        ) {
                    worksnames = worksnames +"\n"+ss.getName();

                }
                tv.setText(worksnames);

            }
            DescargarActivity.log.setText("TERMINADO .....");

        }

        public void execute(Works rr, String s, String s1) {
            doInBackground(rr);
        }
    }

}
