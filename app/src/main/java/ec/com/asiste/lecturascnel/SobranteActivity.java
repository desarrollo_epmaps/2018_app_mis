package ec.com.asiste.lecturascnel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.configuracion.codigoCNEL;
import com.asistecom.worker.web.entity.configuracion.marcaCNEL;
import com.asistecom.worker.web.entity.visitasOut.CodigoObservacion;
import com.asistecom.worker.web.entity.visitasOut.Coordenada;
import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.visitasOut.medidor;

import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ec.com.asiste.lecturascnel.logica.Nombrar_fotos;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.util;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.util.formatearfechaString;
import static ec.com.asiste.lecturascnel.logica.util.formatearhora;
import static ec.com.asiste.lecturascnel.logica.util.rellenarCerosIzquierda;

public class SobranteActivity extends AppCompatActivity implements LocationListener {

    private static final int REQUEST_TAKE_PHOTO = 1 ;
    String ordenSob = "";
    String nombre_foto = "";
    String obs_auto = "";



    List<String> variables = new ArrayList<String>(Arrays.asList("A", "R", "D"));
    List<String> rangos = new ArrayList<String>(Arrays.asList("N", "D", "G","P","T"));
    EditText et_sr, et_srf, et_obs, et_lectura,et_ciclo,et_sector,et_ruta,et_secuencia,et_posterior, et_previo;
    TextView tv_gps;
    Spinner sp_marca, sp_obs, sp_imp, sp_rango, sp_variable;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    String mCurrentPhotoPath;
    Button foto;
    String lat = "";
    String lonn = "";
    String sat = "";
    String dao = "";
    ArrayList<String> fotos = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobrante);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        et_lectura = (EditText)findViewById(R.id.et_sob_lectura);
        et_obs = (EditText)findViewById(R.id.et_sob_obs);
        et_sr = (EditText)findViewById(R.id.et_sob_sr);
        et_srf = (EditText)findViewById(R.id.et_sob_srf);

        et_ciclo = (EditText)findViewById(R.id.et_sob_ciclo);
        et_sector = (EditText)findViewById(R.id.et_sob_sector);
        et_ruta = (EditText)findViewById(R.id.et_sob_ruta);
        et_secuencia = (EditText)findViewById(R.id.et_sob_secuencia);
        et_posterior = (EditText)findViewById(R.id.et_sob_posterior);
        et_previo = (EditText)findViewById(R.id.et_sob_previo);

        sp_rango = (Spinner)findViewById(R.id.sp_sob_rango);
        sp_variable = (Spinner)findViewById(R.id.sp_sob_variable);

        foto = (Button)findViewById(R.id.bt_sob_foto);
        tv_gps = (TextView)findViewById(R.id.tv_sob_gps);
        sp_imp = (Spinner)findViewById(R.id.sp_sob_imp);
        sp_obs = (Spinner)findViewById(R.id.sp_sob_obs);
        sp_marca = (Spinner)findViewById(R.id.sp_sob_marca);

        try {
            cargarObs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        map_data();
        ordenSob = util.nombrarSobNuevo(Parametros.usuario.toString(),et_ciclo.getText().toString(),sp_variable.getSelectedItem().toString()
                , sp_rango.getSelectedItem().toString());

        et_ciclo.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                ordenSob = util.nombrarSobNuevo(Parametros.usuario.toString(),et_ciclo.getText().toString(),sp_variable.getSelectedItem().toString()
                        , sp_rango.getSelectedItem().toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

    }
    public void  map_data()
    {
        et_ciclo.setText(Parametros.vt_actual.ordenLectura.ciclo.toString());
        et_sector.setText(Parametros.vt_actual.ordenLectura.sector.toString());
        et_ruta.setText(Parametros.vt_actual.ordenLectura.ruta.toString());
        et_secuencia.setText(Parametros.vt_actual.ordenLectura.secuencia.toString());
    }
    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude()+"";
        lonn =   location.getLongitude()+"";
        tv_gps.setText("GPS: "+ lat + "  ,  " + lonn) ;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                fotos.add(util.renombraFotoVideo(nombre_foto));
                int cantidad = fotos.size();
                //tv_fotos.setText(String.valueOf(cantidad));
                foto.setText("FOTO("+String.valueOf(cantidad)+")");
            }
        }
        catch (Exception ex)
        {}
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    public boolean validar_foto_video()
    {
        boolean resultado = false;
        int f = 0;
        int v = 0;

        for (String vv : fotos
                ) {
            if(vv.contains(".jpg"))
            {
                f += 1;
            }
            else if (vv.contains(".mp4"))
            {

                v += 1;

            }

        }

        if( f >= Parametros.foto_sobrante)
        {
            resultado = true;
        }

        return resultado;

    }
    public void cargarObs() throws Exception {

        List<codigoCNEL> cimp = new ArrayList<codigoCNEL>();
        List<codigoCNEL> cobs = new ArrayList<codigoCNEL>();
        List<marcaCNEL> marcaa = new ArrayList<marcaCNEL>();
        codigoCNEL cod_vacio = new codigoCNEL();
        cod_vacio.tipoCodigo = "";
        cod_vacio.nombreCodigo = "";
        cod_vacio.idCodigo = "";
        marcaCNEL mar_vacio = new marcaCNEL();
        mar_vacio.id = "";
        String cods = fileManager.ReadFile(getApplicationContext(),Parametros.dir_archivos.toString(),"/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class,cods);
        cimp.add(cod_vacio);
        cobs.add(cod_vacio);
        marcaa.add(mar_vacio);
        for (codigoCNEL cc: v.getCodigoCNEL()
                ) {

            if(cc.tipoCodigo.contains("1"))
            {
                cimp.add(cc);
            }
            else
            {
                cobs.add(cc);

            }

        }
        for (marcaCNEL mr: v.getMarcaCNEL()
             ) {

            marcaa.add(mr);

        }
        ArrayAdapter adapterV = new ArrayAdapter(this, android.R.layout.simple_spinner_item, variables);
        adapterV.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_variable.setAdapter(adapterV);

        ArrayAdapter adapterR = new ArrayAdapter(this, android.R.layout.simple_spinner_item, rangos);
        adapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_rango.setAdapter(adapterR);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cimp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_imp.setAdapter(adapter);
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cobs);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_obs.setAdapter(adapter2);

        ArrayAdapter adapter3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, marcaa);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_marca.setAdapter(adapter3);
    }
    public void guarda_file() throws Exception {

        ordenSob = util.nombrarSobNuevo(Parametros.usuario.toString(),et_ciclo.getText().toString(),sp_variable.getSelectedItem().toString()
                , sp_rango.getSelectedItem().toString());

        com.asistecom.worker.web.entity.visitasOut.OrdenLectura odl = new com.asistecom.worker.web.entity.visitasOut.OrdenLectura();
        odl.setOrdenLectura(ordenSob);
        odl.setCecgsecu(Parametros.vt_actual.ordenLectura.getCecgsecu());
        odl.setClaseLectura("0");
        Coordenada coord = new Coordenada();
        coord.setCoord_X(lat);
        coord.setCoord_Y(lonn);

        ArrayList<Coordenada> liscord = new ArrayList<Coordenada>();
        liscord.add(coord);

        odl.setCoordenada(liscord);
        odl.setCuentaContrato(util.nombrarSobNuevoCuenta(Parametros.usuario.toString(),et_ciclo.getText().toString(),sp_variable.getSelectedItem().toString()
                , sp_rango.getSelectedItem().toString()));


        odl.setFoto(fotos);
        odl.setSector(et_sector.getText().toString());
        Date currentTime = Calendar.getInstance().getTime();
        odl.setFechaLecturaActual(formatearfechaString(currentTime));
        odl.setHoraLecturaActual(formatearhora(currentTime));
        odl.setLecturaActual(et_lectura.getText().toString());
        odl.setLecturaReal(et_lectura.getText().toString());
        odl.setIdLector(Parametros.usuario.toString());
        odl.setNumeroDeInstalacion("");
        odl.setPeriodo(Parametros.vt_actual.ordenLectura.getPeriodo());
        odl.setRuta(et_ruta.getText().toString());
        odl.setSecuencia(et_secuencia.getText().toString());
        odl.setCiclo(et_ciclo.getText().toString());

        odl.setTipoEmisionFactura("L");
        odl.setTipoActividad(Parametros.vt_actual.ordenLectura.actividadLectura);
        odl.setTipoEnvio("1");

        CodigoObservacion c = new CodigoObservacion();
        c.setCodigo("1");
        ArrayList<CodigoObservacion> cc = new ArrayList<>();
        //cc.add("1");

        if (sp_obs.getSelectedItemPosition() > 0) {
            codigoCNEL auxCod = (codigoCNEL) sp_obs.getSelectedItem();
            CodigoObservacion cod = new  CodigoObservacion();
            cod.setCodigo(auxCod.idCodigo);
            cod.setTipo("observacion1");
            cc.add(cod);
            obs_auto = obs_auto + " "+ auxCod.ObservacionAuto;
        }
        if (sp_imp.getSelectedItemPosition() > 0) {
            codigoCNEL auxCod = (codigoCNEL) sp_imp.getSelectedItem();
            CodigoObservacion cod = new  CodigoObservacion();
            cod.setCodigo(auxCod.idCodigo);
            cod.setTipo("impedimento");
            cc.add(cod);
            obs_auto = obs_auto + " "+ auxCod.ObservacionAuto;
        }
        odl.setObservacionAlfanumerica(obs_auto +"-"+ et_obs.getText().toString());
        medidor med = new medidor();
        med.setMarca(sp_marca.getSelectedItem().toString());
        med.setModelo("");

        med.setSerieFabrica(et_srf.getText().toString());
        med.setSerieAdicional(et_sr.getText().toString());
        med.setMedPosterior(et_posterior.getText().toString());
        med.setMedPrevio(et_previo.getText().toString());
        med.setVariable(sp_variable.getSelectedItem().toString());
        med.setRango(sp_rango.getSelectedItem().toString());
        odl.setMedidor(med);
        odl.setClosedvisits(Parametros.vt_actual.ordenLectura.close);
        odl.setProcessedvisits(Parametros.vt_actual.ordenLectura.processed);
        odl.setContinuousCommunication(Parametros.vt_actual.ordenLectura.continius);
        odl.setCodigoObservacion(cc);
        odl.setIdProyecto(Parametros.vt_actual.ordenLectura.idProyecto);
        odl.setSobrante(true);
        String version = Parametros.version;
        odl.setVersion(version);
        Serializer serializer = new Persister();
        String wb = Parametros.vt_actual.ordenLectura.processed.replace("/","!");
        File result = new File( FilenameUtils.getPath(Parametros.vt_actual.ordenLectura.pathout) + "/sob_" + ordenSob+ ".xml");
        try {
            com.asistecom.worker.web.entity.visitasOut.visitas vv = new com.asistecom.worker.web.entity.visitasOut.visitas();
            vv.setOrdenLectura(odl);
            serializer.write(vv, result);


            String showToastMessage = "GUARDADO.";

            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {


            }
        });

    }
   public void accionG(View view) throws Exception {

        if(validar_foto_video()) {

            if(validarSobrante()) {
                guarda_file();
            }
        }
        else
        {

            Toast.makeText(this, "DEBE TOMAR FOTO/FOTOS", Toast.LENGTH_SHORT).show();


        }
   }
    private File createImageFile() throws IOException, ParseException {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String nom = nf.foto_cnel(rellenarCerosIzquierda(ordenSob,20) , rellenarCerosIzquierda(ordenSob,20) , Parametros.idProyecto,  formatearfechaString(now),formatearhora(now),Parametros.vt_actual.ordenLectura.periodo,Parametros.vt_actual.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName
        nombre_foto = nom;

        File image = new File(storageDir+"/"+nom);


        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public boolean validarSobrante()
    {

        boolean resultado = true;
        if((et_previo.getText().toString() + et_posterior.getText().toString()).equals(""))
        {

            Toast.makeText(this, "DEBE INGRESAR MEIDORES PREVIOS/POSTRERIOR", Toast.LENGTH_SHORT).show();
            resultado = false;

        }
        if(sp_marca.getSelectedItemPosition() < 1)
        {
            Toast.makeText(this, "DEBE SELECIONAR LA MARCA", Toast.LENGTH_SHORT).show();
            resultado = false;

        }
        if(sp_imp.getSelectedItemPosition() < 1 && et_lectura.getText().equals(""))
        {
            Toast.makeText(this, "DATOS INCONSISTENTES", Toast.LENGTH_SHORT).show();
            resultado = false;

        }


        return  resultado;
    }
    public void tomar_foto(View view) throws IOException {
        dispatchTakePictureIntent();
        int cantidad = fotos.size();
        //tv_fotos.setText(String.valueOf(cantidad));
        foto.setText("FOTO("+String.valueOf(cantidad)+")");
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
}
