package ec.com.asiste.lecturascnel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asistecom.worker.web.entity.configuracion.ConfiguracionCiclo;
import com.asistecom.worker.web.entity.configuracion.codigoCNEL;
import com.asistecom.worker.web.entity.visitasOut.CodigoObservacion;
import com.asistecom.worker.web.entity.visitasOut.Coordenada;
import com.asistecom.worker.web.entity.visitasOut.Parametro;
import com.asistecom.worker.web.entity.visitasOut.medidor;
import com.asistecom.worker.web.entity.vistiasIn.parametro;
import com.asistecom.worker.web.entity.vistiasIn.visitas;

import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ec.com.asiste.lecturascnel.dblite.sqLite;
import ec.com.asiste.lecturascnel.logica.Nombrar_fotos;
import ec.com.asiste.lecturascnel.logica.Parametros;
import ec.com.asiste.lecturascnel.logica.SimpleGestureFilter;
import ec.com.asiste.lecturascnel.logica.fileManager;
import ec.com.asiste.lecturascnel.logica.util;

import static android.support.v4.content.FileProvider.getUriForFile;
import static ec.com.asiste.lecturascnel.logica.fileManager.copyFileUsingStream;
import static ec.com.asiste.lecturascnel.logica.util.formatearfechaString;
import static ec.com.asiste.lecturascnel.logica.util.formatearhora;

;

public class
IndustrialesActivity extends AppCompatActivity implements LocationListener, SimpleGestureFilter.SimpleGestureListener {

    Button bfoto,bvideo;
    String nombre_foto = "";
    String obs_auto = "";
    int vides = 0;
    ArrayList<com.asistecom.worker.web.entity.vistiasIn.parametro> lparametros = new ArrayList<>();
    TextView tv_cuenta, tv_ciclo, tv_serie, tv_ruta, tv_propietario, tv_ubic, tv_direccion, tv_sr, tv_srf, tv_marca, tv_fotos;
    EditText et_contrato, et_lectura, et_obs;
    Spinner s_obs, s_imp;
    private SimpleGestureFilter detector;
    private visitas vt = new visitas();
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    ListView lv;
    TextView txtLat;
    String lat = "";
    String lonn = "";
    String sat = "";
    String dao = "";
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    ArrayList<String> fotos = new ArrayList<String>();
    ArrayList<String> videos = new ArrayList<String>();
    TextView tv_gps;
    Spinner sp_imp, sp_cod;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_industriales);
        detector = new SimpleGestureFilter(IndustrialesActivity.this, this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        lv = (ListView) findViewById(R.id.lv_industriales);
        tv_gps = (TextView) findViewById(R.id.tv_i_gps);
        //tv_fotos = (TextView) findViewById(R.id.tv_ig_fotos);
        tv_direccion = (TextView) findViewById(R.id.tv_i_dir);
        tv_serie = (TextView) findViewById(R.id.tv_i_sr);
        tv_cuenta = (TextView) findViewById(R.id.tv_i_cuenta);
        //tv_contrato = (TextView) findViewById(R.id.tv_ig_contrato);
        tv_propietario = (TextView) findViewById(R.id.tv_i_nombre);
        tv_ubic = (TextView) findViewById(R.id.tv_i_geo);
        sp_imp = (Spinner) findViewById(R.id.sp_i_imp);
        sp_cod = (Spinner) findViewById(R.id.sp_i_obs);
        et_obs = (EditText) findViewById(R.id.et_i_obs);
        tv_srf = (TextView) findViewById(R.id.tv_i_srf);
        bfoto = (Button) findViewById(R.id.bt_i_foto);
        bvideo = (Button) findViewById(R.id.bt_i_video);
        try {
            cargarObs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cargarR("");
        mapVisita(vt);


    }

    public void cargarParametros() {


    }


    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences prefe = getSharedPreferences("datos", Context.MODE_PRIVATE);

        lat = location.getLatitude() + "";
        lonn = location.getLongitude() + "";
        tv_gps.setText("GPS: " + lat + "  ,  " + lonn);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    public void cargarR(String previa) {
        try {

            sqLite sq = new sqLite();
            vt = new visitas();
            vt = sq.cuentaR_sql(Parametros.res, previa);
        } catch (Exception e) {
        }


    }

    public void cargarL(String previa) {
        try {

            sqLite sq = new sqLite();
            vt = new visitas();
            vt = sq.cuentaL_sql(Parametros.res, previa);
        } catch (Exception e) {
        }


    }

    public void mapVisita(visitas vt) {

        Serializer serializer = new Persister();
        visitas v = null;
        try {
            String xmll = fileManager.ReadFile(this, vt.ordenLectura.pathin);
            v = serializer.read(visitas.class, xmll);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lparametros = v.ordenLectura.getParametro();


        AdapterItemIndustriales adapter = new AdapterItemIndustriales(this, lparametros);


        String obs_auto = "";
        lv.setAdapter(adapter);

        //tv_fotos.setText("0");
        //et_lectura.setText("");
        et_obs.setText("");
        sp_imp.setSelection(0);
        bvideo.setText("VIDEO("+String.valueOf(0)+")");
        bfoto.setText("FOTO("+String.valueOf(0)+")");
        sp_cod.setSelection(0);
        fotos.clear();
        try {
            //tv_ciclo.setText(vt.ordenLectura.porcion);
            tv_direccion.setText(vt.ordenLectura.direccion);
            tv_serie.setText( vt.ordenLectura.numeroMedidorAdd);
            tv_srf.setText(vt.ordenLectura.numeroMedidor);
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);
            //tv_contrato.setText(vt.ordenLectura.ordenLectura);
            tv_ubic.setText( vt.ordenLectura.sector
                    + "-" + vt.ordenLectura.ruta + "-" + vt.ordenLectura.secuencia);
            ///tv_seciencia.setText(vt.ordenLectura.secuencia);
            //tv_sector.setText(vt.ordenLectura.sector.toString());
            // tv_ruta.setText(vt.ordenLectura.ruta);
            tv_propietario.setText(vt.ordenLectura.informacionAdicional.getNombreCliente());
            tv_cuenta.setText(vt.ordenLectura.cuentaContrato);

        } catch (Exception exx) {

            int a = 0;
        }


    }

    public void accionder(View view) {
        cargarR(vt.ordenLectura.ordenLectura);
        mapVisita(vt);

    }

    public void accionizq(View view) {
        cargarL(vt.ordenLectura.ordenLectura);
        mapVisita(vt);

    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private File createImageFileV() throws IOException, ParseException {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String nom = nf.video_cnel(vt.ordenLectura.cuentaContrato, vt.ordenLectura.ordenLectura, Parametros.idProyecto, formatearfechaString(now), formatearhora(now), vt.ordenLectura.periodo,vt.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName


        File image = new File(storageDir + "/" + nom);

        nombre_foto = nom;
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private File createImageFile() throws IOException, ParseException {
        // Create an image file name
        Nombrar_fotos nf = new Nombrar_fotos();
        Date now = new Date();
        String nom = nf.foto_cnel(vt.ordenLectura.cuentaContrato, vt.ordenLectura.ordenLectura,Parametros.idProyecto , formatearfechaString(now), formatearhora(now), vt.ordenLectura.periodo, vt.ordenLectura.ciclo);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//imageFileName


        File image = new File(storageDir + "/" + nom);
        nombre_foto = nom;

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    static final int REQUEST_VIDEO_CAPTURE =2;
    private static final int VIDEO_CAPTURE = 101;
    private void dispatchTakeVideoIntent() throws ParseException {

        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileV();
            } catch (IOException ex) {
                // Error occurred while creating the File

            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getBaseContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                int cantidad = fotos.size();
                //tv_fotos.setText(String.valueOf(cantidad));
                bfoto.setText("FOTO("+String.valueOf(cantidad)+")");
                fotos.add(util.renombraFotoVideo(nombre_foto));
            }
            if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                int cantidad = fotos.size();
                //tv_fotos.setText(String.valueOf(cantidad));
                vides +=1;
                bvideo.setText("VIDEO("+String.valueOf(vides)+")");
                fotos.add(util.renombraFotoVideo(nombre_foto));
            }
        }
        catch (Exception ex)
        {}
    }

    public void accionVideo(View view) throws ParseException {


        dispatchTakeVideoIntent();
    }

    public void cargarObs() throws Exception {

        List<codigoCNEL> cimp = new ArrayList<codigoCNEL>();
        List<codigoCNEL> cobs = new ArrayList<codigoCNEL>();
        codigoCNEL cod_vacio = new codigoCNEL();
        cod_vacio.tipoCodigo = "";
        cod_vacio.nombreCodigo = "";
        cod_vacio.idCodigo = "";
        cimp.add(cod_vacio);
        cobs.add(cod_vacio);
        String cods = fileManager.ReadFile(getApplicationContext(), Parametros.dir_archivos.toString(), "/config.xml");
        Serializer serializer = new Persister();
        serializer = new Persister();
        ConfiguracionCiclo v = null;
        v = serializer.read(ConfiguracionCiclo.class, cods);
        //Parametros.codigos = v.getCodigoCNEL();
        Parametros.codigos.clear();
        Parametros.codigos.add(cod_vacio);
        for (codigoCNEL cc : v.getCodigoCNEL()
                ) {
            Parametros.codigos.add(cc);
            if (cc.tipoCodigo.contains("1")) {
                cimp.add(cc);
            } else {
                cobs.add(cc);

            }

        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cimp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_imp.setAdapter(adapter);
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cobs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_cod.setAdapter(adapter2);


    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void tomar_foto(View view) throws IOException {
        dispatchTakePictureIntent();


    }

    @Override
    public void onSwipe(int direction) {
        String showToastMessage = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                showToastMessage = "You have Swiped Right.";
                cargarL(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                showToastMessage = "You have Swiped Left.";
                cargarR(vt.ordenLectura.ordenLectura);
                mapVisita(vt);
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                showToastMessage = "You have Swiped Down.";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                showToastMessage = "You have Swiped Up.";
                break;

        }
        Toast.makeText(this, showToastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_i_cnel, menu);
        return true;
    }

    @Override
    public void onDoubleTap() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.bt_i_buscar_serie) {
            Toast.makeText(this, "BUSCAR", Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(this, BuscarLActivity.class);
            startActivity(intent);
        }
        if (id == R.id.bt_i_i) {
            cargarL(vt.ordenLectura.ordenLectura.toString());
            mapVisita(vt);
        }
        if (id == R.id.bt_i_d) {
            cargarR(vt.ordenLectura.ordenLectura.toString());
            mapVisita(vt);
        }
        return super.onOptionsItemSelected(item);
    }

    public void accionGuarado(View view) {

        if(validar_foto_video()) {
            try {
                guarda_file();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(this, "VIDEO OBLIGATIRIO", Toast.LENGTH_LONG).show();

        }
    }

    public boolean validar_foto_video()
    {
        boolean resultado = false;
        int f = 0;
        int v = 0;

        for (String vv : fotos
             ) {
            if(vv.contains(".jpg"))
            {
                f += 1;
            }
            else if (vv.contains(".mp4"))
            {

                v += 1;

            }

        }
        for (String vv : videos
        ) {
            if(vv.contains(".jpg"))
            {
                f += 1;
            }
            else if (vv.contains(".mp4"))
            {

                v += 1;

            }

        }

        if(v >= Parametros.video_industriales && f >= Parametros.foto_industriales)
        {
            resultado = true;
        }

        return resultado;

    }

    public void guaradar() throws Exception {

/// validar pedir fotos pedir retos guardar siguente cuenta0
        if(validar_foto_video())
        {
            guarda_file();


        }
        else
        {

            Toast.makeText(this, "Tomar las fotos o videos necesarios", Toast.LENGTH_LONG).show();
        }
    }
    public void guarda_file() throws Exception {






        com.asistecom.worker.web.entity.visitasOut.OrdenLectura odl = new com.asistecom.worker.web.entity.visitasOut.OrdenLectura();
        odl.setOrdenLectura(vt.ordenLectura.getOrdenLectura());
        odl.setCecgsecu(vt.ordenLectura.getCecgsecu());
        odl.setMedidor(new medidor());
        odl.setNumeroDeInstalacionPrincipal("");
        odl.setClaseLectura("3");
        Coordenada coord = new Coordenada();
        coord.setCoord_X(lat);
        coord.setCoord_Y(lonn);
        odl.setNumFactura("0");
        ArrayList<Coordenada> liscord = new ArrayList<Coordenada>();
        liscord.add(coord);

        odl.setCoordenada(liscord);
        odl.setCuentaContrato(vt.ordenLectura.cuentaContrato);
        odl.setSobrante(false);
        //odl.setParametro(parmasout);
        ArrayList<String> listafotos = new ArrayList<String>();
        for (String aa : fotos
             ) {
            listafotos.add(aa);
        }
        for (String aa : videos
        ) {
            listafotos.add(aa);
        }
        odl.setFoto(listafotos);



        Date currentTime = Calendar.getInstance().getTime();
        odl.setFechaLecturaActual(formatearfechaString(currentTime));
        odl.setHoraLecturaActual(formatearhora(currentTime));
        //odl.setLecturaActual(et_lectura.getText().toString());
        //odl.setLecturaReal(et_lectura.getText().toString());
        odl.setIdLector(Parametros.usuario.toString());
        odl.setNumeroDeInstalacion(vt.ordenLectura.numeroDeInstalacion);
        odl.setPeriodo(vt.ordenLectura.getPeriodo());
        odl.setRuta(vt.ordenLectura.getRuta());
        odl.setSecuencia(vt.ordenLectura.getSecuencia());
        odl.setTipoEmisionFactura("L");
        odl.setEnServicioOut(true);
        odl.setClosedvisits(vt.ordenLectura.close);
        odl.setProcessedvisits(vt.ordenLectura.processed);
        odl.setContinuousCommunication(vt.ordenLectura.continius);
        odl.setCiclo(vt.ordenLectura.porcion.toString());
        odl.setTipoActividad(vt.ordenLectura.actividadLectura);
        odl.setTipoEnvio("3");
        odl.setIdProyecto(vt.ordenLectura.idProyecto);
        odl.setObservacionAlfanumerica(et_obs.getText().toString());
        CodigoObservacion c = new CodigoObservacion();
        c.setCodigo("1");
        ArrayList<CodigoObservacion> cc = new ArrayList<>();
        //cc.add("1");

        if (sp_cod.getSelectedItemPosition() > 0) {
            codigoCNEL auxCod = (codigoCNEL) sp_cod.getSelectedItem();
            CodigoObservacion cod = new  CodigoObservacion();
            cod.setCodigo(auxCod.idCodigo);
            cod.setTipo("observacion1");
            cc.add(cod);
            obs_auto = obs_auto + " "+ auxCod.ObservacionAuto;
        }
        if (sp_imp.getSelectedItemPosition() > 0) {
            codigoCNEL auxCod = (codigoCNEL) sp_imp.getSelectedItem();
            CodigoObservacion cod = new  CodigoObservacion();
            cod.setCodigo(auxCod.idCodigo);
            cod.setTipo("impedimento");
            cc.add(cod);
            obs_auto = obs_auto + " "+ auxCod.ObservacionAuto;
        }

        odl.setObservacionAlfanumerica(obs_auto + et_obs.getText().toString());
        odl.setCodigoObservacion(cc);

        String version = Parametros.version;
        odl.setVersion(version);
        Serializer serializer = new Persister();

        try {
            int a = lv.getAdapter().getCount();


            for (int i = 0; i < a; i++) {


                parametro ppp = (parametro) lv.getAdapter().getItem(i);
                File result = new File(FilenameUtils.getPath(Parametros.vt_actual.ordenLectura.pathout) + "/sob_" +ppp.getOrdenTrabajo() + ".xml");
                odl.setLecturaActual(ppp.getLectura());
                odl.setLecturaReal(ppp.getLectura());
                odl.setOrdenLectura(ppp.getOrdenTrabajo());
                com.asistecom.worker.web.entity.visitasOut.visitas vv = new com.asistecom.worker.web.entity.visitasOut.visitas();
                vv.setOrdenLectura(odl);
                serializer.write(vv, result);

            }


            sqLite ss = new sqLite();
            ss.marcarLeido(vt.ordenLectura.ordenLectura.toString());
            String showToastMessage = "GUARDADO.";
            cargarR(vt.ordenLectura.ordenLectura);
            Toast.makeText(this, showToastMessage, Toast.LENGTH_LONG).show();
            mapVisita(vt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {


            }
        });

    }
}